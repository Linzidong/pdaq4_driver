﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AcquistionInspector.Utils
{
    public class EnumUtil
    {
        public static bool GetEnumReadOnly(Enum val)
        {
            Type type = val.GetType();
            FieldInfo fd = type.GetField(val.ToString());
            if (fd == null)
                return true;
            object[] attrs = fd.GetCustomAttributes(typeof(ReadOnlyAttribute), false);
            bool readOnly = false;
            foreach (ReadOnlyAttribute attr in attrs)
            {
                readOnly = attr.ReadOnly;
            }
            return readOnly;
        }

        public static List<UInt32> GetEnumDefaultValues(Enum val)
        {
            Type type = val.GetType();
            FieldInfo fd = type.GetField(val.ToString());
            if (fd == null)
                return null;
            object[] attrs = fd.GetCustomAttributes(typeof(DefaultValueAttribute), false);
            List<UInt32> defaultValues= null;
            foreach (DefaultValueAttribute attr in attrs)
            {
                defaultValues = attr.DefaultValues;
            }
            return defaultValues;
        }

        public static string GetEnumDescription(Enum val)
        {
            Type type = val.GetType();
            FieldInfo fd = type.GetField(val.ToString());
            if (fd == null)
                return "";
            object[] attrs = fd.GetCustomAttributes(typeof(DescriptionAttribute), false);
            string description = string.Empty;
            foreach (DescriptionAttribute attr in attrs)
            {
                description = attr.Description;
            }
            return description;
        }
    }
}
