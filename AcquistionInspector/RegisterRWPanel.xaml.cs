﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Collections;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Collections.ObjectModel;
using System.Windows.Interop;
using AcquistionInspector.ReadWriteCsv;
using System.Windows.Forms;
using System.IO;
namespace AcquistionInspector
{
    /// <summary>
    /// Interaction logic for RegisterRWPanel.xaml
    /// </summary>
    public partial class RegisterRWPanel : System.Windows.Controls.UserControl
    {
        public const int ADDRESS_SPACE_BYTES_PER_PACE = 0x100;
        public const int PDAQ4_ALLOCATED_MEM_SIZE = PDAQ4Controller.MAX_REG_SPACE_SIZE;
        public const int DATA_GRID_ROW = 16;
        public const int DATA_GRID_WIDTH_BASE = 40;
        public const int DATA_GRID_COL_NUM_BYTE = ADDRESS_SPACE_BYTES_PER_PACE / DATA_GRID_ROW;
        public const int DATA_GRID_COL_NUM_WORD = ADDRESS_SPACE_BYTES_PER_PACE / DATA_GRID_ROW / 2;
        public const int DATA_GRID_COL_NUM_DWORD = ADDRESS_SPACE_BYTES_PER_PACE / DATA_GRID_ROW / 4;
        public const int TYPE_BYTE = 1;
        public const int TYPE_WORD = 2;
        public const int TYPE_DWORD = 4;


        private Hashtable VIEW_TYPE_COL_NUM = new Hashtable();
        private Hashtable VIEW_TYPE_COLUMN_WIDTH_MAP = new Hashtable();
        private PDAQ4Controller pdaqcontrollder;
        private int current_view_type = TYPE_BYTE;
        private int current_offset = 0;
        private Byte[] current_register_space;
        private Helper helper = Helper.GetInstance();
        public delegate void ReadRegisterCompleted(object mem);

        private ObservableCollection<string[]> bytetable = new ObservableCollection<string[]>(new List<string[]>(DATA_GRID_ROW));
        private ObservableCollection<string[]> wordtable = new ObservableCollection<string[]>(new List<string[]>(DATA_GRID_ROW));
        private ObservableCollection<string[]> dwordtable = new ObservableCollection<string[]>(new List<string[]>(DATA_GRID_ROW));
        private PDAQ4Controller pdaq4controllder = PDAQ4Controller.GetInstance();
        private Thread registers_read_thread = null;
        public RegisterRWPanel()
        {
            pdaqcontrollder = PDAQ4Controller.GetInstance();
            InitializeComponent();

            VIEW_TYPE_COL_NUM[TYPE_BYTE] = DATA_GRID_COL_NUM_BYTE;
            VIEW_TYPE_COL_NUM[TYPE_WORD] = DATA_GRID_COL_NUM_WORD;
            VIEW_TYPE_COL_NUM[TYPE_DWORD] = DATA_GRID_COL_NUM_DWORD;

            VIEW_TYPE_COLUMN_WIDTH_MAP[TYPE_BYTE] = DATA_GRID_WIDTH_BASE;
            VIEW_TYPE_COLUMN_WIDTH_MAP[TYPE_WORD] = DATA_GRID_WIDTH_BASE * 2;
            VIEW_TYPE_COLUMN_WIDTH_MAP[TYPE_DWORD] = DATA_GRID_WIDTH_BASE * 4;


           // pdaqcontrollder.RegisterReadRegisterObserver(OnReadRegisterCompleted);
            current_register_space = new Byte[PDAQ4_ALLOCATED_MEM_SIZE * sizeof(UInt32)];


            for (int i = 0; i < DATA_GRID_ROW; i++)
            {
                bytetable.Add(new String[DATA_GRID_COL_NUM_BYTE]);
                wordtable.Add(new String[DATA_GRID_COL_NUM_WORD]);
                dwordtable.Add(new String[DATA_GRID_COL_NUM_DWORD]);

            }


            this.RegisterByteTable.ItemsSource = bytetable;
            this.RegisterWordTable.ItemsSource = wordtable;
            this.RegisterDwordTable.ItemsSource = dwordtable;




        }

        
        public void OnReadRegisterCompleted(UInt32[] mem)
        {
            Buffer.BlockCopy(mem, 0, current_register_space, current_offset, mem.Length * sizeof(UInt32));
            this.Dispatcher.BeginInvoke((ReadRegisterCompleted)UpdateRegisterValue, mem);
        }

        public void UpdateRegisterValue(object mem)
        {

            for (int i = 0; i < DATA_GRID_ROW; i++)
            {

                String[] byte_new_ptr = new String[DATA_GRID_COL_NUM_BYTE];
                String[] word_new_ptr = new String[DATA_GRID_COL_NUM_WORD];
                String[] dword_new_ptr = new String[DATA_GRID_COL_NUM_DWORD];

                for (int j = 0; j < DATA_GRID_COL_NUM_BYTE; j++)
                    byte_new_ptr[j] = string.Format("{0:X2}", current_register_space[current_offset + i * DATA_GRID_COL_NUM_BYTE + j]);
                for (int j = 0; j < DATA_GRID_COL_NUM_WORD; j++)
                    word_new_ptr[j] = string.Format("{0:X4}", current_register_space[current_offset + i * DATA_GRID_COL_NUM_BYTE + 2 * j] + ((current_register_space[current_offset + i * DATA_GRID_COL_NUM_BYTE + 2 * j + 1]) << 8));
                for (int j = 0; j < DATA_GRID_COL_NUM_DWORD; j++)
                    dword_new_ptr[j] = string.Format("{0:X8}", current_register_space[current_offset + i * DATA_GRID_COL_NUM_BYTE + 4 * j] + ((current_register_space[current_offset + i * DATA_GRID_COL_NUM_BYTE + 4 * j + 1]) << 8) + ((current_register_space[current_offset + i * DATA_GRID_COL_NUM_BYTE + 4 * j + 2]) << 16) + ((current_register_space[current_offset + i * DATA_GRID_COL_NUM_BYTE + 4 * j + 3]) << 24));

                bytetable[i] = byte_new_ptr;
                wordtable[i] = word_new_ptr;
                dwordtable[i] = dword_new_ptr;

            }
        }


        private void View_Click(object sender, RoutedEventArgs e)
        {
            this.RegisterByteTable.Visibility = System.Windows.Visibility.Collapsed;
            this.RegisterWordTable.Visibility = System.Windows.Visibility.Collapsed;
            this.RegisterDwordTable.Visibility = System.Windows.Visibility.Collapsed;

            if (sender == this.ByteView)
            {
                this.RegisterByteTable.Visibility = System.Windows.Visibility.Visible;
                this.current_view_type = TYPE_BYTE;

            }
            else if (sender == this.WordView)
            {
                this.RegisterWordTable.Visibility = System.Windows.Visibility.Visible;
                this.current_view_type = TYPE_WORD;
            }
            else if (sender == this.DwordView)
            {
                this.RegisterDwordTable.Visibility = System.Windows.Visibility.Visible;
                this.current_view_type = TYPE_DWORD;
            }
        }

        private void OffsetAddress_MouseWheel(object sender, MouseWheelEventArgs e)
        {

            Int32 rotation = (Int32)(e.Delta / System.Windows.Forms.SystemInformation.MouseWheelScrollDelta);

            this.current_offset += rotation * ADDRESS_SPACE_BYTES_PER_PACE;
            if (this.current_offset < 0)
                this.current_offset = 0;
            else if (this.current_offset >= PDAQ4_ALLOCATED_MEM_SIZE - ADDRESS_SPACE_BYTES_PER_PACE)
                this.current_offset = PDAQ4_ALLOCATED_MEM_SIZE - ADDRESS_SPACE_BYTES_PER_PACE;
            this.OffsetAddress.Content = string.Format("0x{0:X6}", this.current_offset);
            UpdateRegisterValue(this.current_register_space);
        }

     

        private void RegisterTable_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = String.Format("{0:X2}", (e.Row.GetIndex()));
        }

        private void RegisterByteTable_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            RegisterEditWindow wind = new RegisterEditWindow();

            System.Windows.Controls.DataGridCell gcell = sender as System.Windows.Controls.DataGridCell;
            int colindex = gcell.Column.DisplayIndex;
            int rowindex = RegisterDwordTable.SelectedIndex;
            String[] selectedrow = (String[])RegisterDwordTable.SelectedItems[0];
            uint dwordoffset = (uint)((this.current_offset / 4) + rowindex * DATA_GRID_COL_NUM_DWORD + colindex);
            uint orginalvalue = Convert.ToUInt32(selectedrow[colindex], 16);
            wind.Title = string.Format("BAR_ADDRESS:{0:X8}", dwordoffset * sizeof(UInt32));
            wind.Top = e.GetPosition(this).Y;
            wind.Left = e.GetPosition(this).X;

            wind.SetRegisterToWrite(orginalvalue, dwordoffset);
            wind.ShowDialog();

        }

        private void EditPageOffset(object sender, MouseButtonEventArgs e)
        {
            EditPageOffsetWindow win = new EditPageOffsetWindow((string)this.OffsetAddress.Content, ADDRESS_SPACE_BYTES_PER_PACE, (uint)PDAQ4_ALLOCATED_MEM_SIZE);
            win.ShowDialog();
            if (win.DialogResult != null && (bool)win.DialogResult)
            {
                OffsetAddress.Content = string.Format("0x{0:X6}", win.StartAddress);
                current_offset = (Int32)win.StartAddress;
                if (this.current_offset > PDAQ4_ALLOCATED_MEM_SIZE - ADDRESS_SPACE_BYTES_PER_PACE)
                current_offset = PDAQ4_ALLOCATED_MEM_SIZE - ADDRESS_SPACE_BYTES_PER_PACE;
                
                UpdateRegisterValue(this.current_register_space);
            }
            win.Close();
        }

        private void OnSave(object sender, RoutedEventArgs e)
        {
            SaveRegisterWindow win = new SaveRegisterWindow(this.current_register_space, (UInt32)this.current_offset, ADDRESS_SPACE_BYTES_PER_PACE);
            win.ShowDialog();
            win.Close();
        }

        private void OnLoadCVS(object sender, RoutedEventArgs e)
        {

            OpenFileDialog openfiledlg = new OpenFileDialog();
            openfiledlg.FileName = "Registers"; // Default file name
            openfiledlg.DefaultExt = ".csv"; // Default file extension
            openfiledlg.Filter = "CSV documents (.csv)|*.csv"; // Filter files by extension 
            openfiledlg.CheckFileExists = false;

            openfiledlg.Title = "Select data archive to load";
            if (openfiledlg.ShowDialog() == DialogResult.OK)
            {

                try
                {
                    // Open document 
                    string filename = openfiledlg.FileName;
                    using (CsvFileReader reader = new CsvFileReader(filename))
                    {
                        CsvRow row = new CsvRow();
                         MessageBoxResult msgboxresult = System.Windows.MessageBox.Show("Confirm to write the block?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (MessageBoxResult.Yes == msgboxresult)
                        {
                            while (reader.ReadRow(row))
                            {
                                UInt32 addresstowrite = Convert.ToUInt32(row[0], 16);
                                UInt32 value = Convert.ToUInt32(row[1], 16);
                                this.pdaqcontrollder.WritePDAQ4_CARD_Reg(addresstowrite/4,value);
                            }
                          
                        }
                    }
                       
                }


                catch (Exception exception)
                {
                    helper.Error(exception.Message);
                }



            }

        }

        private void OnLoad(object sender, RoutedEventArgs e)
        {
            if (registers_read_thread != null && registers_read_thread.IsAlive)
            {
                registers_read_thread.Abort();
                while (registers_read_thread.IsAlive) Thread.Sleep(10);
                registers_read_thread = null;
            }
          

                registers_read_thread = new Thread((ThreadStart)ReadRegisters);
                registers_read_thread.Start();
            
        }
        public void OnDeviceFound(int isfound)
        {
            if (isfound==1) 
                this.IsEnabled = true;
            else 
                this.IsEnabled = false;

        }
        void ReadRegisters()
        {
            try
            {
                while (true)
                {
                    if (pdaq4controllder.IsPDAQ4Found == 1 && pdaq4controllder.AutoRefresh)
                    {
                        UInt32[] mem = pdaqcontrollder.ReadPDAQ4_CARD_RegSpace((UInt32)this.current_offset / sizeof(UInt32), (UInt32)ADDRESS_SPACE_BYTES_PER_PACE / sizeof(UInt32));
                        OnReadRegisterCompleted(mem);
                    }
                    Thread.Sleep((int)(pdaqcontrollder.ReadRegisterInterval * 1000));
                }

            }
            catch( ThreadAbortException)
            {

            }
        }

        public void RefreshRegistersManually()
        {
             if (pdaq4controllder.IsPDAQ4Found == 1 && !pdaq4controllder.AutoRefresh)
             {
                 //UInt32[] mem = pdaqcontrollder.ReadPDAQ4_CARD_RegSpace((UInt32)this.current_offset / sizeof(UInt32), (UInt32)ADDRESS_SPACE_BYTES_PER_PACE / sizeof(UInt32));
                UInt32[] mem = pdaqcontrollder.ReadPDAQ4_CARD_RegSpace(RegCon.RT_FRAMER_OFFSET / sizeof(UInt32), (UInt32)ADDRESS_SPACE_BYTES_PER_PACE / sizeof(UInt32));
                 OnReadRegisterCompleted(mem);
             }
        }

        private void Unload(object sender, RoutedEventArgs e)
        {
            if (registers_read_thread != null && registers_read_thread.IsAlive)
            {
                registers_read_thread.Abort();
                while (registers_read_thread.IsAlive) Thread.Sleep(10);
                registers_read_thread = null;
            }
        }

        private void OnLoadCVSFromFolder(object sender, RoutedEventArgs e)
        {

            FolderBrowserDialog openfiledlg = new FolderBrowserDialog();
           

            openfiledlg.Description= "Select Folder and load the all register archiving files";
            UInt32 LineNumber = 1;
            if (openfiledlg.ShowDialog() == DialogResult.OK)
            {
                MessageBoxResult msgboxresult = System.Windows.MessageBox.Show("Confirm to write the block?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (MessageBoxResult.No == msgboxresult)
                    return;

                try
                {
                    // Open document 
                    string selectedpath = openfiledlg.SelectedPath;
                    DirectoryInfo aDir = new DirectoryInfo(selectedpath);
                    FileInfo[] fileinfos = aDir.GetFiles();

                    foreach (FileInfo fileinfo in fileinfos)
                    { 
                            CsvFileReader reader = new CsvFileReader(fileinfo.FullName);
                            if (fileinfo.Extension.ToLower()!=".csv")
                            {
                                helper.Warn(String.Format("The file {0} will not be parsed,it is not a csv file",fileinfo.FullName));
                                continue;
                            }
                            CsvRow row = new CsvRow();
                            while (reader.ReadRow(row))
                            {
                                UInt32 addresstowrite = Convert.ToUInt32(row[0], 16);
                                UInt32 value = Convert.ToUInt32(row[1], 16);
                                this.pdaqcontrollder.WritePDAQ4_CARD_Reg(addresstowrite/4,value);
                            }
                          
                        
                     }
                }

                catch (Exception exception)
                {
                    helper.Error(exception.Message);
                }



            }

        }

       



    }
}