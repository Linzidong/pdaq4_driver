﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AcquistionInspector.Models
{
    public class RegisterModel : ObservableObject
    {

        private RegisterType _registerType;

        private string _description;

        public string Description
        {
            get => _description;
            set { _description = value;
                RaisePropertyChanged(()=>Description);
            }
        }


        private ulong _address;

        private ulong _value;

        private string _name;

        private bool _readOnly;

        private uint _offset;

        public RegisterType RegisterType
        {
            get => _registerType;
            set
            {
                _registerType = value;
                RaisePropertyChanged(() => RegisterType);
            }
        }

        public uint Offset
        {
            get => _offset;
            set
            {
                _offset = value;
                RaisePropertyChanged(() => Offset);

            }
        }


        public bool ReadOnly
        {
            get => _readOnly;
            set
            {
                _readOnly = value;

                RaisePropertyChanged(() => ReadOnly);
            }
        }


        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                RaisePropertyChanged(() => Name);
            }
        }

        public ulong Address
        {
            get => _address;
            set
            {
                _address = value;
                RaisePropertyChanged(() => Address);
            }
        }

        public ulong Value
        {
            get => _value;
            set
            {
                _value = value;
                RaisePropertyChanged(() => Value);
            }
        }

    }
}
