﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using PDAQ4DriverAgent.PDAQ4Implementation;
using PDAQ4DriverInterface;
using System.IO;
using System.Collections;
using AcquistionInspector.Models;

namespace AcquistionInspector
{
    public delegate void OnFrame(Frame frame);

    public delegate void OnRTFrameSizeChanged(int new_RTVectorPerFrame, int new_RFSamplesPerVector);

    public delegate void OnRFFrameSizeChanged(int new_RFVectorPerFrame, int new_RFSamplesPerVector);

    public delegate void OnViewStyleChanged(bool circleview);

    public delegate void OnChannelConnected(DMAInfo dmainfo, FifoInfo info);

    public class PDAQ4Controller
    {
        // max valid register number
        public const int MAX_REG_SPACE_SIZE = 0x100000;

        public const int BYTE_PER_PAGE = 256;
        public const int page_size = 64; //64 register per page
        private DriverAgent drvagent;

        public delegate void ReadRegistersCompletedCallBack(UInt32[] mem);

        public event OnFrame OnFrameEvent;

        public event OnFrame onReadRawEvent;

        public event OnRTFrameSizeChanged RTFrameSizeChangedEvent;

        public event OnRFFrameSizeChanged RFFrameSizeChangedEvent;

        public event OnViewStyleChanged ViewStyleChanged;

        public event OnChannelConnected ChannelConnected;

        private List<ReadRegistersCompletedCallBack> RegisterCompletedObservers;
        private static PDAQ4Controller s_instance = null;
        private volatile float read_register_thread_interval = 1.0f; // 1 second
        private FiFoConnection connection_rf = null;
        private FiFoConnection connection_rt = null;
        private FiFoConnection connection_rf_raw = null;
        private int ispdaq4found = 0;

        public bool AutoRefresh
        { get; set; }

        public DriverAgent DrvAgent
        {
            get { return drvagent; }
        }

        public static PDAQ4Controller GetInstance()
        {
            if (s_instance == null)
                s_instance = new PDAQ4Controller();
            return s_instance;
        }

        private PDAQ4Controller()
        {
            RegisterCompletedObservers = new List<ReadRegistersCompletedCallBack>();
            drvagent = new DriverAgent();
            AutoRefresh = false;
        }

        public void OnFrame(Frame frame)
        {
            Console.WriteLine("Frameblock size = {0}", frame.FrameBlock.Length);

            OnFrameEvent(frame);
        }

        public void OnReadFrame(Frame frame)
        {
            onReadRawEvent(frame);
            // fill the frame.FrameBlock
        }

        public int IsPDAQ4Found
        {
            get { return ispdaq4found; }
            set { Interlocked.Exchange(ref ispdaq4found, value); }
        }

        public IPDAQDevice FindPDAQ4_CARD()
        {
            IPDAQDevice device = drvagent.FindPDAQ4();
            return device;
        }

        public void OpenPDAQ4_CARD()
        {
            drvagent.OpenPDAQ4();
        }

        public void CreateACQChannels(DMAInfo dmainfo)
        {
            FifoInfo[] fifos = drvagent.QueryFIFO();

            ChannelHeader rf_channelheader = new ChannelHeader();
            rf_channelheader.ChannelIndex = 0;
            rf_channelheader.ChannelType = ChannelType.RF_CHANNEL;
            rf_channelheader.FrameHdrSize = 96; // TODO - use type size
            rf_channelheader.FrameRate = 30;
            rf_channelheader.VectorHdrSize = 16;
            rf_channelheader.VectorsPerFrame = (ushort)dmainfo.vector_per_frame;
            rf_channelheader.SamplesPerVector = (ushort)dmainfo.rf_sample_per_vector;
            rf_channelheader.BitsPerSample = 16;
            rf_channelheader.SampleMask = 0x0fff;
            rf_channelheader.SwVersion = 0x0300;
            connection_rf = new Frame_Connection(rf_channelheader, fifos[0]);
            RFFrameSizeChangedEvent((int)dmainfo.vector_per_frame, (int)dmainfo.rf_sample_per_vector);

            ChannelHeader rt_channelheader = new ChannelHeader();
            rt_channelheader.ChannelType = ChannelType.RT_CHANNEL;
            rt_channelheader.ChannelIndex = 1;
            rt_channelheader.FrameHdrSize = 96; // TODO - use type size
            rt_channelheader.FrameRate = 30;
            rt_channelheader.VectorHdrSize = 16;
            rt_channelheader.VectorsPerFrame = (ushort)dmainfo.vector_per_frame;
            rt_channelheader.SamplesPerVector = (ushort)dmainfo.rt_sample_per_vector;
            rt_channelheader.BitsPerSample = 8;

            RTFrameSizeChangedEvent((int)dmainfo.vector_per_frame, (int)dmainfo.rt_sample_per_vector);
            connection_rt = new Frame_Connection(rt_channelheader, fifos[1]);

            //ChannelHeader rf_raw_channelheader = new ChannelHeader();
            //rf_raw_channelheader.ChannelType = ChannelType.RF_RAW_CHANNEL;

            //rf_raw_channelheader.ChannelIndex = 1;
            //rf_raw_channelheader.FrameHdrSize = 0; // TODO - use type size
            //rf_raw_channelheader.FrameRate = 30;
            //rf_raw_channelheader.VectorHdrSize = 0;
            //rf_raw_channelheader.VectorsPerFrame = (ushort)dmainfo.vector_per_frame;
            //rf_raw_channelheader.SamplesPerVector = (ushort)dmainfo.rf_sample_per_vector;
            //rf_raw_channelheader.BitsPerSample = 16;

            //connection_rf_raw = new Diag_Frame_Connection(rf_raw_channelheader, fifos[2]);

            drvagent.PDAQ4ConnectFIFOChannel(connection_rf);
            drvagent.PDAQ4ConnectFIFOChannel(connection_rt);
            //drvagent.PDAQ4ConnectFIFOChannel(connection_rf_raw);
            //ChannelConnected(dmainfo, fifos[2]);
            connection_rf.FrameReadyEvent += new DeviceGenericEvent<Frame>(this.OnFrame);
            connection_rt.FrameReadyEvent += new DeviceGenericEvent<Frame>(this.OnFrame);
            //connection_rf_raw.FrameReadyEvent += new DeviceGenericEvent<Frame>(this.OnReadFrame);
            connection_rt.start();
            connection_rf.start();
            //connection_rf_raw.start();
        }

        internal bool WriteRegisterValue(RegisterModel register, uint num)
        {
            if(drvagent == null)
            {
                return false;
            }


          bool success =  drvagent.WriteRegisterValue((uint)(register.RegisterType), (uint)register.Offset,num);

            return success;

        }

        public void ClosePDAQ4_CARD()
        {
            if (drvagent != null)
            {

                drvagent.StopDMA();
                //drvagent.StopRawRFDMA();
                drvagent.ClosePDAQ4();
            }

            if (connection_rf != null)
            {
                connection_rt.stop();

            }

            if (connection_rt != null)
            {

                connection_rf.stop();
            }
            //connection_rf_raw.stop();
        }

        public DMAInfo GetDMAInfo()
        {
            return drvagent.GetDMAInfo();
        }

        public ArrayList ReadPDAQ4_Registers(UInt32 registerType, UInt32 offset, UInt32 number = 1)
        {

            return drvagent.ReadRegisterByIC(registerType, offset, number);
        }

        public uint ReadPDAQ4_CARD_RegSpaceBytesLength()
        {
            return drvagent.ReadPDAQ4RegSpaceBytesLength();
        }

        public UInt32[] ReadPDAQ4_CARD_RegSpace(UInt32 start_reg_offset, UInt32 reg_number_to_red)
        {
            return drvagent.ReadPDAQ4RegSpace(start_reg_offset, reg_number_to_red);
        }

        public void WritePDAQ4_CARD_Reg(uint reg_address_byte4_offset, uint value)
        {
            drvagent.WritePDAQ4Reg(reg_address_byte4_offset, value);
        }

        public void WritePDAQ4_CARD_RegBlock(uint StartBARAddress, uint[] dataspace)
        {
            drvagent.WritePDAQ4RegBlock(StartBARAddress, dataspace);
        }

        public void StartDMA()
        {
            // drvagent.PDAQ4Reset();
            DMAInfo dmainfo = drvagent.PDAQ4DMAInit();
            CreateACQChannels(dmainfo);
            //drvagent.EnableDMAReadInt(true);
            //drvagent.EnableDMAWriteInt(true);
            //drvagent.EnableHostLatency(true);
            //drvagent.StartRawRFDMA();
            //drvagent.StartDMA();
            drvagent.MotorStart();
        }

        public void StopDMA()
        {
            connection_rf.stop();
            connection_rt.stop();
            //connection_rf_raw.stop();
            //drvagent.EnableDMAReadInt(false);
            //drvagent.EnableDMAWriteInt(false);
            //drvagent.EnableHostLatency(false);
            drvagent.MotorStop();
            drvagent.PDAQ4DisConnectFIFOChannel(connection_rf);
            drvagent.PDAQ4DisConnectFIFOChannel(connection_rt);
            //drvagent.StopDMA();
            //drvagentdrvagent.StopRawRFDMA();
        }

        public float ReadRegisterInterval
        {
            get
            { return this.read_register_thread_interval; }
            set
            {
                this.read_register_thread_interval = value;
            }
        }
    }

    public class FrameQueue
    {
        public FrameQueue(BinaryWriter wr, int bytes)
        {

            _bytes = bytes;
            _wr = wr;
            _evData = new ManualResetEvent(false);
            _queue = new Queue<byte[]>();
        }

        ~FrameQueue()
        {
            Stop();
            _queue = null;
            _evData.Dispose();
        }

        public void Start()
        {
            _queue.Clear();
            //if (_thread == null)
            //{
            //    _thread = new Thread(new ThreadStart(this.QueueThread));
            //    _thread.SetApartmentState(ApartmentState.MTA);
            //}
            //_thread.Start();

            if(_task == null)
            {


            _cancellationTokenSource = new CancellationTokenSource();

            _task = new Task(QueueThread, _cancellationTokenSource.Token);

            _task.Start();
            }
        }

        public void Stop()
        {
            
            //if (_thread != null)
            //{
            //    _thread.Abort();
            //    while (_thread.IsAlive) Thread.Sleep(10);
            //    _thread = null;
            //}

            if(_task != null)
            {
                _cancellationTokenSource.Cancel();
                _task = null;
                _cancellationTokenSource = null;
            }
        }

        public void OnFrameData(Frame f)
        {
            byte[] ar = new byte[_bytes];

            Array.Copy(f.FrameBlock, ar, _bytes);


            _queue.Enqueue(ar);
            _evData.Set();
        }

        private Queue<byte[]> _queue;
        private ManualResetEvent _evData;
        private Thread _thread;
        private Task _task;
        private CancellationTokenSource _cancellationTokenSource;
        private int _bytes;
        private BinaryWriter _wr;

        private void QueueThread()
        {
            

            byte[] f = null;
            try
            {
                while (true)
                {
                    if (_cancellationTokenSource.IsCancellationRequested)
                    {
                        return;
                    }

                    _evData.WaitOne();

                    while (_queue.Count > 0)
                    {

                        f = _queue.Dequeue();

                        _wr.Write(f);
                        f = null;
                        _wr.Flush();
                    }
                    _evData.Reset();
                }
            }
            catch (ThreadAbortException)
            {
                //while (_queue.Count > 0)
                //{
                //    f = _queue.Dequeue();
                //    _wr.Write(f);
                //    f = null;
                //    _wr.Flush();
                //}
                //Thread.ResetAbort();
            }
        }
    }
}