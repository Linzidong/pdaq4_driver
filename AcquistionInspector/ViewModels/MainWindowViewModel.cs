﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;

namespace AcquistionInspector.ViewModels
{
    public  class MainWindowViewModel:ViewModelBase
    {

        public static bool DoShot { get; set; }

        private RelayCommand _imageShotCommand;

        public RelayCommand ImageShotCommand
        {
            get => _imageShotCommand; set
            {
                _imageShotCommand = value;

                RaisePropertyChanged(() => ImageShotCommand);
            }
        }

        public MainWindowViewModel()
        {
            InitCommands();
        }

        private void InitCommands()
        {
            //ImageShotCommand = new RelayCommand(ImageShot);
        }

        private void ImageShot()
        {
            DoShot = true;
        }
    }
}
