﻿using AcquistionInspector.Models;
using AcquistionInspector.Utils;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using PDAQ4DriverAgent.PDAQ4Implementation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace AcquistionInspector.ViewModels
{
    public class RegisterListPanelViewModel : ViewModelBase
    {

        private Helper _helper = Helper.GetInstance();

        private PDAQ4Controller _controller = PDAQ4Controller.GetInstance();

        public static List<string> REGISTER_NAME_LIST = new List<string>() {

         "FRAMER_VERSION_REG",
         "FRAMER_STATUS_REG",
         "FRAMER_CONTROL_REG",
         "FRAMER_MEMORY_BASE",
         "FRAMER_MEMORY_LENGTH",
         "FRAMER_MSI_DATA",
         "",
         "",
        };


        private RelayCommand<RegisterModel> _selectWriteRegisterCommand;

        public RelayCommand<RegisterModel> SelectWriteRegisterCommand
        {
            get => _selectWriteRegisterCommand; set
            {
                _selectWriteRegisterCommand = value;

                RaisePropertyChanged(() => SelectWriteRegisterCommand);
            }
        }
        private RelayCommand<RegisterModel> _readRegisterCommand;

        public RelayCommand<RegisterModel> ReadRegisterCommand
        {
            get => _readRegisterCommand; set
            {
                _readRegisterCommand = value;

                RaisePropertyChanged(() => ReadRegisterCommand);
            }
        }

        private RelayCommand<RegisterModel> _resetRegisterCommand;

        public RelayCommand<RegisterModel> ResetRegisterCommand
        {
            get => _resetRegisterCommand; set
            {
                _resetRegisterCommand = value;

                RaisePropertyChanged(() => ResetRegisterCommand);
            }
        }

        private RegisterModel _selectToWriteRegister;

        public RegisterModel SelectToWriteRegister
        {
            get => _selectToWriteRegister;
            set
            {
                _selectToWriteRegister = value;
                RaisePropertyChanged(() => SelectToWriteRegister);

            }
        }


        private RelayCommand<string> _writeRegisterCommand;

        public RelayCommand<string> WriteRegisterCommand
        {
            get => _writeRegisterCommand; set
            {
                _writeRegisterCommand = value;

                RaisePropertyChanged(() => WriteRegisterCommand);
            }
        }

        private RelayCommand _readAllRegisterCommand;

        public RelayCommand ReadAllRegisterCommand
        {
            get => _readAllRegisterCommand; set
            {
                _readAllRegisterCommand = value;

                RaisePropertyChanged(() => ReadAllRegisterCommand);
            }
        }


        private bool _writePopIsOpen;

        public bool WritePopIsOpen
        {
            get => _writePopIsOpen;
            set
            {
                _writePopIsOpen = value;
                RaisePropertyChanged(() => WritePopIsOpen);

            }
        }

        private bool _isDecMode = true;

        public bool IsDecMode
        {
            get => _isDecMode;
            set
            {
                _isDecMode = value;
                RaisePropertyChanged(() => IsDecMode);

            }
        }



        private void ReadRegisterList(RegisterType type, uint offset, uint number
            )
        {

            ObservableCollection<RegisterModel> registerList = null;
            Type nameType = null;

            switch (type)
            {
                case RegisterType.RfFramer:
                    registerList = RfFramerRegisterList;
                    nameType = typeof(RegisterName_RfRtFramer);
                    break;
                case RegisterType.RtFramer:
                    registerList = RtFramerRegisterList;
                    nameType = typeof(RegisterName_RfRtFramer);
                    break;
                case RegisterType.DMA:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            var list = _controller.DrvAgent.ReadRegisterByIC((uint)type, offset, number);

            uint i = 0;


            foreach (var item in list)
            {
                if (item is RegisterMd md)
                {

                    
                    registerList.Add(new RegisterModel()
                    {
                        ReadOnly = EnumUtil.GetEnumReadOnly((RegisterName_RfRtFramer)(i * 4)),
                        RegisterType = type,
                        Value = md.value,
                        Address = md.address,
                        Name = Enum.GetName(nameType, i * 4),
                        Offset = i * 4,
                        Description = EnumUtil.GetEnumDescription((RegisterName_RfRtFramer)(i * 4)),
                    });
                }
                i++;
            }
        }

     

        private PDAQ4Controller controller = PDAQ4Controller.GetInstance();



        public ObservableCollection<RegisterModel> RtFramerRegisterList { get; set; } = new ObservableCollection<RegisterModel>();
        public ObservableCollection<RegisterModel> RfFramerRegisterList { get; set; } = new ObservableCollection<RegisterModel>();
        public ObservableCollection<RegisterModel> DMARegisterList { get; set; } = new ObservableCollection<RegisterModel>();

        public RegisterListPanelViewModel()
        {
            RegisterCommands();
            //var msi_data = controller.DrvAgent.ReadRegisterValue(RegCon.RT_FRAMER_OFFSET + 0x14);
        }

        private void RegisterCommands()
        {
            ResetRegisterCommand = new RelayCommand<RegisterModel>(ResetRegisterExe);

            ReadAllRegisterCommand = new RelayCommand(() =>
            {
                ReadRegisterList(RegisterType.RfFramer, 0, RegCon.RT_FRAMER_REG_COUNT);
                ReadRegisterList(RegisterType.RtFramer, 0, RegCon.RF_FRAMER_REG_COUNT);


            });

            ReadRegisterCommand = new RelayCommand<RegisterModel>(reg => {


                var list = _controller.DrvAgent.ReadRegisterByIC((uint)reg.RegisterType, (uint)reg.Offset, 1);

                if(list.Count < 1)
                {
                    _helper.Error(string.Format("Read Register:'{0}' failed", reg.Name));
                }
                else
                {
                    var registerInfo = (RegisterMd)list[0];

                    reg.Value = registerInfo.value;

                    _helper.Info(string.Format("Read Register:'{0}' succeed,value = 0x{1:X8}", reg.Name,reg.Value));
                    //Message success TODO
                }


            });



            SelectWriteRegisterCommand = new RelayCommand<RegisterModel>(reg =>
            {

                SelectToWriteRegister = reg;
                WritePopIsOpen = true;

            });

            WriteRegisterCommand = new RelayCommand<string>(value =>
            {


                if (SelectToWriteRegister == null)
                {
                    MessageBox.Show("No selected Register,Please try again~", "Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
                }

                bool pass = false;
               
                uint num = 0;

                NumberStyles numerStyle = IsDecMode ? NumberStyles.None : NumberStyles.HexNumber;

                CultureInfo us = CultureInfo.GetCultureInfo("en-US");

                pass = UInt32.TryParse(value.Trim(), numerStyle, us.NumberFormat, out num);

                if (pass)
                {

                    WritePopIsOpen = false;
                    bool success =  controller.WriteRegisterValue(SelectToWriteRegister, num);

                    if (success)
                    {
                        ReadRegisterCommand.Execute(SelectToWriteRegister);
                        _helper.Info(string.Format("Register:'{0}'has written value {1:X8}", SelectToWriteRegister.Name, num));
                        
                        

                    }
                    else
                    {
                        _helper.Error(string.Format("Register:'{0}' write value failed", SelectToWriteRegister.Name));
                        
                    }
                }
                else
                {
                    MessageBox.Show("The input content is illegal~", "Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
                }

            });
        }

        private void ResetRegisterExe(RegisterModel reg)
        {
            if (reg.ReadOnly)
            {
                _helper.Info(string.Format("This Register is readonly"));
                return;
            }


            List<UInt32> defaultValues = null;
            UInt32 value = 0;

            bool noValidValue = false;
            switch (reg.RegisterType)
            {
                case RegisterType.RfFramer:

                    defaultValues = EnumUtil.GetEnumDefaultValues((RegisterName_RfRtFramer)reg.Offset);
                    if(defaultValues == null || defaultValues.Count < 1)
                    {
                        noValidValue = true;
                    }
                    else
                    {
                        value = EnumUtil.GetEnumDefaultValues((RegisterName_RfRtFramer)reg.Offset)[0];
                    }
                    break;
                case RegisterType.RtFramer:
                    defaultValues = EnumUtil.GetEnumDefaultValues((RegisterName_RfRtFramer)reg.Offset);
                    if (defaultValues == null || defaultValues.Count < 2)
                    {
                        noValidValue = true;
                    }
                    else
                    {
                        value = EnumUtil.GetEnumDefaultValues((RegisterName_RfRtFramer)reg.Offset)[1];
                    }
                    break;
                case RegisterType.DMA:
                    return;
                    break;
                default:
                    break;
            }

            if (noValidValue)
            {
                MessageBox.Show("No default value for this register now", "Error", MessageBoxButton.OK,
                        MessageBoxImage.Error);

                return;
                //_helper.Error(string.Format("Register:'{0}'no default value for this register now", SelectToWriteRegister.Name));
            }

            bool success = controller.WriteRegisterValue(reg, value);

            if (success)
            {

                _helper.Info(string.Format("Register:'{0}'reset to value {1:X8} succeed", SelectToWriteRegister.Name, value));
            }
            else
            {
                _helper.Error(string.Format("Register:'{0}' reset value failed", SelectToWriteRegister.Name));
            }

        }
    }
}
