﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AcquistionInspector
{
    
    /// <summary>
    /// Interaction logic for EditPageOffset.xaml
    /// </summary>
    /// 
    public partial class EditPageOffsetWindow : Window
    {
        private UInt32 __startaddress;
        private UInt32 __maxAddress;
        public UInt32  StartAddress
        {
            get
            { return __startaddress; }
            set
            { __startaddress = value; }
        }
        
        public EditPageOffsetWindow(string current_start_address,UInt32 pagesize,UInt32 maxAddress)
        {
            InitializeComponent();
            StartAddress = Convert.ToUInt32(current_start_address, 16);
            __maxAddress = maxAddress;
            this.TextBox_StartAddress.Text = string.Format("{0:X}", StartAddress);
            
        }

        

        private new void PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int hexNumber;
            e.Handled = !(int.TryParse(e.Text, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out hexNumber) );
        }

        private void OnOk(object sender, RoutedEventArgs e)
        {
            int hexNumber;
            int.TryParse(this.TextBox_StartAddress.Text, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out hexNumber);
            StartAddress = Convert.ToUInt32(this.TextBox_StartAddress.Text, 16);
            if (((hexNumber % 4 == 0) && (hexNumber <= __maxAddress - PDAQ4Controller.BYTE_PER_PAGE)))
            {
                this.DialogResult = true;
            }
               
            else
            {
                this.TextBox_StartAddress.BorderBrush = Brushes.Red;

            }

        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void TextBox_StartAddress_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
