﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Threading;
namespace AcquistionInspector
{
    /// <summary>
    /// Interaction logic for RegistermappingPanel.xaml
    /// </summary>
    /// 
   
    public enum ACCESS_TYPE
    {
        READ_ONLY,
        READ_WRITE
    }
    public class MappingRegister : INotifyPropertyChanged

    {
        
        private UInt32 value;
        public String RegisterName{get;set;}
        public UInt32 BarAddress{get;set;}
        public String Description {get;set;}
        public int DwordLength { get; set; }
        public ACCESS_TYPE AccessType { get; set; }
        public MappingRegister()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public UInt32 Value
        {
            get
            { return value; }
            set
            {
                this.value = value;
                // Call OnPropertyChanged whenever the property is updated
                OnPropertyChanged("Value");
            }
        }

        // Create the OnPropertyChanged method to raise the event 
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }

   
     
    public partial class RegisterMappingPanel : UserControl
    {
        private Thread registers_read_thread;
        private ObservableCollection<MappingRegister> register_table = new ObservableCollection<MappingRegister> ();
        private Helper helper = Helper.GetInstance();
        private PDAQ4Controller pdaqcontrollder = PDAQ4Controller.GetInstance();
        public delegate void ReadRegisterCompleted(object mem);
        public RegisterMappingPanel()
        {
            InitializeComponent();
            LoadRegDefXML();
            this.RegisterMappingTable.ItemsSource = register_table;
            //pdaqcontrollder.RegisterReadRegisterObserver(OnReadRegisterCompleted);
        }

       
        private void RegisterByteTable_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            

            MappingRegister register = (MappingRegister)RegisterMappingTable.SelectedItem;
            if (register.AccessType == ACCESS_TYPE.READ_WRITE)
            {
                RegisterEditWindow wind = new RegisterEditWindow();
                wind.Top = e.GetPosition(this).Y;
                wind.Left = e.GetPosition(this).X;
                wind.Title = string.Format("BAR_ADDRESS:{0:X8}", register.BarAddress);
                wind.SetRegisterToWrite(register.Value, register.BarAddress / sizeof(UInt32));
                wind.ShowDialog();
                
            }
          


        }

        private void LoadRegDefXML()
        {
            try
            {
                System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
                XmlDocument doc = new XmlDocument();
                doc.Load(a.GetManifestResourceStream("AcquistionInspector.regdef.xml"));
                XmlNodeList regnodelist = doc.SelectNodes("/RegDef/Reg");

                foreach (XmlNode regnode in regnodelist)
                {

                    MappingRegister mapreg = new MappingRegister();

                    mapreg.BarAddress = Convert.ToUInt32(regnode.Attributes["bar_address"].Value, 16);
                    mapreg.RegisterName = regnode.Attributes["name"].Value;
                    mapreg.DwordLength = Convert.ToInt16(regnode.Attributes["dword_length"].Value);
                    if (regnode.Attributes["access"].Value.Equals("RO"))
                        mapreg.AccessType = ACCESS_TYPE.READ_ONLY;
                    else if (regnode.Attributes["access"].Value.Equals("RW"))
                        mapreg.AccessType = ACCESS_TYPE.READ_WRITE;
                    XmlNode commentnode = regnode.SelectSingleNode("Comment");
                    mapreg.Description = commentnode.InnerText;
                    register_table.Add(mapreg);

                }
            }
                catch(Exception e)
                {
                    helper.Error(e.Message);
                }

            }

    

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (registers_read_thread != null && registers_read_thread.IsAlive)
            {
                registers_read_thread.Abort();
                while (registers_read_thread.IsAlive) Thread.Sleep(10);
                registers_read_thread = null;
            }
            

            registers_read_thread = new Thread((ThreadStart)ReadRegisters);
            registers_read_thread.Start();
            
        }
        public void RefreshRegistersManually()
        {
            if (pdaqcontrollder.IsPDAQ4Found == 1 && !pdaqcontrollder.AutoRefresh)
            {
                foreach (MappingRegister register in register_table)
                {

                    if (register.DwordLength != 1)
                        throw new Exception(string.Format("Register {0} Length is not DWORD", register.RegisterName));
                    uint[] mem = pdaqcontrollder.ReadPDAQ4_CARD_RegSpace(register.BarAddress / 4, 1);
                    this.Dispatcher.Invoke(
                    (Action)delegate
                    {
                        register.Value = mem[0];
                    });

                }
            }
        
        }
        void ReadRegisters()
        {

            while (true)
            {

                try
                {
                    if (pdaqcontrollder.IsPDAQ4Found == 1 && pdaqcontrollder.AutoRefresh)
                    {
                        foreach (MappingRegister register in register_table)
                        {

                            if (register.DwordLength != 1)
                                throw new Exception(string.Format("Register {0} Length is not DWORD", register.RegisterName));
                            uint[] mem = pdaqcontrollder.ReadPDAQ4_CARD_RegSpace(register.BarAddress / 4, 1);
                            this.Dispatcher.Invoke(
                            (Action)delegate
                            {
                                register.Value = mem[0];
                            });

                        }

                    }
                   
                    Thread.Sleep((int)(pdaqcontrollder.ReadRegisterInterval * 1000));
                }

                catch (ThreadAbortException)
                {

                }

            }
        }

        private void OnUnLoad(object sender, RoutedEventArgs e)
        {
            if (registers_read_thread != null && registers_read_thread.IsAlive)
            {
                registers_read_thread.Abort();
                while (registers_read_thread.IsAlive) Thread.Sleep(10);
                registers_read_thread = null;
            }
        }

        public void OnDeviceFound(int isfound)
        {
            if (isfound == 1)
                this.IsEnabled = true;
            else
                this.IsEnabled = false;

        }
     }
    
}
