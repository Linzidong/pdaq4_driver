﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Timers;
using AcquistionInspector.ViewModels;

namespace AcquistionInspector
{
    /// <summary>
    /// Interaction logic for ImagingPanel.xaml
    /// </summary>
    public partial class ImagingPanel : UserControl
    {
        private int m_bShowImage = 1;
        
        private byte[] m_ivusData;
        private bool _scan = true;
        private WriteableBitmap m_writeableBitmap;
        Thread simulation_thread = null;
        Random r1 = new Random(100);
        private int __vectorperframe =  256;
        private int __samplepervector = 256;
        private PDAQ4Controller controller = PDAQ4Controller.GetInstance();
        public ImagingPanel()
        {
            InitializeComponent();
            
            //OnChangeViewMode();
            controller.OnFrameEvent += OnNewFrame;
            controller.RTFrameSizeChangedEvent += OnRTFrameSizeChanged;
            m_writeableBitmap = new WriteableBitmap(256, 256, 96, 96, PixelFormats.Gray8, null);
            OnChangeViewMode();
            /*
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(SimulationTest);
          
            aTimer.Interval = 25;
            aTimer.Enabled = true;
            */
        }

        public void OnRTFrameSizeChanged(int new_vectorperframe,int new_samplespervector)
        { 
            controller.OnFrameEvent -= OnNewFrame;
            __vectorperframe = new_vectorperframe;
            __samplepervector = new_samplespervector;
            m_ivusData = new byte[__vectorperframe * __samplepervector];
            m_writeableBitmap = new WriteableBitmap(__samplepervector,__vectorperframe, 96, 96, PixelFormats.Gray8, null);
            OnChangeViewMode();
            controller.OnFrameEvent += OnNewFrame;
            
        }
        
        public void OnNewFrame(PDAQ4DriverInterface.Frame frame)
        {
            if (frame.ChannelHdr.ChannelType == PDAQ4DriverInterface.ChannelType.RT_CHANNEL)
            {

                //if (MainWindowViewModel.DoShot)
                //{
                //    var imageBt = frame.FrameBlock;
                //    var sb = new StringBuilder(); 
                //    var offset = 0;
                //    foreach(var b in imageBt)
                //    {
                //        sb.Append(Convert.ToString(b, 16));
                //        offset++;
                //        if(offset % 4 == 0)
                //        {
                //            sb.AppendLine();
                //        }
                //    }

                //    var str = sb.ToString();

                //    var folder = AppDomain.CurrentDomain.BaseDirectory + "ImageShot\\";

                //    var dir = new DirectoryInfo(folder);
                //    if (!dir.Exists)
                //    {
                //        dir.Create();
                //    }
                //    var path = folder + DateTime.Now.Ticks + ".dat";
                //    FileStream tempFile = new FileStream(path, FileMode.Create);

                //    byte[] buf = Encoding.UTF8.GetBytes(str);
                //    tempFile.Write(buf, 0, buf.Length);

                //    tempFile.Flush();
                //    tempFile.Close();

                //    MainWindowViewModel.DoShot = false;

                //}
                this.Dispatcher.BeginInvoke((OnFrame)Imaging, frame);
            }
            

        }
        public void OnChangeViewMode()
        {

            Vector3DCollection myNormalCollection = new Vector3DCollection();
            Point3DCollection myPositionCollection = new Point3DCollection();
            PointCollection myTextureCoordinatesCollection = new PointCollection();
            Int32Collection myTriangleIndicesCollection = new Int32Collection();

            m_ringMesh.Normals = myNormalCollection;

            // Create a collection of vertex positions for the MeshGeometry3D. 
            // There will be totally VectorPerFrame*2 vetexs
            if (_scan)
            { 
                // The normal of the vetex is direct to the view.
                for (int i = 0; i < __vectorperframe * 2; i++)
                {
                    myNormalCollection.Add(new Vector3D(0, 0, 1));
                }
                for (int i = 0; i < __vectorperframe; i++)
                {
                    double di = (double)i;

                    double x = Math.Cos(di * Math.PI * 2.0 / (__vectorperframe-1.0));
                    double y = -Math.Sin(di * Math.PI * 2.0 / (__vectorperframe - 1.0));

                    myPositionCollection.Add(new Point3D(x * 0.05, y * 0.05, 0));
                    myPositionCollection.Add(new Point3D(x, y, 0));

                    myTextureCoordinatesCollection.Add(new Point(0, di / (__vectorperframe - 1.0)));
                    myTextureCoordinatesCollection.Add(new Point(1, di / (__vectorperframe - 1.0)));
                }
                m_ringMesh.Positions = myPositionCollection;
                m_ringMesh.TextureCoordinates = myTextureCoordinatesCollection;

                for (int i = 0; i < (__vectorperframe - 1); i++)
                {
                    myTriangleIndicesCollection.Add(i * 2);
                    myTriangleIndicesCollection.Add(i * 2 + 2);
                    myTriangleIndicesCollection.Add(i * 2 + 1);
                    myTriangleIndicesCollection.Add(i * 2 + 1);
                    myTriangleIndicesCollection.Add(i * 2 + 2);
                    myTriangleIndicesCollection.Add(i * 2 + 3);
                }
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    myNormalCollection.Add(new Vector3D(0, 0, 1));
                }

                myPositionCollection.Add(new Point3D(-1, -1, 0));
                myPositionCollection.Add(new Point3D(-1, 1, 0));
                myPositionCollection.Add(new Point3D(1, -1, 0));
                myPositionCollection.Add(new Point3D(1, 1, 0));

                myTextureCoordinatesCollection.Add(new Point(0, 0));
                myTextureCoordinatesCollection.Add(new Point(0, 1));
                myTextureCoordinatesCollection.Add(new Point(1, 0));
                myTextureCoordinatesCollection.Add(new Point(1, 1));
                m_ringMesh.Positions = myPositionCollection;
                m_ringMesh.TextureCoordinates = myTextureCoordinatesCollection;

                myTriangleIndicesCollection.Add(0);
                myTriangleIndicesCollection.Add(2);
                myTriangleIndicesCollection.Add(1);
                myTriangleIndicesCollection.Add(1);
                myTriangleIndicesCollection.Add(2);
                myTriangleIndicesCollection.Add(3);
            }
            m_ringMesh.TriangleIndices = myTriangleIndicesCollection;

            // Apply the mesh to the geometry model.
            m_ringModel.Geometry = m_ringMesh;

            DiffuseMaterial myMaterial = new DiffuseMaterial(new ImageBrush(m_writeableBitmap));
            m_ringModel.Material = myMaterial;


        }
        public void DrawImage(byte[] data)
        {
            Marshal.Copy(data, 0, m_writeableBitmap.BackBuffer, __vectorperframe * __samplepervector);

            try
            {
                m_writeableBitmap.Lock();
                m_writeableBitmap.AddDirtyRect(new Int32Rect(0, 0, __samplepervector,__vectorperframe));
            }
            finally
            {
                m_writeableBitmap.Unlock();
            }
        }
        void SimulationTest(object sender, ElapsedEventArgs e)
        {   
            

            this.Dispatcher.Invoke((Action)delegate
            {

                Random r = new Random(r1.Next(1000));
                byte[] data = new byte[__vectorperframe * __samplepervector];
                for (int i = 0; i < __vectorperframe; i++)
                {
                    for (int j = 0; j < __samplepervector; j++)
                    {
                        data[i * __samplepervector + j] = (byte)r.Next(255);
                    }
                }
                DrawImage(data);  
            });


           
        }
        public void Imaging(PDAQ4DriverInterface.Frame f)
        {
            double time = 1.0e-5 * (f.HwFrameTime - f.PrevHwFrameTime);

            if (m_bShowImage != 0)
            {
                byte[] data = new byte[__vectorperframe * __samplepervector];
                for (int i = 0; i < __vectorperframe; i++)
                {
                    PDAQ4DriverInterface.Vector v = (PDAQ4DriverInterface.Vector)f.GetVector(i);

                    for (int j = 0; j < __samplepervector; j++)
                    {
                        data[i * __samplepervector + j] = v.GetSample((j) % __samplepervector);
                    }
                }
                try
                {
                    DrawImage(data);
                }
                catch
                {
                }



            }
        }
    }
}