﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PDAQ4DriverInterface;
using System.IO;
using System.Globalization;
using System.Windows.Forms;
using PDAQ4DriverAgent.PDAQ4Implementation;
using System.Runtime.InteropServices;
namespace AcquistionInspector
{
    /// <summary>
    /// Interaction logic for SettingPanel.xaml
    /// </summary>
    public partial class SettingPanel : System.Windows.Controls.UserControl
    {
        private Helper helper = Helper.GetInstance();
        private PDAQ4Controller pdaqcontroller = PDAQ4Controller.GetInstance();
        private BinaryWriter m_envArchiver;
        private BinaryWriter m_rfArchiver;
        private FrameQueue _envQue;
        private FrameQueue _rfQue;
        private IvusFileHeader _envHdr = new IvusFileHeader(ChannelType.RT_CHANNEL);
        private IvusFileHeader _rfHdr = new IvusFileHeader(ChannelType.RF_CHANNEL);
        private bool m_bRfHeaderOnly;
        private bool m_bEnvHeaderOnly;
        private ushort _envFrames;
        private ushort _rfFrames;
        private FrameViewer _fv;
        PDAQ4Controller controller;
        CatheterTable cathertable = null;
        BinaryReader __reader;
        FileStream _stream;
        Raw_RF_DataManager raw_rfdata_mgr = null;
        public SettingPanel()
        {
            InitializeComponent();
            controller = PDAQ4Controller.GetInstance();
            for (UInt32 i = 1; i <= 31; i++)
                cbframes.Items.Add(i.ToString());
            controller.RFFrameSizeChangedEvent += OnRFFrameSizeChanged;
            controller.RTFrameSizeChangedEvent += OnRTFrameSizeChanged;
            //controller.ChannelConnected += PreLoadDMABuffer;
            controller.onReadRawEvent += OnDMAReadRawRfFrame;

        }
        private void OnRFFrameSizeChanged(int vector_per_frame, int sample_per_vector)
        {



        }

        private void OnRTFrameSizeChanged(int vector_per_frame, int sample_per_vector)
        { }

        private void OnFrame(PDAQ4DriverInterface.Frame frame)
        {


            if (frame.ChannelHdr.ChannelType == ChannelType.RF_CHANNEL && _rfQue != null)
            {
                _rfFrames++;
                _rfQue.OnFrameData(frame);
            }
            else if (frame.ChannelHdr.ChannelType == ChannelType.RT_CHANNEL && _envQue != null)
            {
                _envFrames++;
                _envQue.OnFrameData(frame);
            }
            //else
            //{

            //    //System.Diagnostics.Trace.WriteLine(String.Format("Frame type = {0}", frame.ChannelType));
            //}

        }
        private void UpdatePDAQCardInfo()
        {
            try
            {
                IPDAQDevice CARD = pdaqcontroller.FindPDAQ4_CARD();
                this.PID.Text = string.Format("{0:X}", CARD.PID);
                this.VID.Text = string.Format("{0:X}", CARD.VID);
                this.DeviceName.Text = CARD.DeviceName;
            }
            catch (Exception e)
            {
                helper.Error(e.Message);
            }

        }


        private void OnInitialized(object sender, EventArgs e)
        {
            UpdatePDAQCardInfo();
        }
        private void CloseArchivers()
        {
            if (m_envArchiver != null)
            {
                _envQue.Stop();
                m_envArchiver.Flush();
                //m_envArchiver.Seek(0, SeekOrigin.Begin);
                //_envHdr.Frames = _envFrames;
                //m_envArchiver.Write(_envHdr.AsByteArray, 0, 14);
                //m_envArchiver.Flush();
                m_envArchiver.Close();
                m_envArchiver = null;
                ckBoxSaveEnv.IsChecked = false;
                _envQue = null;
            }
            if (m_rfArchiver != null)
            {
                _rfQue.Stop();
                m_rfArchiver.Flush();
                //m_rfArchiver.Seek(0, SeekOrigin.Begin);
                //_rfHdr.Frames = _rfFrames;
                //m_rfArchiver.Write(_rfHdr.AsByteArray, 0, 14);
                //m_rfArchiver.Flush();
                m_rfArchiver.Close();
                m_rfArchiver = null;
                ckBoxSaveRf.IsChecked = false;
                _rfQue = null;
            }
            controller.OnFrameEvent -= OnFrame;
        }
        public void StartAcq()
        {
            OpenArchivers();
            ckBoxSaveEnv.IsEnabled = false;
            viewarchive.IsEnabled = false;
            ckBoxSaveRf.IsEnabled = false;
            checkBoxRfHdr.IsEnabled = false;
            checkBoxEnvHdr.IsEnabled = false;

        }
        public void StopAcq()
        {
            CloseArchivers();
            ckBoxSaveEnv.IsEnabled = true;
            viewarchive.IsEnabled = true;
            ckBoxSaveRf.IsEnabled = true;
            checkBoxRfHdr.IsEnabled = true;
            checkBoxEnvHdr.IsEnabled = true;

        }
        private void OpenArchivers()
        {
            DMAInfo dmainfo = controller.GetDMAInfo();

            _envFrames = 0;
            _rfFrames = 0;
            Stream st;
            DateTime t = DateTime.Now;


            String name = t.ToString("dd-MMM-yyyy", DateTimeFormatInfo.InvariantInfo);

            name += String.Format("_{0}H{1}M{2}S.bin", t.Hour, t.Minute, t.Second);
            string basefolder = "c:\\temp";

            if ((bool)ckBoxSaveEnv.IsChecked || (bool)ckBoxSaveRf.IsChecked)
            {
                controller.OnFrameEvent += OnFrame;
                FolderBrowserDialog dlg = new FolderBrowserDialog();
                //dlg.RootFolder = Environment.SpecialFolder.MyComputer;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    basefolder = dlg.SelectedPath;
                }
            }

            if ((bool)ckBoxSaveEnv.IsChecked)
            {
                st = File.Open(basefolder + "\\env_" + name, FileMode.Create, System.IO.FileAccess.ReadWrite);
                m_envArchiver = new BinaryWriter(st);

                if (m_bEnvHeaderOnly)
                {
                    _envHdr.HeaderOnly = true;
                    _envHdr.FrameSize = 96;

                }
                else
                {
                    _envHdr.HeaderOnly = false;
                    _envHdr.FrameSize = (int)(dmainfo.rt_frame_size);
                    _envHdr.BytesPerSample = 1;
                    _envHdr.VectorPerFrame = (int)dmainfo.vector_per_frame;
                    _envHdr.SamplePerVector = (int)dmainfo.rt_sample_per_vector;
                }

                //m_envArchiver.Write(_envHdr.AsByteArray);

                _envQue = new FrameQueue(m_envArchiver, _envHdr.FrameSize);
                _envQue.Start();

            }
            if ((bool)ckBoxSaveRf.IsChecked)
            {
                st = File.Open(basefolder + "\\rf_" + name, FileMode.Create, System.IO.FileAccess.ReadWrite);
                m_rfArchiver = new BinaryWriter(st);

                if (m_bRfHeaderOnly)
                {
                    _rfHdr.HeaderOnly = true;
                    _rfHdr.FrameSize = 96;
                }
                else
                {
                    _rfHdr.HeaderOnly = false;
                    _rfHdr.FrameSize = (int)(dmainfo.rf_frame_size);
                    _rfHdr.BytesPerSample = 2;
                    _rfHdr.VectorPerFrame = (int)dmainfo.vector_per_frame;
                    _rfHdr.SamplePerVector = (int)dmainfo.rf_sample_per_vector;
                }

                //m_rfArchiver.Write(_rfHdr.AsByteArray);
                _rfQue = new FrameQueue(m_rfArchiver, _rfHdr.FrameSize);
                _rfQue.Start();
            }
        }

        private void checkBoxRfHdr_Checked(object sender, RoutedEventArgs e)
        {
            m_bRfHeaderOnly = (bool)checkBoxRfHdr.IsChecked;
            this.ckBoxSaveRf.IsChecked = true;
        }

        private void checkBoxEnvHdr_Checked(object sender, RoutedEventArgs e)
        {
            m_bEnvHeaderOnly = (bool)checkBoxEnvHdr.IsChecked;
            this.ckBoxSaveEnv.IsChecked = true;
        }

        public void OnDeviceFound(int isfound)
        {


        }
        private void viewarchive_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openfiledlg = new OpenFileDialog();
            openfiledlg.Filter = "Binary data archive (*.bin)|*.bin";
            openfiledlg.Title = "Select data archive to load";
            if (openfiledlg.ShowDialog() == DialogResult.OK)
            {
                _fv = new FrameViewer();
                _fv.Show();
                _fv.LoadArchive(openfiledlg.FileName);

            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var opencathertabledlg = new OpenFileDialog();
            opencathertabledlg.Filter = "XML config file (*.xml)|*.xml";
            opencathertabledlg.Title = "Select Catheter Profile to load";
            if (opencathertabledlg.ShowDialog() == DialogResult.OK)
            {

                string filename = opencathertabledlg.FileName;
                Cursor = System.Windows.Input.Cursors.Wait;
                try
                {

                    cathertable = CatheterTable.ReadCathTable(filename);
                    cbcathetertable.Items.Clear();
                    foreach (KeyValuePair<uint, Catheter> pair in cathertable.CatheterList)
                    {
                        string cathertablename = String.Format("{0:D2}-", pair.Key) + pair.Value.CatheterName;
                        cbcathetertable.Items.Add(cathertablename);

                    }
                }
                catch (Exception err)
                { helper.Error(err.Message); }
                Cursor = System.Windows.Input.Cursors.Arrow;

            }
        }

        private void cbcathetertable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string s = (string)this.cbcathetertable.SelectedItem;

            s = s.Substring(0, 2);

            if (cathertable != null)
            {
                uint id;

                if (uint.TryParse(s, out id))
                {
                    Catheter cath = cathertable.CatheterList[id];
                    //cath.ImagingModes[0].TRCControl.TGCIndex = new byte[8] { 1, 0, 0, 0, 0, 0, 0, 0 };
                    cath.ImagingModes[0].AcquisitionDelay = cath.ImagingModes[0].TRCControl.SystemDelay[0];
                    //m_dsp.Mode = cath.ImagingModes[0];

                    this.cbdepth.Items.Clear();

                    foreach (uint depth in cath.ImagingModes[0].DepthValue)
                    {
                        double d = cath.CathSize * 1000 / 30 + depth * CatheterTable.SoundSpeed * Math.Cos(Math.PI * cath.xDucerAngle / 180) * 0.005;
                        DepthParam dp = new DepthParam(depth, (float)d);
                        cbdepth.Items.Add(dp.diameter);
                    }
                    if (cbdepth.Items != null && cbdepth.Items.Count > 0)
                        cbdepth.SelectedIndex = (int)cath.ImagingModes[0].DefaultDepth;

                    cbframerate.Items.Clear();
                    for (int n = 5; n <= cath.ImagingModes[0].TRCControl.MotorSpeed; n += 5)
                        cbframerate.Items.Add(n.ToString());

                    cbframerate.SelectedIndex = cbframerate.Items.Count - 1; // default to max

                }
                else
                {
                    throw new Exception("Failed to find catheter data");
                }
            }

        }

        private void on_button_raw_rf_down(object sender, RoutedEventArgs e)
        {
            var openrawrfdatafile = new OpenFileDialog();
            openrawrfdatafile.Filter = "RAW RF Data file (*.bin)|*.bin";
            openrawrfdatafile.Title = "Select RAW RF Data file to load";
            if (openrawrfdatafile.ShowDialog() == DialogResult.OK)
            {

                string filename = openrawrfdatafile.FileName;
                _stream = File.Open(filename, FileMode.Open, System.IO.FileAccess.Read);
                raw_rfdata_mgr = Raw_RF_DataManager.GetRaw_RF_DataManager(_stream);
                m_raw_rf_data_file.Text = filename;
                PreLoadDMABufferForTest();


            }
            else
            {
                m_raw_rf_data_file.Text = "";
            }

        }

        


        private void PreLoadDMABufferForTest()
        {
            if (pdaqcontroller.IsPDAQ4Found == 0)
                return;

            FifoInfo[] fifos = pdaqcontroller.DrvAgent.QueryFIFO();
            FifoInfo fifoinfo = fifos[2];
            if (raw_rfdata_mgr == null)
            {
                return;
            }
            raw_rfdata_mgr.FrameSize = 256*1024*2;
            long totallength = raw_rfdata_mgr.DataSize;
            if (fifoinfo.FrameType != 2)
            {
                throw new Exception("The Frame Type is not correct, only RAW_RF_FRAME is support for preloading dma buffer");
            }
            // Get one raw rf frame size
            long rawrfframesize = 256*1024*2;
            // Fill the fifo with data
            if (totallength % rawrfframesize != 0)
            {
                throw new Exception("The raw rf data file size is not times of the raw rf frame size ");
            }


            for (int i = 0; i < fifoinfo.FifoSize; i++)
            {
                FifoElement element = (FifoElement)fifoinfo.fifo_element_list[i];
                byte[] frameblockdata = raw_rfdata_mgr.NextFrameBlock;
                long blocksize = frameblockdata.Length;
                for (int j = 0; j < element.dma_descriptor_list.Count; j++)
                {
                    HardwareResource hwres = (HardwareResource)element.dma_descriptor_list[j];
                    if (hwres.Bytes >= blocksize)
                    {
                        Marshal.Copy(frameblockdata, 0, hwres.VirtualPointer, frameblockdata.Length);
                        break;
                    }
                    else
                    {
                        Marshal.Copy(frameblockdata, 0, hwres.VirtualPointer, (int)hwres.Bytes);
                        blocksize -= hwres.Bytes;
                    }
                }
            }
        }

        private void PreLoadDMABuffer(DMAInfo dmainfo, FifoInfo fifoinfo)
        {
            if (raw_rfdata_mgr == null)
            {
                return;
            }
            raw_rfdata_mgr.FrameSize = dmainfo.raw_rf_frame_size;
            long totallength = raw_rfdata_mgr.DataSize;
            if (fifoinfo.FrameType != 2)
            {
                throw new Exception("The Frame Type is not correct, only RAW_RF_FRAME is support for preloading dma buffer");
            }
            // Get one raw rf frame size
            long rawrfframesize = dmainfo.raw_rf_frame_size;
            // Fill the fifo with data
            if (totallength % rawrfframesize != 0)
            {
                throw new Exception("The raw rf data file size is not times of the raw rf frame size ");
            }


            for (int i = 0; i < fifoinfo.FifoSize; i++)
            {
                FifoElement element = (FifoElement)fifoinfo.fifo_element_list[i];
                byte[] frameblockdata = raw_rfdata_mgr.NextFrameBlock;
                long blocksize = frameblockdata.Length;
                for (int j = 0; j < element.dma_descriptor_list.Count; j++)
                {
                    HardwareResource hwres = (HardwareResource)element.dma_descriptor_list[j];
                    if (hwres.Bytes >= blocksize)
                    {
                        Marshal.Copy(frameblockdata, 0, hwres.VirtualPointer, frameblockdata.Length);
                        break;
                    }
                    else
                    {
                        Marshal.Copy(frameblockdata, 0, hwres.VirtualPointer, (int)hwres.Bytes);
                        blocksize -= hwres.Bytes;
                    }
                }
            }
        }

        void OnDMAReadRawRfFrame(PDAQ4DriverInterface.Frame frame)
        {
            //Copy the next frame to the read slot of dma fifo
            Array.Copy(raw_rfdata_mgr.NextFrameBlock, frame.FrameBlock, raw_rfdata_mgr.NextFrameBlock.Length);
        }


    }
  public class Raw_RF_DataManager
  {
      private FileStream __stream;
      private BinaryReader __reader;
      private long __internalpos = 0;
      private long __rawrfframesize = 0;
      private static Raw_RF_DataManager mgr = null;
      private Raw_RF_DataManager(FileStream stream)
      {
        __stream = stream;
        __reader = new BinaryReader(__stream);
      }
      public static Raw_RF_DataManager GetRaw_RF_DataManager(FileStream stream)
      {
          if (mgr==null)
              mgr = new Raw_RF_DataManager(stream);
          return mgr;
      }
      public long FrameSize
      {
          set{

              __rawrfframesize = value;
          }
          get
          {
              if (__rawrfframesize == 0)
              {
                  throw new Exception("The raw rf frame size not set yet");
              }
              return __rawrfframesize;
          }

      }
      public long DataSize
      {
          get { 
              return __reader.BaseStream.Length; 
          }
      }
      public byte[] NextFrameBlock
      {    
          get
          {

              __reader.BaseStream.Position = __internalpos;
          byte [] buffer = new byte[FrameSize];
          
          __reader.BaseStream.Read(buffer,0,(int)FrameSize);
          __internalpos+=FrameSize;
          __internalpos=__internalpos%__reader.BaseStream.Length;
          return buffer;
         }
      }
  }














    public class IvusFileHeader
    {
        /// <summary>
        /// Channel Type
        /// </summary>
        public ChannelType Type
        {
            get { return (ChannelType)_data[0]; }
            set { _data[0] = (byte)value; }
        }

        /// <summary>
        /// Only frame headers are stored
        /// </summary>
        public bool HeaderOnly
        {
            get { return (_data[1] != 0); }
            set { _data[1] = (byte)((value == true) ? 0xff : 0); }
        }

        /// <summary>
        /// Total number of frames in this archive
        /// </summary>
        public UInt16 Frames
        {
            get { return BitConverter.ToUInt16(_data, 2); }
            set { Array.Copy(BitConverter.GetBytes(value), 0, _data, 2, 2); }
        }
        /// <summary>
        /// Size in byte of frames (maybe header only)
        /// </summary>
        public int FrameSize
        {
            get { return BitConverter.ToInt32(_data, 4); }
            set { Array.Copy(BitConverter.GetBytes(value), 0, _data, 4, 4); }
        }
        public int VectorPerFrame
        {
            get { return BitConverter.ToInt16(_data, 8); }
            set { Array.Copy(BitConverter.GetBytes(value), 0, _data, 8, 2); }        
        }
        public int SamplePerVector
        {
            get { return BitConverter.ToInt16(_data, 10); }
            set { Array.Copy(BitConverter.GetBytes(value), 0, _data, 10, 2); }            
        }
        public int BytesPerSample
        {
            get { return BitConverter.ToInt16(_data, 12); }
            set { Array.Copy(BitConverter.GetBytes(value), 0, _data, 12, 2); }         
        }
        public byte[] AsByteArray
        {
            get { return (byte[])_data.Clone(); }
        }

        public IvusFileHeader(ChannelType t)
        {
            _data = new byte[14];
            Type = t;
            if (t == ChannelType.RT_CHANNEL)
            { BytesPerSample = 1; }
            else
            { BytesPerSample = 2; }
        }
        public IvusFileHeader(byte[] ar)
        {
            _data = (byte[])ar.Clone();
        }
        private byte[] _data = null;
    }

    internal class DepthParam
    {
        public DepthParam(uint s, float f)
        {
            samples = s;
            f /= 1000;
            diameter = f.ToString(".## mm");
        }
        public uint samples;
        public string diameter;
    }


 
}
