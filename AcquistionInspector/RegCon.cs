﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
// ReSharper disable InconsistentNaming

namespace AcquistionInspector
{
    class RegCon
    {
        public const uint RF_FRAMER_OFFSET = 0xe0100;
        public const uint RF_FRAMER_REG_COUNT = 64;
        public const uint RF_FRAME_HEADER_SIZE = 96;
        public const uint RF_FRAME_VECTOR_HEADER_SIZE = 16;
        public const uint RF_FRAME_VECTOR_SAMPLE_SIZE = 2;
        public const uint RF_FRAME_VECTOR_SAMPLE_NUMBER = 1024;
        public const uint RF_FRAME_VECTOR_NUMBER = 256;
        public const uint RF_FRAME_SIZE =
            RF_FRAME_HEADER_SIZE + RF_FRAME_VECTOR_NUMBER * (RF_FRAME_VECTOR_HEADER_SIZE + RF_FRAME_VECTOR_SAMPLE_NUMBER * RF_FRAME_VECTOR_SAMPLE_SIZE);
        // GetFrameSize(RF_FRAME_HEADER_SIZE, RF_FRAME_VECTOR_HEADER_SIZE, RF_FRAME_VECTOR_SAMPLE_SIZE, RF_FRAME_VECTOR_SAMPLE_NUMBER, RF_FRAME_VECTOR_NUMBER);

        #region DefaultVaule

        public const uint RF_FRAME_SYNC_PATTERN = 0x5A5A5A5A;
        public const uint RT_FRAME_SYNC_PATTERN = 0xA5A5A5A5;
        public const uint RT_FRAME_CHANNEL_TYPE = 0, RF_FRAME_CHANNEL_TYPE = 2;
        public const uint RT_FRAME_CHANNEL_ID = 1, RF_FRAME_CHANNEL_ID = 2;
        

        #endregion



        public const uint RT_FRAMER_OFFSET = 0xe0300;
        public const uint RT_FRAMER_REG_COUNT = 64;
        public const uint RT_FRAME_HEADER_SIZE = 96;
        public const uint RT_FRAME_VECTOR_HEADER_SIZE = 16;
        public const uint RT_FRAME_VECTOR_SAMPLE_SIZE = 1;
        public const uint RT_FRAME_VECTOR_SAMPLE_NUMBER = 256;
        public const uint RT_FRAME_VECTOR_NUMBER = 256;
        public const uint RT_FRAME_SIZE =
            RT_FRAME_HEADER_SIZE + RT_FRAME_VECTOR_NUMBER * (RT_FRAME_VECTOR_HEADER_SIZE + RT_FRAME_VECTOR_SAMPLE_NUMBER * RT_FRAME_VECTOR_SAMPLE_SIZE);
        //GetFrameSize(RT_FRAME_HEADER_SIZE, RT_FRAME_VECTOR_HEADER_SIZE, RT_FRAME_VECTOR_SAMPLE_SIZE, RT_FRAME_VECTOR_SAMPLE_NUMBER, RT_FRAME_VECTOR_NUMBER);


        public static uint GetFrameSize(uint frameHeaderSize,uint vectorHeaderSize,uint vectorSampleSize,uint vectorSampleNumber,uint vectorNumber)
        {
            return frameHeaderSize + vectorNumber * (vectorHeaderSize + vectorSampleNumber * vectorSampleSize);
        }

    }

    public enum RegisterName_RfRtFramer
    {
        [ReadOnly(true),Description("Version info")]
        FRAMER_VERSION_REG = 0x00,
        [ReadOnly(true),Description("status")]
        FRAMER_STATUS_REG = 0x04,

        FRAMER_CONTROL_REG = 0x08,
        FRAMER_MEMORY_BASE = 0x0C,
        FRAMER_MEMORY_LENGTH = 0x10,
        FRAMER_MSI_DATA = 0x14,
        FRAMER_MSI_ADDR_LOWER = 0x18,
        FRAMER_MSI_ADDR_UPPER = 0x1C,
        reserved0 = 0x20,
        reserved1 = 0x24,
        reserved2 = 0x28,
        reserved3 = 0x2C,
        reserved4 = 0x30,
        reserved5 = 0x34,
        reserved6 = 0x38,
        reserved7 = 0x03C,

        [ReadOnly(true)]
        FRAMER_VH0 = 0x40,
        [ReadOnly(true)]
        FRAMER_VH1 = 0x44,
        [ReadOnly(true)]
        FRAMER_VH2 = 0x48,
        [ReadOnly(true)]
        FRAMER_VH3 = 0x4C,
        reserved8 = 0x50,
        reserved9 = 0x54,
        reserved10 = 0x58,
        reserved11 = 0x05C,
        reserved12 = 0x60,
        reserved13 = 0x64,
        reserved14 = 0x68,
        reserved15 = 0x6C,
        reserved16 = 0x70,
        reserved17 = 0x74,
        reserved18 = 0x78,
        reserved19 = 0x7C,

        [DefaultValue(RegCon.RF_FRAME_SYNC_PATTERN,RegCon.RT_FRAME_SYNC_PATTERN)]
        FRAMER_FH0 = 0x80,

        [DefaultValue(0x01000202, 0x01000001),
         Description("Vector per frame(31:16)\nFrame Channel Type(15:8)\nFrame Channel ID(7:0)")]
        FRAMER_FH1 = 0x84,

        [DefaultValue(RegCon.RF_FRAME_VECTOR_SAMPLE_NUMBER,RegCon.RT_FRAME_VECTOR_SAMPLE_NUMBER),
         Description("Sample per vector")]
        FRAMER_FH2 = 0x88,
        FRAMER_FH3 = 0x8C,
        [ReadOnly(true)]
        FRAMER_FH4 = 0x90,
        [ReadOnly(true)]
        FRAMER_FH5 = 0x94,
        [ReadOnly(true)]
        FRAMER_FH6 = 0x98,
        [ReadOnly(true)]
        FRAMER_FH7 = 0x9C,
        FRAMER_FH8 = 0xA0,

        [DefaultValue(0x00000000,0x00000000)]
        FRAMER_FH9 = 0xA4,
        [DefaultValue(5000, 2*1*5000)]
        FRAMER_FH10 = 0xA8,
        [DefaultValue(0x00000000,0x00000000)]
        FRAMER_FH11= 0xAC,
        [DefaultValue(RegCon.RF_FRAME_SIZE,RegCon.RT_FRAME_SIZE)]
        FRAMER_FH12= 0xB0,
        FRAMER_FH13= 0xB4,

        [DefaultValue(0x00102030, 0x00102030)]
        FRAMER_FH14= 0xB8,
        [DefaultValue(0x40506070, 0x40506070)]
        FRAMER_FH15 = 0xBC,
        [DefaultValue(0x8090A0B0, 0x8090A0B0)]
        FRAMER_FH16 = 0xC0,
        [DefaultValue(0xC0D0E0F0, 0xC0D0E0F0)]
        FRAMER_FH17 = 0xC4,
        FRAMER_FH18= 0xC8,
        FRAMER_FH19= 0xCC,
        FRAMER_FH20= 0xD0,
        FRAMER_FH21= 0xD4,
        FRAMER_FH22= 0xD8,
        FRAMER_FH23= 0xDC,
        reserved20 = 0xE0,
        reserved21 = 0xE4,
        reserved22 = 0xE8,
        reserved23 = 0xEC,
        reserved24 = 0xF0,
        reserved25 = 0xF4,
        reserved26 = 0xF8,
        reserved27 = 0xFC,
    }



    public enum RegisterType
    {
        RfFramer,
        RtFramer,
        DMA
    }

    public class ReadOnlyAttribute : Attribute
    {
        public bool ReadOnly { get; set; }

        public ReadOnlyAttribute(bool readOnly)
        {
            this.ReadOnly = readOnly;
        }
    }

    public class DefaultValueAttribute : Attribute
    {
        public List<UInt32> DefaultValues { get; set; }

        public DefaultValueAttribute(params UInt32[] values)
        {
            DefaultValues = new List<uint>();
            DefaultValues.AddRange(values.ToList());
        }

    }

    public class DescriptionAttribute : Attribute
    {
        public string Description { get; set; }

        public DescriptionAttribute(string desc)
        {
            Description = desc;
        }

    }
}
