/*++

Copyright (c) Microsoft Corporation.  All rights reserved.

	THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
	KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
	PURPOSE.

Module Name:

	IsrDpc.c

Abstract:

	Contains routines related to interrupt and dpc handling.

Environment:

	Kernel mode

--*/

#include "precomp.h"
#include "Trace.h"
#include "IsrDpc.tmh"
//#include "Trace.h"

NTSTATUS
PDAQ4InterruptCreate(
	IN PDEVICE_EXTENSION DevExt
	,
	_In_ PCM_PARTIAL_RESOURCE_DESCRIPTOR    ResourcesRaw,
	_In_ PCM_PARTIAL_RESOURCE_DESCRIPTOR    ResourceTranslated
)

{
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");

	NTSTATUS                    status = STATUS_SUCCESS;
	WDF_INTERRUPT_CONFIG        InterruptConfig;

	PAGED_CODE();

	/*WDF_INTERRUPT_CONFIG_INIT(&InterruptConfig,
		PDAQ4EvtInterruptIsr,
		PDAQ4EvtInterruptDpc);*/

		//InterruptConfig.EvtInterruptEnable = PDAQ4EvtInterruptEnable;
		//InterruptConfig.EvtInterruptDisable = PDAQ4EvtInterruptDisable;

		//InterruptConfig.AutomaticSerialization = TRUE;

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "MessageCount= %d\n", ResourcesRaw->u.MessageInterrupt.Raw.MessageCount);

	for (int i = 0; i < MSG_INT_NUM; i++)
	{
		WDF_INTERRUPT_CONFIG_INIT(&InterruptConfig,
			PDAQ4EvtInterruptIsr,
			PDAQ4EvtInterruptDpc);

		InterruptConfig.InterruptRaw = ResourcesRaw;
		InterruptConfig.InterruptTranslated = ResourceTranslated;

		status = WdfInterruptCreate(DevExt->Device,
			&InterruptConfig,
			WDF_NO_OBJECT_ATTRIBUTES,
			&DevExt->Interrupt[i]);

		//TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Create Interupt No. %d\n", i);
		if (!NT_SUCCESS(status)) {
			TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "WdfInterruptCreate failed: %d\n", status);
			//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfInterruptCreate failed: %d\n", status));
			return status;
		}
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "Create Interupt No. %d\n", i);
	}

	return status;
}

BOOLEAN
PDAQ4EvtInterruptIsr(
	IN WDFINTERRUPT Interrupt,
	IN ULONG        MessageID
)

{
	//WdfInterruptAcquireLock(Interrupt);
	//TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Entry");
	//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Enter ISR ID: %u \n", MessageID));

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "InterruptIsr (u x X d) ID: %u,%x,%X,%d\n", MessageID, MessageID, MessageID, MessageID);

	//WdfInterruptReleaseLock(Interrupt);

	//return TRUE;

	PDEVICE_EXTENSION   devExt;
	devExt = PDAQ4GetDeviceContext(WdfInterruptGetDevice(Interrupt));
	BOOLEAN             isRecognized = FALSE;
	//LONGLONG it;
	//ULONG inc;
	DMA_ITEM* pDmaItem;
	//PPDAQ4_REG pConfig = devExt->ConfigSpace;
	//LARGE_INTEGER irp_time;
	PWrite_DMA_Descriptor_Controller_Registers   pwReq;
	ULONG               lastPtr;
	UNREFERENCED_PARAMETER(MessageID);

	pwReq = (PWrite_DMA_Descriptor_Controller_Registers)(devExt->FpgaDmaDescClrRegWrite);

	//RT Frame prepared
	if (1 == MessageID) {
		isRecognized = TRUE;

		lastPtr = READ_REGISTER_ULONG((PULONG)&pwReq->WR_DMA_LAST_PTR);
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "RT Frame prepared,lastPtr = %u", lastPtr));

		if (lastPtr == MAX_DMA_DESCRIPTOR_ENTRIES - 1) {
			lastPtr = 1;
		}
		else
		{
			lastPtr += 2;
		}

		WRITE_REGISTER_ULONG((PULONG)&pwReq->WR_DMA_LAST_PTR, lastPtr);

		//KeQuerySystemTime(&irp_time);
		//it = KeQueryInterruptTime();
		//inc = KeQueryTimeIncrement();
		/*KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "RT Frame prepared, begin to transfer RF and RT Frame"));

		WdfInterruptQueueDpcForIsr(devExt->Interrupt[MessageID]);

		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Deal with RT Frame Interupt \n"));*/
	}

	if (2 == MessageID) {
		isRecognized = TRUE;
	}

	//DMA Transfer finish
	if (0 == MessageID) {
		isRecognized = TRUE;
		//lastPtr = READ_REGISTER_ULONG((PULONG)&pwReq->WR_DMA_LAST_PTR);
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "DMA Transfer finish");
		pDmaItem = (DMA_ITEM*)ExInterlockedRemoveHeadList(&(devExt->DmaWriteList.queue_list), &(devExt->DmaWriteList.queueLock));

		if (pDmaItem != NULL)    // this can NOT be empty
		{
			ExInterlockedInsertTailList(&(devExt->DmaWriteList.done_list), (PLIST_ENTRY)pDmaItem, &(devExt->DmaWriteList.doneLock));
			//devExt->debug_info.ReadInterruptNum++;
			/*if (WdfInterruptQueueDpcForIsr(Interrupt))
			{
				KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Deferred prodcure call is queued.. \n"));
			}
			else
			{
				KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Deferred prodcure call is not queued \n"));
			}*/
		}

		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "DMA Transfer finish"));

		WdfInterruptQueueDpcForIsr(devExt->Interrupt[MessageID]);

		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Deal with DMA Finish \n"));
	}

	//if (write_intstatus)
	//{
	//	isRecognized = TRUE;

	//	KeQuerySystemTime(&irp_time);
	//	it = KeQueryInterruptTime();
	//	inc = KeQueryTimeIncrement();

	//	//CLEAR_WRITE_FRAME_END_INT(pConfig);
	//	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Clear Write Int Status Register, and DMA write interrupt status value = %d\n", devExt->ConfigSpace->rIntStatus.Write_Frame_End_int));
	//	pDmaItem = (DMA_ITEM*)ExInterlockedRemoveHeadList(&(devExt->DmaWriteList.queue_list), &(devExt->DmaWriteList.queueLock));
	//	if (pDmaItem != NULL)    // this can NOT be empty
	//	{
	//		devExt->debug_info.FIFO_Index++;
	//		devExt->debug_info.FIFO_Index %= (devExt->Frame_FIFO_size);
	//		//if (devExt->FIFO_Index == 0)
	//		//{
	//		//	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "The DMA is finished, and Start again \n"));
	//			//START_DMA(pConfig);
	//		//}

	//		ExInterlockedInsertTailList(&(devExt->DmaWriteList.done_list), (PLIST_ENTRY)pDmaItem, &(devExt->DmaWriteList.doneLock));
	//		devExt->debug_info.WriteInterruptNum++;
	//		if (WdfInterruptQueueDpcForIsr(Interrupt))
	//		{
	//			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Deferred prodcure call is queued.. \n"));
	//		}
	//		else
	//		{
	//			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Deferred prodcure call is not queued \n"));
	//		}
	//	}
	//}
	//else if (read_intstatus == 1)
	//{
	//	isRecognized = TRUE;
	//	KeQuerySystemTime(&irp_time);
	//	it = KeQueryInterruptTime();
	//	inc = KeQueryTimeIncrement();

	//	//CLEAR_READ_FRAME_END_INT(pConfig);
	//	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Clear Read Int Status Register, and DMA read interrupt status value = %d\n", devExt->ConfigSpace->rIntStatus.Read_Frame_End_int));
	//	pDmaItem = (DMA_ITEM*)ExInterlockedRemoveHeadList(&(devExt->DmaReadList.queue_list), &(devExt->DmaReadList.queueLock));
	//	if (pDmaItem != NULL)    // this can NOT be empty
	//	{
	//		ExInterlockedInsertTailList(&(devExt->DmaReadList.done_list), (PLIST_ENTRY)pDmaItem, &(devExt->DmaReadList.doneLock));
	//		devExt->debug_info.ReadInterruptNum++;
	//		if (WdfInterruptQueueDpcForIsr(Interrupt))
	//		{
	//			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Deferred prodcure call is queued.. \n"));
	//		}
	//		else
	//		{
	//			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Deferred prodcure call is not queued \n"));
	//		}
	//	}
	//}
	//else
	//{
	//	//CLEAR_WRITE_FRAME_END_INT(pConfig);
	//	CLEAR_READ_FRAME_END_INT(pConfig);
	//	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Not Interrupt needed, status is not set\n"));
	//	//FPGA_MOTOR_RUN(pConfig, 0);
	//}

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Complete ISR \n"));

	return isRecognized;
}

_Use_decl_annotations_
VOID
PDAQ4EvtInterruptDpc(
	WDFINTERRUPT Interrupt,
	WDFOBJECT    Device
)

{
	/*WdfInterruptAcquireLock(Interrupt);

	WdfInterruptReleaseLock(Interrupt);

	return;*/
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Enter DPC");

	PDEVICE_EXTENSION   devExt;
	PWrite_DMA_Descriptor_Controller_Registers   pwReq;
	ReadWrite_DMA_Status_and_Descriptor_Table_PTR   pDesc;
	ULONG               lastPtr;

	UNREFERENCED_PARAMETER(Device);

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "DPC is called\n"));

	DMA_ITEM* pDmaItem;
	devExt = PDAQ4GetDeviceContext(WdfInterruptGetDevice(Interrupt));
	pDesc = (ReadWrite_DMA_Status_and_Descriptor_Table_PTR)(devExt->DescriptorTableCommonBufferBaseVa);
	pwReq = (PWrite_DMA_Descriptor_Controller_Registers)(devExt->FpgaDmaDescClrRegWrite);

	WdfInterruptAcquireLock(Interrupt);

	//RT Frame prepared
	/*if (Interrupt == devExt->Interrupt[3]) {
	}*/

	//DMA Transfer finish
	if (Interrupt == devExt->Interrupt[0]) {
		while (TRUE)
		{
			pDmaItem = (DMA_ITEM*)ExInterlockedRemoveHeadList(&(devExt->DmaWriteList.done_list), &(devExt->DmaWriteList.doneLock));
			if (pDmaItem == NULL)
			{
				KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Exit DPC due to no Write item in done list\n"));
				break;
			}

			FifoUpdateOnWritten(devExt->PSharedFifo[RF_FRAME]);
			FifoUpdateOnWritten(devExt->PSharedFifo[RT_FRAME]);

			ExInterlockedInsertTailList(&(devExt->DmaWriteList.queue_list), (PLIST_ENTRY)pDmaItem, &(devExt->DmaWriteList.queueLock));
			devExt->debug_info.WriteEventRaised++;
		}
	}

	WdfInterruptReleaseLock(Interrupt);

	////
	//// Acquire this device's InterruptSpinLock.
	////
	//WdfInterruptAcquireLock(Interrupt);

	//// When there is an interrupt, handle the write dma items in the done list and notify the user application to read RT AND RF
	//

	//// When there is an interrupt, handle the read dma items in the done list and notify the user application to write another RF RAW FRAME if any.
	//while (TRUE)
	//{
	//	pDmaItem = (DMA_ITEM*)ExInterlockedRemoveHeadList(&(devExt->DmaReadList.done_list), &(devExt->DmaReadList.doneLock));
	//	if (pDmaItem == NULL)
	//	{
	//		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Exit DPC due to no Read item in done list\n"));
	//		break;
	//	}

	//	FifoUpdateOnRead(devExt->PSharedFifo[RF_DIAG_RAW_FRAME]);
	//	devExt->debug_info.ReadEventRaised++;
	//	ExInterlockedInsertTailList(&(devExt->DmaReadList.queue_list), (PLIST_ENTRY)pDmaItem, &(devExt->DmaReadList.queueLock));
	//}

	//WdfInterruptReleaseLock(Interrupt);

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Exit DPC \n"));

	return;
}

NTSTATUS
PDAQ4EvtInterruptEnable(
	IN WDFINTERRUPT Interrupt,
	IN WDFDEVICE    Device
)
/*++

Routine Description:

	Called by the framework at DIRQL immediately after registering the ISR with the kernel
	by calling IoConnectInterrupt.

Return Value:

	NTSTATUS
--*/
{
	UNREFERENCED_PARAMETER(Device);
	UNREFERENCED_PARAMETER(Interrupt);
	//PDEVICE_EXTENSION  devExt;

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4EvtInterruptEnable\n"));

	return STATUS_SUCCESS;
}

NTSTATUS
PDAQ4EvtInterruptDisable(
	IN WDFINTERRUPT Interrupt,
	IN WDFDEVICE    Device
)
/*++

Routine Description:

	Called by the framework at DIRQL before Deregistering the ISR with the kernel
	by calling IoDisconnectInterrupt.

Return Value:

	NTSTATUS
--*/
{
	//    PDEVICE_EXTENSION  devExt;
	UNREFERENCED_PARAMETER(Device);
	UNREFERENCED_PARAMETER(Interrupt);

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4EvtInterruptDisable\n"));
	return STATUS_SUCCESS;
}