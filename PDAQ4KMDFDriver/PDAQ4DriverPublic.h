#if !defined(_PDAQ4_PUBLIC_H_)
#define _PDAQ4_PUBLIC_H_

//
// The following value is arbitrarily chosen from the space defined
// by Microsoft as being "for non-Microsoft use"
//
// NOTE: This is still a value for OSR's PLX PCI bridge chips:
//
// GUID_OSR_PLX_INTERFACE GUID
// {29D2A384-2E47-49b5-AEBF-6962C22BD7C2}
//DEFINE_GUID(GUID_PDAQ4_INTERFACE,
//	0x29d2a384, 0x2e47, 0x49b5, 0xae, 0xbf, 0x69, 0x62, 0xc2, 0x2b, 0xd7, 0xc2);
// {D99A1855-176A-483D-8AB1-24B7F65B3BDA}
DEFINE_GUID(GUID_PDAQ4_INTERFACE,
	//0xd99a1855, 0x176a, 0x483d, 0x8a, 0xb1, 0x24, 0xb7, 0xf6, 0x5b, 0x3b, 0xda);
// {858A470A-2BF1-4E0C-AB1F-4C8C91D30724}

0x858a470a, 0x2bf1, 0x4e0c, 0xab, 0x1f, 0x4c, 0x8c, 0x91, 0xd3, 0x7, 0x24);

// Constants for board identification and resources

// Board ID structure
struct PDAQ4_board_id {
	UINT16 vendor;
	UINT16 device;
};

/* Convenience constant for bytes per word (as defined by the driver) */
#define PDAQ4_BYTES_PER_WORD  (sizeof(UINT32))

/* Maximum size for a block of configuration bitstream data */
#define MAX_CONFIG_BLOCK_SIZE (1024)

// Structure definition to carry blocks of configuration bitstream data
struct PDAQ4_data_block {
	UINT32 num_words;
	UINT32 block_words[MAX_CONFIG_BLOCK_SIZE];
};

typedef struct __WriteToRegister
{
	//The address of register to write, offset of the BAR0
	UINT32 offset;
	// The value of the register to value
	UINT32 value;
} WriteToRegister;

typedef struct __WriteToRegisterBlock
{
	UINT32 StartBARAddress;
	UINT32 RegisterNum;
	UINT32 RegisterBlock[1];
} WriteToRegisterBlock;

#define MAX_NUM_OF_SG_DESCRIPTOR 128
#define MAX_TRANSFER_SINGLE_DESCRIPTOR	0x100000
#define MIN_TRANSFER_SIGNLE_DESCRIPTOR	128
#define MAX_TRANSFGER_DMA_TRANSCATION (MIN_TRANSFER_SIGNLE_DESCRIPTOR*8192)
#define DMA_DESTINATION_ADDRESS_ALIGNMENT	0x1000

#define FRAME_HEADER_SIZE				96
#define VECTOR_HEADER_SIZE				16
#define RF_SAMPLE_BYTES					2
//#define RT_SAMPLE_BYTES					1
#define RT_SAMPLE_BYTES					2
#define VECTOR_NUM_PER_FRAME_256		256
#define VECTOR_NUM_PER_FRAME_512		512
// just for test
#define MAX_FRAME_FIFO_SIZE					128
#define FRAME_FIFO_NUM					3

#define SAMPLE_PER_VECTOR_1024		1024
#define SAMPLE_PER_VECTOR_2048		2048
#define SAMPLE_PER_VECTOR_512		512
#define SAMPLE_PER_VECTOR_256		256

#define FRAME_SIZE(vectors_per_frame,samples_per_vector,sample_bytes) ((FRAME_HEADER_SIZE + (vectors_per_frame*(VECTOR_HEADER_SIZE + (samples_per_vector*sample_bytes)))))
#define RAW_FRAME_SIZE(vector_per_frame,samples_per_vector,sample_bytes) ( vector_per_frame * samples_per_vector * sample_bytes)
#define ROUND_TO(size,pagesize) (((ULONG_PTR)(size)+pagesize-1)&~(pagesize-1))
#define ROUND_TO_DESCRIPTOR_MIN(size) (ROUND_TO(size,MIN_TRANSFER_SIGNLE_DESCRIPTOR))
#define ROUND_TO_DESCRIPTOR_MAX(size) (ROUND_TO(size,MAX_TRANSFER_SINGLE_DESCRIPTOR))
#define ALLOCE_FRAME_SIZE(samples_per_vector,vectors_per_frame,sample_bytes) ROUND_TO_DESCRIPTOR(FRAME_SIZE(samples_per_vector,vectors_per_frame,sample_bytes))

#define FIFO_HEADER_SIZE	(sizeof(DrvSharedFifo))

#define MAX_DESCRIPTORS_PER_FRAME	10

#define DMA_DESC_TABLE_TYPE_LONG   0
#define DMA_DESC_TABLE_TYPE_SHORT  1

#define OFFSET_VERSION	    0
#define OFFSET_MOT_RUN	    (1<<2)
#define OFFSET_RT_FRM_CNT   (2<<2)
#define OFFSET_RF_FRM_CNT   (3<<2)
#define OFFSET_SOFT_RESET   (5<<2)
#define OFFSET_INT_ENABLE   (6<<2)
#define OFFSET_INT_STATUS   (7<<2)

#define OFFSET_DMA_CTL1_REG (0x35<<2)
#define OFFSET_DMA_CTL2_REG (0x36<<2)
#define OFFSET_RD_CNT		(0x37<<2)

#define NT_DEVICE_NAME      L"\\Device\\PDAQ4"
#define DOS_DEVICE_NAME     L"\\DosDevices\\PDAQ4"

typedef enum __FRAME_TYPE
{
	RF_FRAME,
	RT_FRAME,
	RF_DIAG_RAW_FRAME
}FRAME_TYPE;

///////////////////////////////////////////////////////
/// A resource with 3 views: bus, kernel and WIN32
typedef struct _TriViewResourceResource
{
	LARGE_INTEGER     physAddr;
	PUCHAR             pvUser;   // Application poniter
	ULONG             length;

	////Zidong
	//LARGE_INTEGER     physFunAddr;
	//PUCHAR             pvFunUser;   // Application poniter
	//ULONG             funLength;
	////Zidong
#ifdef KERNEL_MODE
	ULONG			  flags;
	PUCHAR            pvKernel;
	//Zidong
	/*PUCHAR            pvFunKernel;*/
	PMDL              pMdl;
	WDFCOMMONBUFFER   commonbuffer;
#else  // following are not visible, DO NOT TOUCH
	ULONG             ReadOnly;
	PVOID             Reserved[3];
#endif
} DrvSharedResource, * PDrvSharedResource;

///
typedef struct __FifoElement
{
	ULONG	nSharedResource;
	PDrvSharedResource	pResourceList[MAX_DESCRIPTORS_PER_FRAME];
}FifoElement, * PFifoEelment;

///////////////////////////////////////////////////
// A fifo has one end in kertnel, and the other in Win32
// Constraint: one end is writter while the other is reader.
typedef  struct _DualViewFifo
{
	DrvSharedResource shareheader;
	LONG    nItems;
	LONG    nItemSize;      // size of data
	LONG    nAllocSize;     // size of allocated space for data. >= 'nItemSize'
	volatile LONG    nWrite;// modified by writer only
	volatile LONG    nRead; // modified by reader only
	//debug info
	volatile LONG nOverflow;// in case we didn't consume fast enough

	// the following are for kernel use only
#ifdef KERNEL_MODE
	PKEVENT      pEvent;           // used for kernel to notify application
	FifoElement	 FifoElementList[MAX_FRAME_FIFO_SIZE];  // user mode: do NOT modify this structure.
	//PKSEMAPHORE  pSemaphore;
#else
	
	PVOID       DoNotTouch[1409];
#endif
	// FIFO structure is open-ended - type specific data immediately follows here
} DrvSharedFifo, * PDrvSharedFifo;

#define NUM_SHARED_FIFOES   3
#define DRV_FIFO_ENV		0
#define DRV_FIFO_DRF		1
#define DRV_FIFO_EVT		2
#define DRV_FIFO_AUX		3   // not supportted by PDAQ

typedef struct __InitRequst
{
	ULONG n_rfframesize;
	ULONG n_rtframesize;
}DrvInitRequest, * PDrvInitRequest;

/////////////////////////// Common memory partition /////////////////////////
typedef struct _tagEvent
{
	LARGE_INTEGER liTime;   // time stamp - system time
	int   evId;
	ULONG len;              // 'data' length in bytes
	unsigned char  data[48];         // make this structure 64 bytes
} DrvEventInfo, * PDrvEventInfo;

////////////////////////// ioctl STRUCTURE /////////////////////////////////
typedef struct _ConnectFifoIn  // IN IOCTL_SUBSCRIBE
{
	HANDLE  hEvent;				// application handle of PKEVENT in FIFO structure
	FRAME_TYPE   frame_type;       // FIFO index
	// HANDLE  hSemaphore;
} DrvConnectFifoInPacket, * PDrvConnectFifoInPacket;

typedef struct  _ConnectFifoOut // OUT IOCTL_SUBSCRIBE
{
	DrvSharedFifo* pFifo;
} DrvConnectFifoOutPacket, * PDrvConnectFifoOutPacket;

//
typedef struct _QueryResource
{
	DrvSharedResource  resource[1];
}   DrvQueryResourcePacket, * PDrvQueryResourcePacket;

// FPGA image
typedef struct _tagLoadFpgaImagePath
{
	WCHAR wszPath[260]; // MAX_PATH is undefined in DDK
} DrvLoadFpgaPacket, * PDrvLoadFpgaPacket;

// Start Channel
typedef struct _tagStartChannel
{
	UCHAR Index;
	ULONG bStart;
} DrvStartChannelPacket, * PDrvStartChannelPacket;

/// diagnostic interface

typedef struct __FifoElementPacket
{
	ULONG	nSharedResource;
	DrvSharedResource ResourceList[MAX_DESCRIPTORS_PER_FRAME];
}	FifoElementPacket;

typedef struct _tagQueryFifoElement
{
	ULONG				 nFifoElement;
	FRAME_TYPE			 frame_type;
	FifoElementPacket	 packets[MAX_FRAME_FIFO_SIZE];  // user mode: do NOT modify this structure.
} DrvQueryFiFoPacket, * PDrvQueryFifoPacket;

typedef struct __DrvDebugInfo
{
	ULONG	FIFO_Index;
	ULONG	DMA_Word_Counter;
	ULONG	DMA_RTFrames_InBuffer;
	ULONG	DMA_RFFrames_InBuffer;
	ULONG   ReadInterruptNum;
	ULONG   WriteInterruptNum;
	ULONG   ReadEventRaised;
	ULONG   WriteEventRaised;
}DrvDebugInfo, * PDrvDebugInfo;
/// This is used to query number of channel overflows upon imaging stop.
/// overflow indicates that the whole imaging chain is slower than HW
typedef struct _tagStopAcqOutput
{
	OUT LONG nEnvOverflows;
	OUT LONG nRfOverflows;
	OUT LONG nAuxOverflows;
} DrvStopOutputPacket, * PDrvStopOutputPacket;

typedef struct _registerPacket {
	PCHAR Address;
	ULONG Value;

} RegisterPacket, *PRegisterPacket;

#define FILE_DEVICE_IDAQ  FILE_DEVICE_UNKNOWN

//Device control codes for use with DeviceIoControl()

//Initializes device reconfiguration
#define IOCTL_PDAQ4_INIT_RECONFIG CTL_CODE(FILE_DEVICE_IDAQ, 0x800, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Writes a block of configuration data to the device
#define IOCTL_PDAQ4_WRITE_CONFIG_REG CTL_CODE(FILE_DEVICE_IDAQ, 0x801, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Read a block of configuration data to the device
#define IOCTL_PDAQ4_READ_CONFIG_REGS CTL_CODE(FILE_DEVICE_IDAQ, 0x802, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Read a block of configuration data length to the device
#define IOCTL_PDAQ4_READ_CONFIG_LENGTH CTL_CODE(FILE_DEVICE_IDAQ, 0x803, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Writes a block of configuration data to the device
#define IOCTL_PDAQ4_WRITE_CONFIG_REG_BLOCK CTL_CODE(FILE_DEVICE_IDAQ, 0x804, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_INIT                  CTL_CODE(FILE_DEVICE_IDAQ, 0x805, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_RELEASE               CTL_CODE(FILE_DEVICE_IDAQ, 0x806, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_RESET                 CTL_CODE(FILE_DEVICE_IDAQ, 0x807, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_START_CHN             CTL_CODE(FILE_DEVICE_IDAQ, 0x808, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_QUERY_RESOURCE        CTL_CODE(FILE_DEVICE_IDAQ, 0x809, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_QUERY_DEVICE          CTL_CODE(FILE_DEVICE_IDAQ, 0x80A, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_LOADFPGA              CTL_CODE(FILE_DEVICE_IDAQ, 0x80B, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_CONNECT_FIFO          CTL_CODE(FILE_DEVICE_IDAQ, 0x80C, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_DISCONNECT_FIFO       CTL_CODE(FILE_DEVICE_IDAQ, 0x80D, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_START_ACQ      		    CTL_CODE(FILE_DEVICE_IDAQ, 0x80E, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_STOP_ACQ       		    CTL_CODE(FILE_DEVICE_IDAQ, 0x80F, METHOD_BUFFERED, FILE_ANY_ACCESS)

// Debug IOCTLS
#define IOCTL_GET_UNKNOWN_PACKETS   CTL_CODE(FILE_DEVICE_IDAQ, 0x810, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_CONFIG_DMA        	  CTL_CODE(FILE_DEVICE_IDAQ, 0x811, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_INTERNAL_RESET     	  CTL_CODE(FILE_DEVICE_IDAQ, 0x812, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_GET_NUM_DMADESC   	  CTL_CODE(FILE_DEVICE_IDAQ, 0x813, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_GET_DMABUFFER   	      CTL_CODE(FILE_DEVICE_IDAQ, 0x814, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_RESET_SHARED_RESOURCE   CTL_CODE(FILE_DEVICE_IDAQ, 0x815, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_DMA_INIT				  CTL_CODE(FILE_DEVICE_IDAQ, 0x816, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_DIAG_QUERY_DEBUG_INFO	CTL_CODE(FILE_DEVICE_IDAQ, 0x817, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_CONNECT_RF_DIAG_FIFO	CTL_CODE(FILE_DEVICE_IDAQ, 0x818, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_GET_DBG_INFO			CTL_CODE(FILE_DEVICE_IDAQ, 0x819, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_READ_REGISTER_VALUE	CTL_CODE(FILE_DEVICE_IDAQ, 0x81A, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_WRITE_REGISTER_VALUE	CTL_CODE(FILE_DEVICE_IDAQ, 0x81B, METHOD_BUFFERED, FILE_ANY_ACCESS)

//#define IOCTL_INIT                  CTL_CODE(FILE_DEVICE_IDAQ, 0x800, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_RELEASE               CTL_CODE(FILE_DEVICE_IDAQ, 0x801, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_RESET                 CTL_CODE(FILE_DEVICE_IDAQ, 0x802, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_START_CHN             CTL_CODE(FILE_DEVICE_IDAQ, 0x803, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_QUERY_RESOURCE        CTL_CODE(FILE_DEVICE_IDAQ, 0x804, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_QUERY_DEVICE          CTL_CODE(FILE_DEVICE_IDAQ, 0x805, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_LOADFPGA              CTL_CODE(FILE_DEVICE_IDAQ, 0x806, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_CONNECT_FIFO          CTL_CODE(FILE_DEVICE_IDAQ, 0x807, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_DISCONNECT_FIFO       CTL_CODE(FILE_DEVICE_IDAQ, 0x808, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_START_ACQ      		CTL_CODE(FILE_DEVICE_IDAQ, 0x809, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_STOP_ACQ       		CTL_CODE(FILE_DEVICE_IDAQ, 0x810, METHOD_BUFFERED, FILE_ANY_ACCESS)
//
//  // Debug IOCTLS
//#define IOCTL_GET_UNKNOWN_PACKETS   CTL_CODE(FILE_DEVICE_IDAQ, 0x811, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_CONFIG_DMA        	CTL_CODE(FILE_DEVICE_IDAQ, 0x812, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_INTERNAL_RESET     	CTL_CODE(FILE_DEVICE_IDAQ, 0x813, METHOD_BUFFERED, FILE_ANY_ACCESS)
//
//#define IOCTL_GET_NUM_DMADESC   	CTL_CODE(FILE_DEVICE_IDAQ, 0x814, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_GET_DMADESC   	    CTL_CODE(FILE_DEVICE_IDAQ, 0x815, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_RESET_SHARED_RESOURCE CTL_CODE(FILE_DEVICE_IDAQ, 0x816, METHOD_BUFFERED, FILE_ANY_ACCESS)
//
//#define IOCTL_GET_FRAMER_MODE                   CTL_CODE(FILE_DEVICE_IDAQ, 0x8FF, METHOD_OUT_DIRECT, FILE_READ_DATA)
//#define IOCTL_SET_FRAMER_MODE                   CTL_CODE(FILE_DEVICE_IDAQ, 0x900, METHOD_BUFFERED, FILE_ANY_ACCESS)
//#define IOCTL_WAIT_UNTIL_RECEIVING_RF_FRAME     CTL_CODE(FILE_DEVICE_IDAQ, 0x901, METHOD_OUT_DIRECT, FILE_READ_DATA)
//#define IOCTL_WAIT_UNTIL_RECEIVING_RT_FRAME     CTL_CODE(FILE_DEVICE_IDAQ, 0x902, METHOD_OUT_DIRECT, FILE_READ_DATA)
//
//#define IOCTL_GET_VERSION                   CTL_CODE(FILE_DEVICE_IDAQ, 0x903, METHOD_OUT_DIRECT, FILE_READ_DATA)

typedef struct {
	unsigned int	Done : 1;
	unsigned int	Reserved : 31;
} DMA_Descriptor_Status, * DMA_Descriptor_Status_PTR;

//Zidong

/*
When the DMA Descriptor Controller is externally instantiated, these registers are
accessed through a BAR. The offsets must be added to the base address for the write
controller. When the Descriptor Controller is internally instantiated these
registers are accessed through BAR0. The write controller is at offset 0x0100.
*/
#define	OFFSET_WRITE_CONTROLLER	(0x0100)
typedef struct {
	ULONG	RC_Write_Status_and_Descriptor_Base_Low;
	ULONG	RC_Write_Status_and_Descriptor_Base_High;
	ULONG	EP_Write_Descriptor_FIFO_Base_Low;
	ULONG	EP_Write_Descriptor_FIFO_Base_High;
	ULONG	WR_DMA_LAST_PTR;
	ULONG	WR_TABLE_SIZE;
	DMA_Descriptor_Status	WR_CONTROL;
} Write_DMA_Descriptor_Controller_Registers, * PWrite_DMA_Descriptor_Controller_Registers;

#define	OFFSET_READ_CONTROLLER	(0)
typedef struct {
	ULONG	RC_Read_Status_and_Descriptor_Base_Low;
	ULONG	RC_Read_Status_and_Descriptor_Base_High;
	ULONG	EP_Read_Descriptor_FIFO_Base_Low;
	ULONG	EP_Read_Descriptor_FIFO_Base_High;
	ULONG	RD_DMA_LAST_PTR;
	ULONG	RD_TABLE_SIZE;
	DMA_Descriptor_Status	RD_CONTROL;
} Read_DMA_Descriptor_Controller_Registers, * PRead_DMA_Descriptor_Controller_Registers;

/*
// Note - first 8 words are framer_config_regs; see the Excel file for full list of registers
// word	Byte HEX addr
0	x00	ro	version info //major, minor, iteration (read only)
1	x04	ro	status reg{ 24'b0 reserved, current frame index } (read only)
2	x08	rw	control register, bit 0 enables ddr writes; bit 1 enables MSI write
3	x0C	rw	memory base of frame header, points to ddr
4	x10	rw	memory table length for all frames, bytes
5	x14	rw	MSI data value, {16'b0 reserved, data value[15:2], 2'b--},
//					where -- is non zero interrupt vector
6	x18	rw	MSI vector address of ISR , lower 32 bits

7	x1C	rw	MSI vector address, upper 32 bits
*/
typedef struct {
	ULONG	Version;
	ULONG	Status;

#define	ENABLE_FRAME_DDR_WRITE	(0x00000001)
#define	ENABLE_FRAME_MSI_WRITE	(0x00000002)
	ULONG	Control;

	ULONG	Header_Base;
	ULONG	Table_Length;
	ULONG	MSI_Data;
	ULONG	MSI_Low_Addr;
	ULONG	MSI_High_Addr;
} Framer_Config_Regs, * Framer_Config_Regs_PTR;

// Each framer has (8) 32-bit configuration registers accessible through BAR2 (or system console for debug).
// Framer1 (RF framer) is at x400 and framer2 (RT framer) is at xC00.
#define	OFFSET_RF_FRAMER	(0xe0100)	//(0x400)
#define	OFFSET_RT_FRAMER	(0xe0300)	//(0xc00)

#define MIN_TRANSFER_CHUNK_SIZE	(4)					//Minimum transfer size of one dword (4 bytes).
#define MAX_TRANSFER_CHUNK_SIZE	(1024 * 1024 - 4)	//Maximum transfer size of 1 M (1024 * 1024) - 4 bytes.

/*
The DMA Descriptor Controller records the completion status for read and write
descriptors in separate status tables. Each table has 128 consecutive DWORD entries
that correspond to the 128 descriptors. The actual descriptors are stored immediately
after the status entries at offset 0x200 from the values programmed into the RC Read
Descriptor Base and RC Write Descriptor Base registers.
*/
#define MAX_DMA_DESCRIPTOR_ENTRIES	(128)

typedef struct {
	unsigned int	SIZE : 18;	// unit: DWORDs
	unsigned int	ID : 7;
	unsigned int	Reserved : 7;
} DMA_Descriptor_Control, * DMA_Descriptor_Control_PTR;

typedef struct {
	ULONG	RD_LOW_SRC_ADDR;
	ULONG	RD_HIGH_SRC_ADDR;
	ULONG	RD_CTRL_LOW_DEST_ADDR;
	ULONG	RD_CTRL_HIGH_DEST_ADDR;
	DMA_Descriptor_Control	CONTROL;
	ULONG	Reserved[3];
} Read_Descriptor_Format, * Read_Descriptor_Format_PTR;

typedef struct {
	ULONG	WR_LOW_SRC_ADDR;
	ULONG	WR_HIGH_SRC_ADDR;
	ULONG	WR_CTRL_LOW_DEST_ADDR;
	ULONG	WR_CTRL_HIGH_DEST_ADDR;
	DMA_Descriptor_Control	CONTROL;
	ULONG	Reserved[3];
} Write_Descriptor_Format, * Write_Descriptor_Format_PTR;

typedef struct {
	DMA_Descriptor_Status	Status[MAX_DMA_DESCRIPTOR_ENTRIES];
	Read_Descriptor_Format	Descriptor[MAX_DMA_DESCRIPTOR_ENTRIES];
} Read_DMA_Status_and_Descriptor_Table, * Read_DMA_Status_and_Descriptor_Table_PTR;

typedef struct {
	DMA_Descriptor_Status	Status[MAX_DMA_DESCRIPTOR_ENTRIES];
	Write_Descriptor_Format	Descriptor[MAX_DMA_DESCRIPTOR_ENTRIES];
} Write_DMA_Status_and_Descriptor_Table, * Write_DMA_Status_and_Descriptor_Table_PTR;

typedef struct {
	Read_DMA_Status_and_Descriptor_Table Read;
	Write_DMA_Status_and_Descriptor_Table Write;
} ReadWrite_DMA_Status_and_Descriptor_Table, * ReadWrite_DMA_Status_and_Descriptor_Table_PTR;

#define RF_FPGA_RING_BUFFER_BASE    (0x1000000)
#define RF_FPGA_RING_BUFFER_ENTRIES (16)

#define RT_FPGA_RING_BUFFER_BASE    (0x2000000)
#define RT_FPGA_RING_BUFFER_ENTRIES (32)

#endif  // _PDAQ4_PUBLIC_H_