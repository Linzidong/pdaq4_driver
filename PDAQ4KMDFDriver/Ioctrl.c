#include "Precomp.h"
#include "Trace.h"
#include "ioctrl.tmh"
#include <stdlib.h>


#define READ_REG_NUM (8)


VOID PDAQ4_IOCTL_WriteRegisterValue(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param) {
	UNREFERENCED_PARAMETER(param);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Enter PDAQ4_IOCTL_WriteRegisterValue");
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBufferIn;
	size_t                  BufferLengthIn;

	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveInputBuffer(Request, 4, &DataBufferIn, &BufferLengthIn);

	ULONG(*params)[3];

	params = (ULONG(*)[3])DataBufferIn;
	ULONG type = *((PULONG)DataBufferIn); //(*params)[0];
	ULONG offset = *((PULONG)DataBufferIn + 1); //(*params)[1];
	ULONG value = *((PULONG)DataBufferIn + 2);//(*params)[2];

	PUCHAR base = NULL;
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Input type = %d, offset = %d, value = %d\n", type, offset, value));
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Input type = %d, offset = %d, value = %d\n", type, offset, value);
	/*WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, 0);

	return;*/
	if (type == 0) {
		//base = devExt->FpgaDmaRegBase;
		base = devExt->FpgaFunRegBase + OFFSET_RF_FRAMER;
	}
	else if (type == 1) {

		base = devExt->FpgaFunRegBase + OFFSET_RT_FRAMER;

	}

	PULONG address = (PULONG)(base + offset);

	WRITE_REGISTER_ULONG(address,value);

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Write Register succeed");


	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, 0);



}

VOID PDAQ4_IOCTL_ReadRegisterValue(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param) {
	UNREFERENCED_PARAMETER(param);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Enter PDAQ4_IOCTL_ReadRegisterValue");

	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBufferOut;
	PVOID                   DataBufferIn;
	size_t                  BufferLengthOut;
	size_t                  BufferLengthIn;
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveInputBuffer(Request, 4, &DataBufferIn, &BufferLengthIn);
	ULONG(*params)[3];

	params = (ULONG(*)[3])DataBufferIn;
	ULONG type = *((PULONG)DataBufferIn); //(*params)[0];
	ULONG offset = *((PULONG)DataBufferIn+1); //(*params)[1];
	ULONG number = *((PULONG)DataBufferIn+2);//(*params)[2];



	PUCHAR base = NULL;
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Input type = %d, offset = %d, number = %d\n", type, offset, number));

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Input type = %d, offset = %d, number = %d\n", type, offset, number);
	

	if (type == 0) {
		//base = devExt->FpgaDmaRegBase;
		base = devExt->FpgaFunRegBase + OFFSET_RF_FRAMER;
	}
	else if (type == 1) {

		base = devExt->FpgaFunRegBase + OFFSET_RT_FRAMER;

	}


	//strcpy(DataBufferIn, &params);

	status = WdfRequestRetrieveOutputBuffer(Request, number * sizeof(RegisterPacket), &DataBufferOut, &BufferLengthOut);

	//PRegisterPacket pReg = (PRegisterPacket)DataBufferOut;
	//ULONG(*output)[READ_REG_NUM];
	
	PRegisterPacket buffer = (PRegisterPacket)DataBufferOut;
	//PULONG buffer = (PULONG)DataBufferOut;
	PULONG address = (PULONG)(base + offset);

	//output = (ULONG(*)[READ_REG_NUM])DataBufferOut;
	for (ULONG i = 0; i < number; i++)
	{
		
		buffer->Address = address;
		buffer->Value = READ_REGISTER_ULONG((PULONG)(address));
		//PUCHAR address = base + offset + (i * 4);
	  //*buffer = READ_REGISTER_ULONG((PULONG)(address));
	  
		//*output[i] = registerValue;

		//pReg++;
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "output Address = %x, Value = %d\n", (ULONG)(buffer ->Address), buffer->Value));
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "output Address = %x, Value = %d\n", (ULONG)(buffer->Address), buffer->Value);
		buffer++;
		address++;
	}

	//PULONG pValue = (PULONG)DataBufferOut;

	//*pValue = registerValue;

	////DrvQueryResourcePacket* pQuery = (DrvQueryResourcePacket*)DataBuffer;

	//if (pValue == NULL)
	//{
	//	WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
	//	return;
	//}



	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Exit PDAQ4_IOCTL_ReadRegisterValue");


	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, number * sizeof(RegisterPacket));
}

//VOID PDAQ4_IOCTL_ReadRegisterValue(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param) {
//	UNREFERENCED_PARAMETER(param);
//	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Enter PDAQ4_IOCTL_ReadRegisterValue");
//
//	NTSTATUS                status = STATUS_SUCCESS;
//	PVOID                   DataBufferOut;
//	PVOID                   DataBufferIn;
//	size_t                  BufferLengthOut;
//	size_t                  BufferLengthIn;
//	PDEVICE_EXTENSION devExt = NULL;
//	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
//
//	status = WdfRequestRetrieveInputBuffer(Request, 4, &DataBufferIn, &BufferLengthIn);
//	ULONG (*params)[3];
//
//	params = (ULONG (*) [3])DataBufferIn;
//	ULONG type = (*params)[0];
//	ULONG offset = (*params)[1];
//	ULONG number = (*params)[2];
//
//	
//
//	PUCHAR base = NULL;
//
//	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Input type = %d, offset = %d, number = %d\n",type,offset,number);
//	if (type == 0) {
//		base = devExt->FpgaDmaRegBase;
//	}
//	else if (type == 1) {
//		base = devExt->FpgaFunRegBase;
//	}
//	//strcpy(DataBufferIn, &params);
//	
//	status = WdfRequestRetrieveOutputBuffer(Request, number * sizeof(ULONG), &DataBufferOut, &BufferLengthOut);
//
//	PULONG pBuffer = (PULONG)DataBufferOut;
//
//	READ_REGISTER_BUFFER_ULONG((PULONG)(base + offset), pBuffer, number);
//
//	/*for (ULONG i = 0; i < number; i++)
//	{
//		PUCHAR address = base + (offset + (i * 4));
//		ULONG registerValue = READ_REGISTER_ULONG((PULONG)(address));
//
//		pReg->Address = address;
//		pReg->Value = registerValue;
//
//		pReg++;
//		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "output Address = %x, Value = %d\n", (ULONG)address, registerValue);
//		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "pReg output Address = %x, Value = %d\n", (ULONG)(pReg -> Address), pReg->Value);
//	}*/
//
//	//PULONG pValue = (PULONG)DataBufferOut;
//
//	//*pValue = registerValue;
//
//	////DrvQueryResourcePacket* pQuery = (DrvQueryResourcePacket*)DataBuffer;
//
//	//if (pValue == NULL)
//	//{
//	//	WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
//	//	return;
//	//}
//
//	
//	
//	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Exit PDAQ4_IOCTL_ReadRegisterValue");
//	
//
//	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, 4);
//}




VOID
PDAQ4_IOCTL_QueryResource(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveOutputBuffer(Request, sizeof(DrvQueryResourcePacket), &DataBuffer, &BufferLength);
	DrvQueryResourcePacket* pQuery = (DrvQueryResourcePacket*)DataBuffer;

	if (pQuery == NULL)
	{
		WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
		return;
	}
	
	MapSharedMemory(&devExt->FPGARegisterFunc);

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "FPGARegisterFunc -> physAddr = %x", devExt->FPGARegisterFunc.physAddr.QuadPart);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "FPGARegisterFunc -> pvUser = %x", devExt->FPGARegisterFunc.pvUser);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "FPGARegisterFunc -> physAddr = %x", devExt->FPGARegisterFunc.length);

	memcpy(&pQuery->resource[0], &devExt->FPGARegisterFunc, sizeof(DrvSharedResource));

	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, sizeof(DrvQueryResourcePacket));
}




VOID
PDAQ4_IOCTL_Init(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	//UnmapSharedMemory(&devExt->FPGARegister);
	UnmapSharedMemory((PDrvSharedResource)devExt->PSharedFifo[0]);
	UnmapSharedMemory((PDrvSharedResource)devExt->PSharedFifo[1]);
	//UnmapSharedMemory((PDrvSharedResource)devExt->PSharedFifo[2]);

	for (ULONG i = 0; i < devExt->Shared_Resource_Num; i++)
	{
		UnmapSharedMemory(&devExt->SharedResourceList[i]);
	}
	for (ULONG i = 0; i < devExt->Diag_Shared_Resource_Num; i++)
	{
		UnmapSharedMemory(&devExt->Diag_SharedResourceList[i]);
	}

	// Rest the PDAQ4
	//RESET_PDAQ(devExt->ConfigSpace);

	//while (*((PREG32)&(devExt->ConfigSpace->rSoftReset))!=0xE0000000)
	{
		KeStallExecutionProcessor(500000);
	}
	RtlZeroMemory(&devExt->debug_info, sizeof(DrvDebugInfo));
	WdfRequestComplete(Request, status);
}

VOID PDAQ4_IOCTL_GetDBGInfo(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param) {
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "RegsBase = %p\n", (devExt->RegsBase));
	TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "FpgaFunRegBase = %p\n", (devExt->FpgaFunRegBase));
}



VOID PDAQ4_IOCTL_QueryDebugInfo(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveOutputBuffer(Request, sizeof(DrvDebugInfo), &DataBuffer, &BufferLength);

	PDrvDebugInfo pQuery = (PDrvDebugInfo)DataBuffer;

	if (pQuery == NULL)
	{
		WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
		return;
	}
	RtlCopyMemory(pQuery, &devExt->debug_info, sizeof(DrvDebugInfo));
	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, BufferLength);
}

VOID PDAQ4_IOCTL_InitializeDMA(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	NTSTATUS    status = STATUS_SUCCESS;
	PDEVICE_EXTENSION DevExt = NULL;
	DevExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	PVOID                   DataBuffer;
	size_t                  BufferLength;

	status = WdfRequestRetrieveInputBuffer(Request, sizeof(DrvInitRequest), &DataBuffer, &BufferLength);
	if (!NT_SUCCESS(status))
	{
		WdfRequestComplete(Request, status);
		return;
	}

	PDrvInitRequest p_initrequest = (PDrvInitRequest)DataBuffer;
	//Set_FIFO_Parameters(DevExt, FRAME_SIZE(VECTOR_NUM_PER_FRAME_256, SAMPLE_PER_VECTOR_1024, RF_SAMPLE_BYTES), FRAME_SIZE(VECTOR_NUM_PER_FRAME_256, SAMPLE_PER_VECTOR_256, RT_SAMPLE_BYTES));
	status = PDAQ4InitWriteDMA(DevExt, p_initrequest->n_rfframesize, p_initrequest->n_rtframesize);
	//get raw rf frame sizef from rfframesize
	ULONG rawrfsize = 0;
	if (p_initrequest->n_rfframesize == 528480)
	{
		rawrfsize = 524288;
	}
	else if (p_initrequest->n_rfframesize == 1052768)
	{
		rawrfsize = 1048576;
	}
	else if (p_initrequest->n_rfframesize == 1056864)
	{
		rawrfsize = 1048576;
	}
	else if (p_initrequest->n_rfframesize == 2105440)
	{
		rawrfsize = 2097152;
	}
	//status = PDAQ4InitReadDMA(DevExt, rawrfsize);
	WdfRequestComplete(Request, status);
}

VOID
PDAQ4_IOCTL_QueryFIFO(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "QueryFIFO Enter");
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveOutputBuffer(Request, sizeof(DrvQueryFiFoPacket), &DataBuffer, &BufferLength);

	if (BufferLength != sizeof(DrvQueryFiFoPacket) * FRAME_FIFO_NUM)
	{
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "QueryFIFO sizeof(DrvSharedResource) = %d", sizeof(DrvSharedResource));
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "QueryFIFO BufferLength = %d", BufferLength);
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "QueryFIFO sizeof(DrvQueryFiFoPacket) = %d", sizeof(DrvQueryFiFoPacket));
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "QueryFIFO BufferLength require = %d", sizeof(DrvQueryFiFoPacket) * FRAME_FIFO_NUM);
		WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
		return;
	}

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "QueryFIFO BufferLength = %d\n ", BufferLength));
	DrvQueryFiFoPacket* pQuery = (DrvQueryFiFoPacket*)DataBuffer;

	if (pQuery == NULL)
	{
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "pQuery == null");
		WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
		return;
	}

	for (ULONG i = 0; i < devExt->Shared_Resource_Num; i++)
	{
		MapSharedMemory(&devExt->SharedResourceList[i]);
	}

	for (ULONG i = 0; i < devExt->Diag_Shared_Resource_Num; i++)
	{
		MapSharedMemory(&devExt->Diag_SharedResourceList[i]);
	}

	pQuery->nFifoElement = devExt->Frame_FIFO_size;
	pQuery->frame_type = RF_FRAME;
	for (ULONG j = 0; j < devExt->Frame_FIFO_size; j++)
	{
		pQuery->packets[j].nSharedResource = devExt->PSharedFifo[RF_FRAME]->FifoElementList[j].nSharedResource;
		for (ULONG i = 0; i < pQuery->packets[j].nSharedResource; i++)
		{
			pQuery->packets[j].ResourceList[i].physAddr = devExt->PSharedFifo[RF_FRAME]->FifoElementList[j].pResourceList[i]->physAddr;
			pQuery->packets[j].ResourceList[i].pvUser = devExt->PSharedFifo[RF_FRAME]->FifoElementList[j].pResourceList[i]->pvUser;
			pQuery->packets[j].ResourceList[i].length = devExt->PSharedFifo[RF_FRAME]->FifoElementList[j].pResourceList[i]->length;
		}
	}
	pQuery++;
	pQuery->frame_type = RT_FRAME;
	pQuery->nFifoElement = devExt->Frame_FIFO_size;
	for (ULONG j = 0; j < devExt->Frame_FIFO_size; j++)
	{
		pQuery->packets[j].nSharedResource = devExt->PSharedFifo[RT_FRAME]->FifoElementList[j].nSharedResource;
		for (ULONG i = 0; i < pQuery->packets[j].nSharedResource; i++)
		{
			pQuery->packets[j].ResourceList[i].physAddr = devExt->PSharedFifo[RT_FRAME]->FifoElementList[j].pResourceList[i]->physAddr;
			pQuery->packets[j].ResourceList[i].pvUser = devExt->PSharedFifo[RT_FRAME]->FifoElementList[j].pResourceList[i]->pvUser;
			pQuery->packets[j].ResourceList[i].length = devExt->PSharedFifo[RT_FRAME]->FifoElementList[j].pResourceList[i]->length;
		}
	}

	pQuery++;
	pQuery->frame_type = RF_DIAG_RAW_FRAME;
	pQuery->nFifoElement = devExt->Diag_RawFrame_FIFO_size;
	for (ULONG j = 0; j < devExt->Diag_RawFrame_FIFO_size; j++)
	{
		pQuery->packets[j].nSharedResource = devExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[j].nSharedResource;
		for (ULONG i = 0; i < pQuery->packets[j].nSharedResource; i++)
		{
			pQuery->packets[j].ResourceList[i].physAddr = devExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[j].pResourceList[i]->physAddr;
			pQuery->packets[j].ResourceList[i].pvUser = devExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[j].pResourceList[i]->pvUser;
			pQuery->packets[j].ResourceList[i].length = devExt->PSharedFifo[RF_DIAG_RAW_FRAME]->FifoElementList[j].pResourceList[i]->length;
		}
	}
	WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, BufferLength);
}

VOID
PDAQ4_IOCTL_WriteConfigREG(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveInputBuffer(Request, sizeof(WriteToRegister), &DataBuffer, &BufferLength);
	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4WriteConfigREG WdfRequestRetrieveInputBuffer failed: !STATUS! = 0x%08X\n ", status));
		WdfRequestComplete(Request, status);
		return;
	}
	WriteToRegister* reg = (WriteToRegister*)DataBuffer;
	UINT32* RegsBase = (UINT32*)devExt->RegsBase;

	if (reg->offset * 4 > devExt->RegsLength)
	{
		ASSERTMSG(FALSE, "Invalid Register offset value, outside of range\n");
		WdfRequestComplete(Request, STATUS_INVALID_PARAMETER);
		return;
	}

	// Write the config Register
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4WriteConfigREG write address:%x value:%x\n ", RegsBase + reg->offset, reg->value));
	*(RegsBase + reg->offset) = reg->value;

	WdfRequestComplete(Request, status);
}

VOID
PDAQ4_IOCTL_WriteConfigREGBlock(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	//size_t HeaderLen = (sizeof(WriteToRegisterBlock)-sizeof(UINT32));
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveInputBuffer(Request, sizeof(WriteToRegisterBlock), &DataBuffer, &BufferLength);
	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4WriteConfigREG WdfRequestRetrieveInputBuffer failed: !STATUS! = 0x%08X\n ", status));
		WdfRequestComplete(Request, status);
		return;
	}

	WriteToRegisterBlock* regblock = (WriteToRegisterBlock*)DataBuffer;

	if (regblock->StartBARAddress + regblock->RegisterNum * sizeof(UINT32) > devExt->RegsLength)
	{
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " The Block is invalid, the start address is %08X,the length is %08X, exceeds the BAR region!", regblock->StartBARAddress, BufferLength - 1));
		WdfRequestComplete(Request, STATUS_UNSUCCESSFUL);
		return;
	}

	PUCHAR datablock = (PUCHAR)regblock->RegisterBlock;
	RtlCopyMemory(devExt->RegsBase + regblock->StartBARAddress, datablock, regblock->RegisterNum * sizeof(UINT32));

	// Write the config Register
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ BLOCK WRITE FINISHED "));

	WdfRequestComplete(Request, STATUS_SUCCESS);
}

VOID
PDAQ4_IOCTL_ReadConfigREGs(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	UINT32* REG_PTR;
	UNREFERENCED_PARAMETER(param);
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, " PDAQ4ReadConfigREGs Entered\n"));

	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	status = WdfRequestRetrieveOutputBuffer(Request, devExt->RegsLength, &DataBuffer, &BufferLength);
	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " PDAQ4ReadConfigREGs WdfRequestRetrieveInputBuffer failed:  !STATUS! = 0x%08X\n", status));
		WdfRequestComplete(Request, status);
		return;
	}

	RtlMoveMemory(DataBuffer, devExt->RegsBase, devExt->RegsLength);
	REG_PTR = (UINT32*)DataBuffer;

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, " PDAQ4ReadConfigREGs, The first 2 UINT32 are %x,%x\n", *REG_PTR, *(REG_PTR + 1)));

	WdfRequestCompleteWithInformation(Request, status, devExt->RegsLength);
}

VOID
PDAQ4_IOCTL_ReadConfigLength(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	UNREFERENCED_PARAMETER(param);
	NTSTATUS                status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	PDEVICE_EXTENSION devExt = NULL;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	status = WdfRequestRetrieveOutputBuffer(Request, 4, &DataBuffer, &BufferLength);

	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " PDAQ4ReadConfigLength WdfRequestRetrieveInputBuffer failed:  !STATUS! = 0x%08X\n", status));
		WdfRequestComplete(Request, status);
		return;
	}

	UINT32* config_length = (UINT32*)DataBuffer;
	*config_length = devExt->RegsLength;
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, " PDAQ4ReadConfigLength Length=%x\n", *config_length));
	WdfRequestSetInformation(Request, sizeof(UINT32));
	WdfRequestComplete(Request, status);
}

// START DMA
void PDAQ4_IOCTL_StartACQ(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4_IOCTL_StartACQ Enter\n"));

	PDEVICE_EXTENSION devExt = NULL;
	NTSTATUS                status = STATUS_SUCCESS;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	//PPDAQ4_REG pConfig = devExt->ConfigSpace;
	////START_DMA(pConfig);
	//RtlZeroMemory(&devExt->debug_info, sizeof(DrvDebugInfo));
	//FPGA_MOTOR_RUN(pConfig, 1);

	Framer_Config_Regs_PTR pRegRF = (Framer_Config_Regs_PTR)(devExt->FpgaFunRegBase + OFFSET_RF_FRAMER);
	Framer_Config_Regs_PTR pRegRT = (Framer_Config_Regs_PTR)(devExt->FpgaFunRegBase + OFFSET_RT_FRAMER);

	WRITE_REGISTER_ULONG((PULONG)(&pRegRF->Control), 0);
	WRITE_REGISTER_ULONG((PULONG)(&pRegRT->Control), (ENABLE_FRAME_DDR_WRITE | ENABLE_FRAME_MSI_WRITE));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4_IOCTL_StartACQ Finish\n"));
	/*WRITE_REGISTER_ULONG((PULONG)(&pRegRF->Control), (ENABLE_FRAME_DDR_WRITE | ENABLE_FRAME_MSI_WRITE));
	ULONG rfStatus = READ_REGISTER_ULONG((PULONG) & (pRegRF->Control));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Enable RF\n"));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "RFStatus: %u\n", rfStatus));

	WRITE_REGISTER_ULONG((PULONG)(&pRegRT->Control), (ENABLE_FRAME_DDR_WRITE | ENABLE_FRAME_MSI_WRITE));
	ULONG rtStatus = READ_REGISTER_ULONG((PULONG) & (pRegRT->Control));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Enable RT\n"));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "RTStatus: %u\n", rtStatus));*/

	WdfRequestComplete(Request, status);
}

//STOP DMA

void PDAQ4_IOCTL_StopACQ(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4_IOCTL_StopACQ Enter\n"));

	PDEVICE_EXTENSION devExt = NULL;
	NTSTATUS                status = STATUS_SUCCESS;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	//PPDAQ4_REG pConfig = devExt->ConfigSpace;
	////START_DMA(pConfig);
	//RtlZeroMemory(&devExt->debug_info, sizeof(DrvDebugInfo));
	//FPGA_MOTOR_RUN(pConfig, 1);
	Framer_Config_Regs_PTR pRegRF = (Framer_Config_Regs_PTR)(devExt->FpgaFunRegBase + OFFSET_RF_FRAMER);
	Framer_Config_Regs_PTR pRegRT = (Framer_Config_Regs_PTR)(devExt->FpgaFunRegBase + OFFSET_RT_FRAMER);

	WRITE_REGISTER_ULONG((PULONG)(&pRegRF->Control), 0);
	WRITE_REGISTER_ULONG((PULONG)(&pRegRT->Control), 0);
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4_IOCTL_StopACQ Finish\n"));

	/*PDEVICE_EXTENSION devExt = NULL;
	NTSTATUS                status = STATUS_SUCCESS;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	PPDAQ4_REG pConfig = devExt->ConfigSpace;
	FPGA_MOTOR_RUN(pConfig, 0);*/
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! End");

	WdfRequestComplete(Request, status);
}

////////////////////////////////////////////////////////////////////////////////////////
// Supporting routine for IOCTL_CONNECT_CHANNEL
////////////////////////////////////////////////////////////////////////////////////////
VOID PDAQ4_IOCTL_ConnectChannel(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");

	PDEVICE_EXTENSION devExt = NULL;
	PVOID                   InDataBuffer, OutDataBuffer;
	size_t                  InBufferLength, OutBufferLength;
	NTSTATUS                status = STATUS_SUCCESS;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveInputBuffer(Request, sizeof(DrvConnectFifoInPacket), &InDataBuffer, &InBufferLength);
	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " PDAQ4_IOCTL_ConnectChannel WdfRequestRetrieveInputBuffer failed:  !STATUS! = 0x%08X\n", status));
		WdfRequestComplete(Request, status);
		return;
	}

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "DrvConnectFifoOutPacket Size = %d\n", sizeof(DrvConnectFifoOutPacket));
	status = WdfRequestRetrieveOutputBuffer(Request, sizeof(DrvConnectFifoOutPacket), &OutDataBuffer, &OutBufferLength);
	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " PDAQ4_IOCTL_ConnectChannel WdfRequestRetrieveOutputBuffer failed:  !STATUS! = 0x%08X\n", status));
		WdfRequestComplete(Request, status);
		return;
	}

	DrvConnectFifoInPacket* pIn = (DrvConnectFifoInPacket*)InDataBuffer;
	DrvConnectFifoOutPacket* pOut = (DrvConnectFifoOutPacket*)OutDataBuffer;

	ULONG index = pIn->frame_type;

	if (index <= RF_DIAG_RAW_FRAME)
	{
		if (devExt->PSharedFifo[index]->pEvent != NULL)
		{
			ObDereferenceObject(devExt->PSharedFifo[index]->pEvent);
			devExt->PSharedFifo[index]->pEvent = NULL;
		}
		status = ObReferenceObjectByHandle(pIn->hEvent,
			OBJECT_TYPE_ALL_ACCESS,
			*ExEventObjectType,
			KernelMode,
			(PVOID*)&devExt->PSharedFifo[index]->pEvent,
			NULL
		);
	    NTSTATUS mapStat =	MapSharedMemory((PDrvSharedResource)devExt->PSharedFifo[index]);

		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "MapSharedMemory result = %!STATUS! End", mapStat);
		pOut->pFifo = (PDrvSharedFifo)devExt->PSharedFifo[index]->shareheader.pvUser;
	}
	else
	{
		status = STATUS_INVALID_PARAMETER;
	}
	WdfRequestCompleteWithInformation(Request, status, OutBufferLength);
}

////////////////////////////////////////////////////////////////////////////////////////
// Supporting routine for IOCTL_DISCONNECT_CHANNEL
////////////////////////////////////////////////////////////////////////////////////////
VOID PDAQ4_IOCTL_DisConnectChannel(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param)
{
	PDEVICE_EXTENSION devExt = NULL;
	NTSTATUS status = STATUS_SUCCESS;
	PVOID                   DataBuffer;
	size_t                  BufferLength;
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	status = WdfRequestRetrieveInputBuffer(Request, sizeof(UINT32), &DataBuffer, &BufferLength);

	PUINT32 pindex = (UINT32*)DataBuffer;
	if (*pindex <= RF_DIAG_RAW_FRAME)
	{
		if (devExt->PSharedFifo[*pindex]->pEvent != NULL)
		{
			ObDereferenceObject(devExt->PSharedFifo[*pindex]->pEvent);
			devExt->PSharedFifo[*pindex]->pEvent = NULL;
		}
	}
	else
	{
		status = STATUS_INVALID_PARAMETER;
	}
	WdfRequestComplete(Request, status);
}