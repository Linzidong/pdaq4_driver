// Precompiled header
#include "Precomp.h"
#include "Trace.h"
#include "PDAQ4.tmh"

// System headers
#include <ntddk.h>
#include <wdf.h>
#include <wdfdevice.h>
//#include "../../../../../../work/PCIe/PLX9x5x_PCI_Driver/sys/trace.h"

// Function prototypes
DRIVER_INITIALIZE               DriverEntry;
EVT_WDF_DRIVER_DEVICE_ADD       PDAQ4EvtDeviceAdd;
EVT_WDF_DEVICE_PREPARE_HARDWARE PDAQ4EvtDevicePrepareHardware;
EVT_WDF_DEVICE_RELEASE_HARDWARE PDAQ4EvtDeviceReleaseHardware;
EVT_WDF_DEVICE_D0_ENTRY         PDAQ4EvtDeviceD0Entry;
EVT_WDF_DEVICE_D0_EXIT          PDAQ4EvtDeviceD0Exit;
EVT_WDF_DEVICE_FILE_CREATE      PDAQ4EvtDeviceFileCreate;
EVT_WDF_PROGRAM_DMA				PDAQ4ProgramReadDma;
EVT_WDF_FILE_CLOSE              PDAQ4EvtFileClose;
EVT_WDF_OBJECT_CONTEXT_CLEANUP OnDriverContextCleanup;
EVT_WDF_OBJECT_CONTEXT_CLEANUP  OnDeviceCleanup;

EVT_WDF_FILE_CLEANUP            OnFileCleanup;

#ifdef ALLOC_PRAGMA
#pragma alloc_text (INIT, DriverEntry)
#pragma alloc_text (PAGE, PDAQ4EvtDeviceAdd)
#pragma alloc_text (PAGE, PDAQ4EvtDevicePrepareHardware)
#pragma alloc_text (PAGE, PDAQ4EvtDeviceReleaseHardware)
#pragma alloc_text (PAGE, PDAQ4EvtDeviceD0Exit)
#pragma alloc_text (PAGE, PDAQ4InitializeDeviceExtension)
#pragma alloc_text (PAGE, OnDriverContextCleanup)
#pragma alloc_text (PAGE, OnDeviceCleanup)
#endif

// Device driver entry method
NTSTATUS DriverEntry(PDRIVER_OBJECT DriverObject, PUNICODE_STRING RegistryPath)
{
	WPP_INIT_TRACING(DriverObject, RegistryPath);

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "%!FUNC! Enter");
	//WPP_CLEANUP(DriverObject);

	NTSTATUS status;
	WDF_DRIVER_CONFIG config;
	WDF_OBJECT_ATTRIBUTES attributes;

	WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
	attributes.EvtCleanupCallback = OnDriverContextCleanup;
	//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4: The version is 1.0\n"));
	//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4: DriverEntry\n"));

	WDF_DRIVER_CONFIG_INIT(&config, PDAQ4EvtDeviceAdd);
	status = WdfDriverCreate(DriverObject, RegistryPath, &attributes, &config, WDF_NO_HANDLE);

	if (!NT_SUCCESS(status))
	{
		TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfDriverCreate failed %!STATUS!", status);
		WPP_CLEANUP(DriverObject);
		return status;
	}

	return status;
}

// Device addition callback
NTSTATUS PDAQ4EvtDeviceAdd(
	_In_	WDFDRIVER Driver,
	_In_    PWDFDEVICE_INIT DeviceInit)
{
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");

	WDF_PNPPOWER_EVENT_CALLBACKS		pnpPowerCallbacks;
	WDF_OBJECT_ATTRIBUTES				attributes;
	PDEVICE_EXTENSION					devExt = NULL;
	WDFDEVICE							hDevice;
	WDF_FILEOBJECT_CONFIG				fileConfig;
	NTSTATUS							status;

	UNREFERENCED_PARAMETER(Driver);

	PAGED_CODE();

	//DECLARE_CONST_UNICODE_STRING(ntDeviceName, NT_DEVICE_NAME);
	//DECLARE_CONST_UNICODE_STRING(dosDeviceName, DOS_DEVICE_NAME);

	// Configure the device to use direct I/O (no buffering)
	WdfDeviceInitSetIoType(DeviceInit, WdfDeviceIoDirect);

	//status = WdfDeviceInitAssignName(DeviceInit, &ntDeviceName);
	/*if (!NT_SUCCESS(status))
	{
		TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfDeviceInitAssignName failed %!STATUS!", status);
		return status;
	}*/

	WDF_PNPPOWER_EVENT_CALLBACKS_INIT(&pnpPowerCallbacks);

	pnpPowerCallbacks.EvtDevicePrepareHardware = PDAQ4EvtDevicePrepareHardware;
	pnpPowerCallbacks.EvtDeviceReleaseHardware = PDAQ4EvtDeviceReleaseHardware;
	pnpPowerCallbacks.EvtDeviceD0Entry = PDAQ4EvtDeviceD0Entry;
	pnpPowerCallbacks.EvtDeviceD0Exit = PDAQ4EvtDeviceD0Exit;

	WdfDeviceInitSetPnpPowerEventCallbacks(DeviceInit, &pnpPowerCallbacks);

	//WDF_FILEOBJECT_CONFIG_INIT(&fileConfig, PDAQ4EvtDeviceFileCreate, PDAQ4EvtFileClose, OnFileCleanup);
	//WdfDeviceInitSetFileObjectConfig(DeviceInit, &fileConfig, WDF_NO_OBJECT_ATTRIBUTES);

	WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&attributes, DEVICE_EXTENSION);

	attributes.SynchronizationScope = WdfSynchronizationScopeQueue;
	attributes.ExecutionLevel = WdfExecutionLevelPassive;
	attributes.EvtCleanupCallback = OnDeviceCleanup;

	status = WdfDeviceCreate(&DeviceInit, &attributes, &hDevice);
	if (!NT_SUCCESS(status)) {
		TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfDeviceCreate failed %!STATUS!", status);

		return status;
	}

	devExt = PDAQ4GetDeviceContext(hDevice);
	devExt->Device = hDevice;
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE,
		"     AddDevice PDO (0x%p) FDO (0x%p), DevContext (0x%p)",
		WdfDeviceWdmGetPhysicalDevice(hDevice),
		WdfDeviceWdmGetDeviceObject(hDevice), devExt);

	//
	// Tell the Framework that this device will need an interface
	//
	// NOTE: See the note in Public.h concerning this GUID value.
	//
	status = WdfDeviceCreateDeviceInterface(hDevice,
		(LPGUID)&GUID_PDAQ4_INTERFACE,
		NULL);

	if (!NT_SUCCESS(status)) {
		TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfDeviceCreateDeviceInterface failed %!STATUS!", status);

		return status;
	}

	/*status = WdfDeviceCreateSymbolicLink(hDevice, &dosDeviceName);
	if (!NT_SUCCESS(status))
	{
		TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfDeviceCreateSymbolicLink failed %!STATUS!", status);
		return status;
	}*/

	// TODO - Here is where we would set idle and wake settings

	// Initialize the device extension
	status = PDAQ4InitializeDeviceExtension(devExt);
	if (!NT_SUCCESS(status)) {
		TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "PDAQ4InitializeDeviceExtension failed %!STATUS!", status);
		//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4InitializeDeviceExtension() 0x%08X failed\n", status));
		return(status);
	}

	//status = PDAQ4InterruptCreate(devExt);

	/*if (!NT_SUCCESS(status)) {
		TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfDeviceCreateInterrupt failed %!STATUS!", status);
		return status;
	}*/

	return(status);
}

//NTSTATUS
//Pdaq4DriverCreateDevice(
//	_Inout_ PWDFDEVICE_INIT DeviceInit
//) {
//	WDF_PNPPOWER_EVENT_CALLBACKS    pnpPowerCallbacks;
//	WDF_OBJECT_ATTRIBUTES           deviceAttributes;
//	PDEVICE_EXTENSION                 devContext;
//	WDFDEVICE                       device;
//	WDF_FILEOBJECT_CONFIG           fileConfig;
//	NTSTATUS                        status;
//
//	DECLARE_CONST_UNICODE_STRING(ntDeviceName, NT_DEVICE_NAME);
//
//	PAGED_CODE();
//
//	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Entry");
//	WdfDeviceInitSetIoType(DeviceInit, WdfDeviceIoDirect);
//
//	status = WdfDeviceInitAssignName(DeviceInit, &ntDeviceName);
//	if (!NT_SUCCESS(status))
//	{
//		TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfDeviceInitAssignName failed %!STATUS!", status);
//		return status;
//	}
//
//	WDF_PNPPOWER_EVENT_CALLBACKS_INIT(&pnpPowerCallbacks);
//
//	pnpPowerCallbacks.EvtDevicePrepareHardware = PDAQ4EvtDevicePrepareHardware;
//	pnpPowerCallbacks.EvtDeviceReleaseHardware = PDAQ4EvtDeviceReleaseHardware;
//	pnpPowerCallbacks.EvtDeviceD0Entry = PDAQ4EvtDeviceD0Entry;
//	pnpPowerCallbacks.EvtDeviceD0Exit = PDAQ4EvtDeviceD0Exit;
//
//	WdfDeviceInitSetPnpPowerEventCallbacks(DeviceInit, &pnpPowerCallbacks);
//
//	WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&deviceAttributes, PDEVICE_EXTENSION);
//
//}

// PnP callback to prepare device hardware for use
NTSTATUS PDAQ4EvtDevicePrepareHardware(WDFDEVICE Device,
	WDFCMRESLIST ResourcesRaw,
	WDFCMRESLIST ResourcesTranslated) {
	NTSTATUS                         status = STATUS_SUCCESS;
	PDEVICE_EXTENSION                devExt;
	PCM_PARTIAL_RESOURCE_DESCRIPTOR  descTrans;
	PCM_PARTIAL_RESOURCE_DESCRIPTOR  descRaw;
	PHYSICAL_ADDRESS                 regsBasePA = { 0 };
	ULONG                            regsLength = 0;
	ULONG                           resCount;

	ULONG							 barNum = 0;

	PHYSICAL_ADDRESS				fpgaDmaPA = { 0 };
	PHYSICAL_ADDRESS				fpgaFunPA = { 0 };

	UNREFERENCED_PARAMETER(ResourcesRaw);

	

	PAGED_CODE();

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");

	//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4EvtDevicePrepareHardware Enter\n"));

	devExt = PDAQ4GetDeviceContext(Device);
	resCount = WdfCmResourceListGetCount(ResourcesTranslated);
	ULONG interruptTypeIndex = 0;
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Resource Count = %u\n", resCount);
	// Loop through the resource descriptors supplied to the driver
	for (ULONG i = 0; i < resCount; i++) {
		descRaw = WdfCmResourceListGetDescriptor(ResourcesRaw, i);
		descTrans = WdfCmResourceListGetDescriptor(ResourcesTranslated, i);
		if (!descTrans) {
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfCmResourceListGetDescriptor() 0x%08X failed\n", status));
			return(STATUS_DEVICE_CONFIGURATION_ERROR);
		}

		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Resource Index = %u\n", i);

		// Resolve the type of resource provided
		switch (descTrans->Type) {
		case CmResourceTypeMemory:
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " - Memory Resource [%I64X-%I64X]\n",
				descTrans->u.Memory.Start.QuadPart,
				(descTrans->u.Memory.Start.QuadPart + descTrans->u.Memory.Length)));

			// Only one base address register is expected, and we will map it.
			//LZD
			regsBasePA = descTrans->u.Memory.Start;
			regsLength = descTrans->u.Memory.Length;

			if (0 == barNum && 0x200UL == regsLength)
			{
				devExt->FpgaDmaRegBase = MmMapIoSpaceEx(regsBasePA, regsLength, PAGE_READWRITE | PAGE_NOCACHE);
				//

				if (!devExt->FpgaDmaRegBase)
				{
					KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " !-> Unable to map BAR0 memory %08I64X, length %d",
						regsBasePA.QuadPart,
						regsLength));

					status = STATUS_INSUFFICIENT_RESOURCES;
				}
				else
				{
					devExt->FpgaDmaRegLength = regsLength;
					fpgaDmaPA = regsBasePA;
				}

				//status = DmaDescriptorsCreate(devExt);
				if (!NT_SUCCESS(status))
				{
					KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " DmaDescriptorsCreate failed %!STATUS!",
						status));

					break;
				}
			}
			else if (2 == barNum)
			{
				//devExt->RegsBase = MmMapIoSpaceEx(regsBasePA, regsLength, PAGE_READWRITE | PAGE_NOCACHE);

				devExt->FpgaFunRegBase = MmMapIoSpaceEx(regsBasePA, regsLength, PAGE_READWRITE | PAGE_NOCACHE);

				devExt->RegsBase = devExt->FpgaFunRegBase;

				TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "RegsBase = %p\n", (devExt->RegsBase));
				//TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "FpgaFunRegBase = %p\n", (devExt->FpgaFunRegBase));

				//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "FpgaFunRegBase address %08I64X\n", devExt->FpgaFunRegBase));

				if (!devExt->RegsBase)
				{
					KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " !-> Unable to map BAR2 memory %08I64X, length %d",
						regsBasePA.QuadPart,
						regsLength));
					status = STATUS_INSUFFICIENT_RESOURCES;
				}
				else
				{
					devExt->RegsLength = regsLength;
					devExt->FpgaFunRegLength = regsLength;
					fpgaFunPA = regsBasePA;
				}
			}
			else if (4 == barNum)
			{
			}

			//LZD
			break;

		case CmResourceTypePort:
			TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE,
				" - Port Resource [%08I64X-%08I64X] BAR%d",
				descTrans->u.Port.Start.QuadPart,
				descTrans->u.Port.Start.QuadPart + descTrans->u.Port.Length,
				barNum);
			/*KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " - Port   Resource [%08I64X-%08I64X] %s\n",
				descTrans->u.Port.Start.QuadPart,
				(descTrans->u.Port.Start.QuadPart + descTrans->u.Port.Length - 1)));*/
			break;
		case CmResourceTypeInterrupt:
			interruptTypeIndex++;
			
			TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "CmResourceTypeInterrupt Enter,descTrans -> Flags = %08x\n", (descTrans->Flags));
			if (descTrans->Flags & CM_RESOURCE_INTERRUPT_MESSAGE)
			{
				//TODO
				status = PDAQ4InterruptCreate(devExt, descRaw, descTrans);
			}
			else
			{
				KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " - Interrupt Resource (Non-MSI)"));

				/*TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE,
					" - Interrupt Resource (Non-MSI)");*/
			}
			break;

			/*KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " Interrupt Vector is %X\n",
				descTrans->u.Interrupt.Vector));
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, " MSI message count is %X\n",
				descTrans->u.MessageInterrupt.Raw.MessageCount));*/

		default:
			//
			// Ignore all other descriptors
			KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Unrecognized CM resource type %d\n", descTrans->Type));
			break;
		}

		++barNum;
	}

	// Map the register space as uncacheable memory-mapped I/O, unless this is a
	// dummy device node created by devcon.
	//devExt->RegsLength = regsLength;

	//LZD
	//devExt->FPGARegister.length = regsLength;
	if (devExt->FpgaDmaRegLength > 0 && devExt->FpgaFunRegLength > 0) {
		//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Mapped %d bytes of memory at %p\n", devExt->RegsLength, devExt->RegsBase));
		//devExt->RegsBase = (PUCHAR)MmMapIoSpace(regsBasePA, regsLength, MmNonCached);

		//bar0��ַ
		devExt->FPGARegister.length = devExt->FpgaDmaRegLength;
		devExt->FPGARegister.pvKernel = devExt->FpgaDmaRegBase;
		devExt->FPGARegister.physAddr = fpgaDmaPA;

		//bar2��ַ
		devExt->FPGARegisterFunc.length = devExt->FpgaFunRegLength;
		devExt->FPGARegisterFunc.pvKernel = devExt->FpgaFunRegBase;
		devExt->FPGARegisterFunc.physAddr = fpgaFunPA;

		//devExt->ConfigSpace = (PPDAQ4_REG)devExt->FpgaFunRegBase;

		devExt->FpgaDmaDescClrRegWrite = devExt->FpgaDmaRegBase + OFFSET_WRITE_CONTROLLER;
		devExt->FpgaDmaDescClrRegRead = devExt->FpgaDmaRegBase + OFFSET_READ_CONTROLLER;

		//devExt->RegWriteDMADescriptorBase = devExt->FpgaDmaRegBase + OFFSET_DMA_WRITE_DESCRIPTOR;
		//devExt->RegReadDMADescriptorBase = devExt->FpgaDmaRegBase + OFFSET_DMA_READ_DESCRIPTOR;
		//devExt->RegTransferControlBase = devExt->FpgaDmaRegBase + OFFSET_TRANSFTER_CONTROL;
	}
	else {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "Dummy device node\n"));
		devExt->RegsBase = NULL;
	}

	//Framer_Config_Regs framer_rt = (Framer_Config_Regs)devExt->FpgaFunRegBase + OFFSET_RT_FRAMER


	//write to host memory via DMA
	status = PDAQ4CreateDMAWriteCommonBuffer(devExt);

	if (!NT_SUCCESS(status)) {
		return status;
	}

	// read from host memory via DMA
	/*status = PDAQ4CreateDMAReadCommonBuffer(devExt);W

	if (!NT_SUCCESS(status)) {
		return status;
	}*/
	//LZD
	//Default DMA Init set the default size of RT and RF Frame

	//LZD
	status = PDAQ4InitWriteDMA(devExt, FRAME_SIZE(VECTOR_NUM_PER_FRAME_256, SAMPLE_PER_VECTOR_1024, RF_SAMPLE_BYTES), FRAME_SIZE(VECTOR_NUM_PER_FRAME_256, SAMPLE_PER_VECTOR_256, RT_SAMPLE_BYTES));
	if (!NT_SUCCESS(status)) {
		return status;
	}


	Framer_Config_Regs_PTR pRegRF = (Framer_Config_Regs_PTR)(devExt->FpgaFunRegBase + OFFSET_RF_FRAMER);
	Framer_Config_Regs_PTR pRegRT = (Framer_Config_Regs_PTR)(devExt->FpgaFunRegBase + OFFSET_RT_FRAMER);


	ULONG rf_framer_msi_data = READ_REGISTER_ULONG((PULONG)&(pRegRF->MSI_Data));
	ULONG rf_framer_version = READ_REGISTER_ULONG((PULONG)&pRegRF->Version);
	ULONG rf_framer_status = READ_REGISTER_ULONG((PULONG)&pRegRF->Status);
	

	


	ULONG rt_framer_msi_data = READ_REGISTER_ULONG((PULONG)&pRegRT->MSI_Data);
	ULONG rt_framer_version = READ_REGISTER_ULONG((PULONG)&pRegRT->Version);
	ULONG rt_framer_status = READ_REGISTER_ULONG((PULONG)&pRegRT->Status);

	WRITE_REGISTER_ULONG((PULONG)((PUCHAR)pRegRT + 0x80), 0x11223344);
	WRITE_REGISTER_ULONG((PULONG)((PUCHAR)pRegRT + 0x84), 0x55667788);
	WRITE_REGISTER_ULONG((PULONG)((PUCHAR)pRegRT + 0x88), 0x12345678);
	//WRITE_REGISTER_ULONG((PULONG)(&pRegRT + 0x8C), 0xCCDDEEFF);
	WRITE_REGISTER_ULONG((PULONG)((PUCHAR)pRegRT + 0xC8), 0xAAAAAAAA);
	WRITE_REGISTER_ULONG((PULONG)((PUCHAR)pRegRT + 0xCC), 0xBBBBBBBB);
	WRITE_REGISTER_ULONG((PULONG)((PUCHAR)pRegRT + 0xD0), 0xCCCCCCCC);
	WRITE_REGISTER_ULONG((PULONG)((PUCHAR)pRegRT + 0xD4), 0xDDDDDDDD);
	WRITE_REGISTER_ULONG((PULONG)((PUCHAR)pRegRT + 0xD8), 0xEEEEEEEE);
	WRITE_REGISTER_ULONG((PULONG)((PUCHAR)pRegRT + 0xDC), 0xFFFFFFFF);


	

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rf_framer_version hex = %x\n", rf_framer_version);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rf_framer_status hex = %x\n", rf_framer_status);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rf_framer_msi_data hex = %x\n", rf_framer_msi_data);

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rf_framer_version dec = %u\n", rf_framer_version);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rf_framer_status dec = %u\n", rf_framer_status);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rf_framer_msi_data dec = %u\n", rf_framer_msi_data);

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rt_framer_version hex = %x\n",  rt_framer_version);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rt_framer_status hex = %x\n",   rt_framer_status);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rt_framer_msi_data hex = %x\n", rt_framer_msi_data);

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rt_framer_version dec = %u\n",  rt_framer_version);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rt_framer_status dec = %u\n",   rt_framer_status);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rt_framer_msi_data dec = %u\n", rt_framer_msi_data);

	WRITE_REGISTER_ULONG((PULONG)&pRegRF->MSI_Data, 2);

	WRITE_REGISTER_ULONG((PULONG)&pRegRT->MSI_Data, 3);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "_________________________________");

	rf_framer_msi_data = READ_REGISTER_ULONG((PULONG)&pRegRF->MSI_Data);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rf_framer_msi_data hex = %x\n", rf_framer_msi_data);

	rt_framer_msi_data = READ_REGISTER_ULONG((PULONG)&pRegRT->MSI_Data);

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "rt_framer_msi_data hex = %x\n", rt_framer_msi_data);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "InterruptTypeCount = %d\n", interruptTypeIndex);

	
	return(status);
}

NTSTATUS PDAQ4EvtDeviceReleaseHardware(WDFDEVICE Device, WDFCMRESLIST ResourcesTranslated) {
	NTSTATUS          status = STATUS_SUCCESS;
	PDEVICE_EXTENSION devExt;
	UNREFERENCED_PARAMETER(ResourcesTranslated);
	PAGED_CODE();
	//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4EvtDeviceReleaseHardware Enter\n"));
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");
	//devExt = PDAQ4GetDeviceContext(Device);
	//UnmapSharedMemory(&devExt->FPGARegister);
	//for (ULONG i = 0; i < devExt->Shared_Resource_Num; i++)
	//{
	//	UnmapSharedMemory(&devExt->SharedResourceList[i]);
	//}
	//// Unmap the register space
	//if (devExt->RegsBase) {
	//	MmUnmapIoSpace(devExt->RegsBase, devExt->RegsLength);
	//	devExt->RegsBase = NULL;
	//}
	//devExt = DeviceGetContext(Device);

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4EvtDeviceReleaseHardware exit\n"));
	return(status);
}

NTSTATUS PDAQ4EvtDeviceD0Entry(WDFDEVICE Device, WDF_POWER_DEVICE_STATE PreviousState) {
	NTSTATUS status = STATUS_SUCCESS;
	PDEVICE_EXTENSION devExt;
	UNREFERENCED_PARAMETER(PreviousState);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");

	//devExt = PDAQ4GetDeviceContext(Device);
	//PDAQ4InitRead(devExt);
	//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4EvtDeviceD0Entry()\n"));

	return(status);
}

NTSTATUS PDAQ4EvtDeviceD0Exit(WDFDEVICE Device, WDF_POWER_DEVICE_STATE TargetState) {
	NTSTATUS status = STATUS_SUCCESS;

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");
	UNREFERENCED_PARAMETER(Device);
	UNREFERENCED_PARAMETER(TargetState);
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4EvtDeviceD0Exit()\n"));

	return(status);
}

VOID PDAQ4EvtDeviceFileCreate(WDFDEVICE Device, WDFREQUEST Request, WDFFILEOBJECT FileObject) {
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");
	UNREFERENCED_PARAMETER(Device);
	UNREFERENCED_PARAMETER(FileObject);
	UNREFERENCED_PARAMETER(Request);
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4EvtDeviceFileCreate()\n"));
}

VOID PDAQ4EvtFileClose(WDFFILEOBJECT FileObject) {
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");
	UNREFERENCED_PARAMETER(FileObject);
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4EvtFileClose()\n"));
}

// Initializes the device extension; in essence, this is where the device-specific
// aspects of the driver are configured.
NTSTATUS PDAQ4InitializeDeviceExtension(IN PDEVICE_EXTENSION DevExt) {
	NTSTATUS            status;
	WDF_IO_QUEUE_CONFIG queueWriteConfig;
	WDF_IO_QUEUE_CONFIG queueReadConfig;
	WDF_IO_QUEUE_CONFIG queueDeviceControlConfig;

	PAGED_CODE();
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4InitializeDeviceExtension()---------------\n"));

	//LZD
	//return 0;

	status = 0;

	// Initialize the reconfiguration state space

	/*DevExt->block_count = 0;
	DevExt->words_left = 0;*/

	WDF_IO_QUEUE_CONFIG_INIT(&queueWriteConfig,
		WdfIoQueueDispatchSequential);

	queueWriteConfig.EvtIoWrite = PDAQ4EvtIoWrite;

	__analysis_assume(queueWriteConfig.EvtIoStop != 0);
	status = WdfIoQueueCreate(DevExt->Device,
		&queueWriteConfig,
		WDF_NO_OBJECT_ATTRIBUTES,
		&DevExt->WriteQueue);
	__analysis_assume(queueWriteConfig.EvtIoStop == 0);

	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfIoQueueCreate failed: %!STATUS!", status));
		return status;
	}

	//
	// Set the Write Queue forwarding for IRP_MJ_WRITE requests.
	//
	status = WdfDeviceConfigureRequestDispatching(DevExt->Device,
		DevExt->WriteQueue,
		WdfRequestTypeWrite);

	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "DeviceConfigureRequestDispatching failed 1: %!STATUS!", status));
		return status;
	}

	// Read Queue Configuration

	WDF_IO_QUEUE_CONFIG_INIT(&queueReadConfig,
		WdfIoQueueDispatchSequential);

	queueReadConfig.EvtIoRead = PDAQ4EvtIoRead;

	__analysis_assume(queueReadConfig.EvtIoStop != 0);
	status = WdfIoQueueCreate(DevExt->Device,
		&queueReadConfig,
		WDF_NO_OBJECT_ATTRIBUTES,
		&DevExt->ReadQueue);
	__analysis_assume(queueReadConfig.EvtIoStop == 0);

	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfIoQueueCreate failed: %!STATUS!", status));
		return status;
	}

	//
	// Set the Write Queue forwarding for IRP_MJ_WRITE requests.
	//
	status = WdfDeviceConfigureRequestDispatching(DevExt->Device,
		DevExt->ReadQueue,
		WdfRequestTypeRead);

	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "DeviceConfigureRequestDispatching failed 2: %!STATUS!", status));
		return status;
	}

	// Device Control Queue Configuration

	WDF_IO_QUEUE_CONFIG_INIT(&queueDeviceControlConfig,
		WdfIoQueueDispatchSequential);

	queueDeviceControlConfig.EvtIoDeviceControl = PDAQ4EvtIoDeviceControl;

	__analysis_assume(queueDeviceControlConfig.EvtIoStop != 0);
	status = WdfIoQueueCreate(DevExt->Device,
		&queueDeviceControlConfig,
		WDF_NO_OBJECT_ATTRIBUTES,
		&DevExt->DeviceControlQueue);
	__analysis_assume(queueDeviceControlConfig.EvtIoStop == 0);

	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfIoQueueCreate failed: %!STATUS!", status));
		return status;
	}

	//
	// Set the Write Queue forwarding for IRP_MJ_WRITE requests.
	//
	status = WdfDeviceConfigureRequestDispatching(DevExt->Device,
		DevExt->DeviceControlQueue,
		WdfRequestTypeDeviceControl);

	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "DeviceConfigureRequestDispatching failed 3: %!STATUS!", status));
		return status;
	}

	//
	// Create a WDFINTERRUPT object.
	//
	/*status = PDAQ4InterruptCreate(DevExt);

	if (!NT_SUCCESS(status)) {
		return status;
	}*/

	//KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "PDAQ4InterruptCreate!!!!! 3: %!STATUS!", status));
	return(status);
}

// Event handler for write IRPs
VOID PDAQ4EvtIoWrite(IN WDFQUEUE   Queue,
	IN WDFREQUEST Request,
	IN size_t     Length) {
	PMDL              mdl;
	PULONG32          wordPtr, wordPtr2;
	size_t             length;
	NTSTATUS          status = STATUS_UNSUCCESSFUL;
	PDEVICE_EXTENSION devExt = NULL;

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");
	//
	// Get the device extension from the Queue handle
	//
	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));

	// Get access to the bitstream data being written
	status = WdfRequestRetrieveInputWdmMdl(Request, &mdl);
	if (!NT_SUCCESS(status)) {
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfRequestRetrieveInputWdmMdl failed: STATUS = %d \n", status));
		return;
	}

	wordPtr = (PULONG32)MmGetSystemAddressForMdlSafe(mdl, NormalPagePriority);
	length = MmGetMdlByteCount(mdl);
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "MmGetMdlByteCount = %X \n", length));
	status = WdfRequestRetrieveInputBuffer(Request, 1, &wordPtr2, &length);

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfRequestRetrieveInputBuffer Length = %X \n", length));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "MmGetSystemAddressForMdlSafe = %X \n", wordPtr));
	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_ERROR_LEVEL, "WdfRequestRetrieveInputBuffer = %X \n", wordPtr2));

	_Analysis_assume_(length > 0);

	// If the device has I/O space mapped, write the words out to the PCIe hardware.
	if (devExt->RegsLength > 0) {
		UINT32 wordIndex;
		UINT32 numWords = (length / PDAQ4_BYTES_PER_WORD);

		for (wordIndex = 0; wordIndex < numWords; wordIndex++) {
			// Write to the base address; the entire region is mapped for reconfiguration data.
			WRITE_REGISTER_ULONG((PULONG)devExt->RegsBase, *wordPtr++);
		}
	}

	// Indicate that the write completed with the requested number of bytes
	//status = STATUS_NOT_IMPLEMENTED;
	WdfRequestCompleteWithInformation(Request, status, Length);
}

VOID
OnDeviceCleanup(
	_In_ WDFOBJECT Device
)
{
	PDEVICE_EXTENSION devContext;

	PAGED_CODE();

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Entry");
	//devContext = DeviceGetContext((WDFDEVICE)Device);
	//WdfIoQueuePurge(devContext->RTNotificationQueue, WDF_NO_EVENT_CALLBACK, WDF_NO_CONTEXT);
	//WdfIoQueuePurge(devContext->RFNotificationQueue, WDF_NO_EVENT_CALLBACK, WDF_NO_CONTEXT);
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Exit");
}

VOID
OnDriverContextCleanup(
	_In_ WDFOBJECT DriverObject
)
/*++
Routine Description:

	Free all the resources allocated in DriverEntry.

Arguments:

	DriverObject - handle to a WDF Driver object.

Return Value:

	VOID.

--*/
{
	//UNREFERENCED_PARAMETER(DriverObject);

	PAGED_CODE();

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "%!FUNC! Entry");

	//
	// Stop WPP Tracing
	//
	WPP_CLEANUP(WdfDriverWdmGetDriverObject((WDFDRIVER)DriverObject));
}

VOID
OnFileCleanup(
	_In_ WDFFILEOBJECT  FileObject
)
{
	PAGED_CODE();

	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Entry");
}

// Event Device Control

VOID PDAQ4EvtIoDeviceControl(WDFQUEUE Queue, WDFREQUEST Request, size_t OutputBufferLength, size_t InputBufferLength, ULONG IoControlCode)
{
	//NTSTATUS                status = STATUS_SUCCESS;
	//WDFDEVICE               hDevice;
	WDF_REQUEST_PARAMETERS  params;
	PDEVICE_EXTENSION devExt = NULL;
	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Enter");
	//For Disalbe the warning if those two vairalbes are not used
	UNREFERENCED_PARAMETER(OutputBufferLength);
	UNREFERENCED_PARAMETER(InputBufferLength);

	KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "PDAQ4EvtIoDeviceControl IS CALLED %X CTL Code is \n", IoControlCode));

	devExt = PDAQ4GetDeviceContext(WdfIoQueueGetDevice(Queue));
	WDF_REQUEST_PARAMETERS_INIT(&params);
	WdfRequestGetParameters(Request, &params);

	/*switch (IoControlCode)
	{
	case IOCTL_INIT:
	case IOCTL_START_ACQ: {
		Framer_Config_Regs_PTR pRegRF = (Framer_Config_Regs_PTR)(devExt->FpgaFunRegBase + OFFSET_RF_FRAMER);
		Framer_Config_Regs_PTR pRegRT = (Framer_Config_Regs_PTR)(devExt->FpgaFunRegBase + OFFSET_RT_FRAMER);

		WRITE_REGISTER_ULONG((PULONG)(&pRegRT->Control), 0);
	}

	default:
		break;
	}*/

	switch (IoControlCode)
	{
	case IOCTL_PDAQ4_WRITE_CONFIG_REG:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_WriteConfigREG(Queue, Request, &params);
		break;

	case IOCTL_PDAQ4_READ_CONFIG_REGS:

		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_ReadConfigREGs(Queue, Request, &params);
		break;

	case IOCTL_PDAQ4_READ_CONFIG_LENGTH:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_ReadConfigLength(Queue, Request, &params);
		break;

	case IOCTL_PDAQ4_WRITE_CONFIG_REG_BLOCK:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_WriteConfigREGBlock(Queue, Request, &params);
		break;

	case IOCTL_QUERY_RESOURCE:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_QueryResource(Queue, Request, &params);
		break;

	case IOCTL_INIT:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_Init(Queue, Request, &params);
		break;
	case IOCTL_GET_DMABUFFER:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "PDAQ4_IOCTL_QueryFIFO Enter");
		PDAQ4_IOCTL_QueryFIFO(Queue, Request, &params);
		break;
	case IOCTL_START_ACQ:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_StartACQ(Queue, Request, &params);
		break;
	case IOCTL_STOP_ACQ:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_StopACQ(Queue, Request, &params);
		break;
	case IOCTL_DMA_INIT:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_InitializeDMA(Queue, Request, &params);
		break;
	case IOCTL_CONNECT_FIFO:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_ConnectChannel(Queue, Request, &params);
		break;
	case IOCTL_DISCONNECT_FIFO:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_DisConnectChannel(Queue, Request, &params);
		break;
	case IOCTL_DIAG_QUERY_DEBUG_INFO:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_QueryDebugInfo(Queue, Request, &params);
		break;
	case IOCTL_GET_DBG_INFO:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_GetDBGInfo(Queue, Request, &params);
		break;
	case IOCTL_READ_REGISTER_VALUE:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_ReadRegisterValue(Queue, Request, &params);
		break;
	case IOCTL_WRITE_REGISTER_VALUE:
		ASSERT((IoControlCode & 0x3) == METHOD_BUFFERED);
		PDAQ4_IOCTL_WriteRegisterValue(Queue, Request, &params);
		break;
	default:
		KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "Invalid IOCTL request\n"));
		ASSERTMSG(FALSE, "Invalid IOCTL request\n");
		WdfRequestComplete(Request, STATUS_INVALID_DEVICE_REQUEST);
		break;
	}

	return;
}

//NTSTATUS
//DmaDescriptorsCreate(
//	_In_ PDEVICE_EXTENSION    devContext
//)
//{
//	NTSTATUS    status = STATUS_SUCCESS;
//	PRead_DMA_Descriptor_Controller_Registers    prReq;
//	PWrite_DMA_Descriptor_Controller_Registers   pwReq;
//	WDF_DMA_ENABLER_CONFIG                          dmaConfig;
//
//	PAGED_CODE();
//	//TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Entry");
//
//	do
//	{
//		prReq = (PRead_DMA_Descriptor_Controller_Registers)(devContext->FpgaDmaRegBase + OFFSET_READ_CONTROLLER);
//		pwReq = (PWrite_DMA_Descriptor_Controller_Registers)(devContext->FpgaDmaRegBase + OFFSET_WRITE_CONTROLLER);
//
//		if (!READ_REGISTER_ULONG((PULONG)&prReq->RD_TABLE_SIZE) && !READ_REGISTER_ULONG((PULONG)&pwReq->WR_TABLE_SIZE))
//		{
//			//TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Both lengths of DMA Read/Write descriptor tables are zeroes");
//			break;
//		}
//
//		WdfDeviceSetAlignmentRequirement(devContext->Device, FILE_32_BYTE_ALIGNMENT);
//		WDF_DMA_ENABLER_CONFIG_INIT(&dmaConfig, WdfDmaProfileScatterGather64Duplex, MAX_TRANSFER_CHUNK_SIZE);
//		status = WdfDmaEnablerCreate(devContext->Device, &dmaConfig, WDF_NO_OBJECT_ATTRIBUTES, &devContext->WriteDmaEnabler);
//		if (!NT_SUCCESS(status))
//		{
//			//TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "Failed to create DMA enabler for host reading");
//			break;
//		}
//
//		//WdfDmaEnablerSetMaximumScatterGatherElements(devContext->DmaEnabler, MAX_DMA_DESCRIPTOR_ENTRIES);
//
//		/*TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "The max transfer length is 0x%llx, the max fragment length is 0x%llx",
//			WdfDmaEnablerGetMaximumLength(devContext->DmaEnabler),
//			WdfDmaEnablerGetFragmentLength(devContext->DmaEnabler, WdfDmaDirectionReadFromDevice));*/
//
//		devContext->DescriptorStatusFIFOCommonBufferSize = sizeof(ReadWrite_DMA_Status_and_Descriptor_Table);
//		_Analysis_assume_(devContext->DescriptorStatusFIFOCommonBufferSize > 0);
//		status = WdfCommonBufferCreate(devContext->DmaEnabler,
//			devContext->DescriptorStatusFIFOCommonBufferSize,
//			WDF_NO_OBJECT_ATTRIBUTES,
//			&devContext->DescriptorStatusFIFOCommonBuffer);
//		if (!NT_SUCCESS(status))
//		{
//			TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfCommonBufferCreate (descriptor status FIFOs) failed: %!STATUS!", status);
//			break;
//		}
//		devContext->DescriptorStatusFIFOCommonBufferBase = WdfCommonBufferGetAlignedVirtualAddress(devContext->DescriptorStatusFIFOCommonBuffer);
//		devContext->DescriptorStatusFIFOReadCommonBufferLA = WdfCommonBufferGetAlignedLogicalAddress(devContext->DescriptorStatusFIFOCommonBuffer);
//		devContext->DescriptorStatusFIFOWriteCommonBufferLA.QuadPart =
//			devContext->DescriptorStatusFIFOReadCommonBufferLA.QuadPart + sizeof(Read_DMA_Status_and_Descriptor_Table);
//		RtlZeroMemory(devContext->DescriptorStatusFIFOCommonBufferBase, devContext->DescriptorStatusFIFOCommonBufferSize);
//		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE,
//			"DescriptorStatusFIFOCommonBuffer 0x%p(#0x%I64X), length %I64d",
//			devContext->DescriptorStatusFIFOCommonBufferBase,
//			devContext->DescriptorStatusFIFOReadCommonBufferLA.QuadPart,
//			WdfCommonBufferGetLength(devContext->DescriptorStatusFIFOCommonBuffer));
//
//		WRITE_REGISTER_ULONG((PULONG)(&prReq->RC_Read_Status_and_Descriptor_Base_High),
//			devContext->DescriptorStatusFIFOReadCommonBufferLA.HighPart);
//		WRITE_REGISTER_ULONG((PULONG)(&prReq->RC_Read_Status_and_Descriptor_Base_Low),
//			devContext->DescriptorStatusFIFOReadCommonBufferLA.LowPart);
//		if (0xFF == READ_REGISTER_ULONG((PULONG)&prReq->RD_DMA_LAST_PTR))
//		{
//			WRITE_REGISTER_ULONG((PULONG)(&prReq->EP_Read_Descriptor_FIFO_Base_High), 0);
//			WRITE_REGISTER_ULONG((PULONG)(&prReq->EP_Read_Descriptor_FIFO_Base_Low), 0x40012000);   // previous one: 0x10012000
//		}
//		WRITE_REGISTER_ULONG((PULONG)(&prReq->RD_TABLE_SIZE), MAX_DMA_DESCRIPTOR_ENTRIES - 1);
//		WRITE_REGISTER_ULONG((PULONG)(&prReq->RD_CONTROL), TRUE);
//
//		// To change the RC_Write_Status and Descriptor Base(Low) base address,
//		// all descriptors specified by the WR_TABLE_SIZE must be exhausted!
//		WRITE_REGISTER_ULONG((PULONG)(&pwReq->RC_Write_Status_and_Descriptor_Base_High),
//			devContext->DescriptorStatusFIFOWriteCommonBufferLA.HighPart);
//		WRITE_REGISTER_ULONG((PULONG)(&pwReq->RC_Write_Status_and_Descriptor_Base_Low),
//			devContext->DescriptorStatusFIFOWriteCommonBufferLA.LowPart);
//		if (0xFF == READ_REGISTER_ULONG((PULONG)&pwReq->WR_DMA_LAST_PTR))
//		{
//			WRITE_REGISTER_ULONG((PULONG)(&pwReq->EP_Write_Descriptor_FIFO_Base_High), 0);
//			WRITE_REGISTER_ULONG((PULONG)(&pwReq->EP_Write_Descriptor_FIFO_Base_Low), 0x40010000);  // previous one: 0x10010000
//		}
//		WRITE_REGISTER_ULONG((PULONG)(&pwReq->WR_TABLE_SIZE), MAX_DMA_DESCRIPTOR_ENTRIES - 1);
//		WRITE_REGISTER_ULONG((PULONG)(&pwReq->WR_CONTROL), TRUE);
//
//		devContext->WriteCommonBufferSize = (MAX_TRANSFER_CHUNK_SIZE + 4) * MAX_DMA_DESCRIPTOR_ENTRIES;
//		_Analysis_assume_(devContext->WriteCommonBufferSize > 0);
//		status = WdfCommonBufferCreate(devContext->DmaEnabler,
//			devContext->WriteCommonBufferSize,
//			WDF_NO_OBJECT_ATTRIBUTES,
//			&devContext->WriteCommonBuffer);
//		if (!NT_SUCCESS(status))
//		{
//			TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfCommonBufferCreate (write) failed: %!STATUS!", status);
//			break;
//		}
//		devContext->WriteCommonBufferBase = WdfCommonBufferGetAlignedVirtualAddress(devContext->WriteCommonBuffer);
//		devContext->WriteCommonBufferBaseLA = WdfCommonBufferGetAlignedLogicalAddress(devContext->WriteCommonBuffer);
//		RtlZeroMemory(devContext->WriteCommonBufferBase, devContext->WriteCommonBufferSize);
//		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE,
//			"WriteCommonBuffer 0x%p(#0x%I64X), length %I64d",
//			devContext->WriteCommonBufferBase,
//			devContext->WriteCommonBufferBaseLA.QuadPart,
//			WdfCommonBufferGetLength(devContext->WriteCommonBuffer));
//
//		devContext->ReadCommonBufferSize = (MAX_TRANSFER_CHUNK_SIZE + 4) * MAX_DMA_DESCRIPTOR_ENTRIES;
//		_Analysis_assume_(devContext->ReadCommonBufferSize > 0);
//		status = WdfCommonBufferCreate(devContext->DmaEnabler,
//			devContext->ReadCommonBufferSize,
//			WDF_NO_OBJECT_ATTRIBUTES,
//			&devContext->ReadCommonBuffer);
//		if (!NT_SUCCESS(status))
//		{
//			TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfCommonBufferCreate (read) failed %!STATUS!", status);
//			break;
//		}
//		devContext->ReadCommonBufferBase = WdfCommonBufferGetAlignedVirtualAddress(devContext->ReadCommonBuffer);
//		devContext->ReadCommonBufferBaseLA = WdfCommonBufferGetAlignedLogicalAddress(devContext->ReadCommonBuffer);
//		RtlZeroMemory(devContext->ReadCommonBufferBase, devContext->ReadCommonBufferSize);
//		TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE,
//			"ReadCommonBuffer 0x%p(#0x%I64X), length %I64d",
//			devContext->ReadCommonBufferBase,
//			devContext->ReadCommonBufferBaseLA.QuadPart,
//			WdfCommonBufferGetLength(devContext->ReadCommonBuffer));
//
//		devContext->DmaFrameMode = FRAME_MODE_RT_ONLY;   //by default
//
//		InitDmaDescriptorTable(devContext);
//		status = WdfDmaTransactionCreate(devContext->DmaEnabler, WDF_NO_OBJECT_ATTRIBUTES, &devContext->ReadDmaTransaction);
//		if (!NT_SUCCESS(status))
//		{
//			TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfDmaTransactionCreate (read) failed %!STATUS!", status);
//			break;
//		}
//
//		/*
//				WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
//				status = WdfDmaTransactionCreate(devContext->DmaEnabler, &attributes, &devContext->ReadDmaTransaction);
//				if (!NT_SUCCESS(status))
//				{
//					TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfDmaTransactionCreate (read) failed %!STATUS!", status);
//					break;
//				}
//				WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
//				status = WdfDmaTransactionCreate(devContext->DmaEnabler, &attributes, &devContext->WriteDmaTransaction);
//				if (!NT_SUCCESS(status))
//				{
//					TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfDmaTransactionCreate (write) failed %!STATUS!", status);
//					break;
//				}
//		*/
//	} while (FALSE);
//
//	TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "%!FUNC! Exit");
//	return status;
//}