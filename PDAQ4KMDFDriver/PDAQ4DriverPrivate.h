#if !defined(_PDAQ4_PRIVATE_H_)
#define _PDAQ4_PRIVATE_H_

// Enumerated type representing reconfiguration state
typedef enum {
	RECONFIG_INIT = 0,
	RECONFIG_LOADING,
	RECONFIG_COMPLETE,
	RECONFIG_ERROR
} ReconfigState;

#define MSG_INT_NUM 4
// Mapping the Kernal buffer into user mode
NTSTATUS  MapSharedMemory(IN OUT DrvSharedResource* pSm);
NTSTATUS  UnmapSharedMemory(IN OUT DrvSharedResource* pSm);

typedef struct _tagFifoConfig
{
	ULONG nItems;
	ULONG nAllocSize;
	ULONG nItemSize;
	ULONG nWrite;
	ULONG nRead;
} FIFO_CONFIG, * PFIFO_CONFIG;
//
// The device extension for the device object
//

typedef struct __DMA_ITEM
{
	LIST_ENTRY			le;
	PFifoEelment		rf_fifo_element;
	PFifoEelment		rt_fifo_element;
}DMA_ITEM, * PDMA_ITEM;

typedef struct __DMAList
{
	KSPIN_LOCK queueLock;
	KSPIN_LOCK doneLock;
	LIST_ENTRY queue_list;
	LIST_ENTRY done_list;
}DMAList;

typedef struct _DEVICE_EXTENSION {
	// Pointer back to the Windows device
	WDFDEVICE Device;

	// Following fields are specific to the hardware
	// Configuration

	// HW Resources
	PUCHAR RegsBase;         // Registers base address
	ULONG  RegsLength;       // Registers base length
	DrvSharedResource	FPGARegister;
	DrvSharedResource	FPGARegisterFunc;

	PUCHAR FpgaFunRegBase;         // Registers Function address
	ULONG  FpgaFunRegLength;       // Registers Function length

	PUCHAR FpgaDmaRegBase;
	ULONG  FpgaDmaRegLength;

	PUCHAR FpgaDmaDescClrRegWrite;  //Write DMA Descriptor Controller Registers address
	PUCHAR FpgaDmaDescClrRegRead;  //Read DMA Descriptor Controller Registers address
	//ULONG  FpgaDmaRegLength;

	// Write and device control queue
	WDFQUEUE WriteQueue;
	WDFQUEUE ReadQueue;
	WDFQUEUE DeviceControlQueue;

	//ZIDONG MODFI
	/*PRead_DMA_Descriptor_Controller_Registers DescReadControl;
	PWrite_DMA_Descriptor_Controller_Registers DescWriteControl;*/

	PUCHAR DescriptorTableCommonBufferBaseVa;
	PHYSICAL_ADDRESS WriteDescriptorTableCommonBufferBasePA;
	PHYSICAL_ADDRESS DescriptorTableCommonBufferBasePA;
	size_t                  DescriptorTableCommonBufferSize;
	WDFCOMMONBUFFER         DescriptorTableCommonBuffer;
	//// Descriptors
	//WDFCOMMONBUFFER         DescriptorStatusFIFOCommonBuffer;
	//size_t                  DescriptorStatusFIFOCommonBufferSize;
	//_Field_size_(DescriptorStatusFIFOCommonBufferSize) PUCHAR DescriptorStatusFIFOCommonBufferBase;
	//PHYSICAL_ADDRESS        DescriptorStatusFIFOReadCommonBufferLA; // Logical Address
	//PHYSICAL_ADDRESS        DescriptorStatusFIFOWriteCommonBufferLA; // Logical Address

	//// Write
	//ULONG                   WriteTransferElements;
	//WDFCOMMONBUFFER         WriteCommonBuffer;
	//size_t                  WriteCommonBufferSize;
	//_Field_size_(WriteCommonBufferSize) PUCHAR WriteCommonBufferBase;
	//PHYSICAL_ADDRESS        WriteCommonBufferBaseLA;  // Logical Address
	//WDFDMATRANSACTION 		WriteDmaTransaction;
	//// Read
	//ULONG                   ReadTransferElements;
	//WDFCOMMONBUFFER         ReadCommonBuffer;
	//size_t                  ReadCommonBufferSize;
	//_Field_size_(ReadCommonBufferSize) PUCHAR ReadCommonBufferBase;
	//PHYSICAL_ADDRESS        ReadCommonBufferBaseLA;   // Logical Address
	//WDFDMATRANSACTION 		ReadDmaTransaction;

	//ZIDONG MODFI

	// Reconfiguration state variables
	/*ReconfigState         reconfig_state;
	ULONG                 block_count;
	ULONG                 words_left;
	PUCHAR				  RegTransferControlBase;*/

	//Write to host via DMA Transcation
	PUCHAR					RegWriteDMADescriptorBase;

	WDFDMATRANSACTION		WriteDmaTransaction;
	ULONG					WriteMaximumTransferLength;
	WDFDMAENABLER			WriteDmaEnabler;
	ULONG                   WriteTransferElements;

	ULONG                  RFAllocFrameSize;
	ULONG                  RTAllocFrameSize;
	ULONG                  RFFrameSize;
	ULONG                  RTFrameSize;
	ULONG				   Frame_FIFO_size;

	ULONG					Shared_Resource_Num;
	DrvSharedResource		SharedResourceList[MAX_NUM_OF_SG_DESCRIPTOR];
	ULONG					SharedResourceSize;
	ULONG					SharedResource_Num_Per_RF;
	ULONG					SharedResource_Num_Per_RT;
	PDrvSharedFifo			PSharedFifo[FRAME_FIFO_NUM];

	// Read from host via DMA Transcation
	PUCHAR					RegReadDMADescriptorBase;
	WDFDMATRANSACTION		ReadDmaTransaction;
	ULONG					ReadMaximumTransferLength;
	WDFDMAENABLER			ReadDmaEnabler;
	ULONG                   ReadTransferElements;
	ULONG					Diag_Shared_Resource_Num;
	DrvSharedResource		Diag_SharedResourceList[MAX_NUM_OF_SG_DESCRIPTOR];
	ULONG					Diag_SharedResourceSize;
	ULONG					Diag_SharedResource_Num_Per_RAWRF;
	// only rf raw frame can be read from host via DMA, so only 1 raw frame fifo is necessary

	ULONG                  Diag_RawRFAllocFrameSize;
	ULONG                  Diag_RawRFFrameSize;
	ULONG				   Diag_RawFrame_FIFO_size;
	ULONG				   Diag_RawFrame_FIFO_Num;

	WDFINTERRUPT            Interrupt[MSG_INT_NUM];     // Returned by InterruptCreate

	PPDAQ4_REG				ConfigSpace;
	DMAList					DmaWriteList, DmaReadList;
	// for the DMA item in Write DMA List, hardware support 256 descriptors, 128 for RT and 128 for RF
	DMA_ITEM				DMA_WriteItems[MAX_NUM_OF_SG_DESCRIPTOR / 2];
	// for the DMA item in Read DMA List, hardware support 256 descriptors,256 for raw rf frame
	DMA_ITEM				DMA_ReadItems[MAX_NUM_OF_SG_DESCRIPTOR];

	// Debug Info
	DrvDebugInfo			debug_info;
}  DEVICE_EXTENSION, * PDEVICE_EXTENSION;

// This will generate the function named PDAQ4GetDeviceContext to be use for
// retreiving the DEVICE_EXTENSION pointer.
//
WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(DEVICE_EXTENSION, PDAQ4GetDeviceContext)

#if !defined(ASSOC_WRITE_REQUEST_WITH_DMA_TRANSACTION)
//
// The context structure used with WdfDmaTransactionCreate
//
typedef struct TRANSACTION_CONTEXT {
	WDFREQUEST     Request;
} TRANSACTION_CONTEXT, * PTRANSACTION_CONTEXT;

WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(TRANSACTION_CONTEXT, PDAQ4GetTransactionContext)

#endif

//
// Function prototypes
//
DRIVER_INITIALIZE DriverEntry;

EVT_WDF_DRIVER_DEVICE_ADD PDAQ4EvtDeviceAdd;

EVT_WDF_OBJECT_CONTEXT_CLEANUP PDAQ4EvtDriverContextCleanup;

EVT_WDF_DEVICE_D0_ENTRY PDAQ4EvtDeviceD0Entry;
EVT_WDF_DEVICE_D0_EXIT PDAQ4EvtDeviceD0Exit;
EVT_WDF_DEVICE_PREPARE_HARDWARE PDAQ4EvtDevicePrepareHardware;
EVT_WDF_DEVICE_RELEASE_HARDWARE PDAQ4EvtDeviceReleaseHardware;

EVT_WDF_IO_QUEUE_IO_WRITE PDAQ4EvtIoWrite;
EVT_WDF_IO_QUEUE_IO_READ PDAQ4EvtIoRead;

EVT_WDF_IO_QUEUE_IO_DEVICE_CONTROL PDAQ4EvtIoDeviceControl;

NTSTATUS
PDAQ4SetIdleAndWakeSettings(
	IN PDEVICE_EXTENSION FdoData
);

NTSTATUS
PDAQ4InitializeDeviceExtension(
	IN PDEVICE_EXTENSION DevExt
);

NTSTATUS
PDAQ4PrepareHardware(
	IN PDEVICE_EXTENSION DevExt,
	IN WDFCMRESLIST     ResourcesTranslated
);

NTSTATUS
PDAQ4InitWrite(
	IN PDEVICE_EXTENSION DevExt
);

NTSTATUS
PDAQ4InitializeHardware(
	IN PDEVICE_EXTENSION DevExt
);

VOID
PDAQ4Shutdown(
	IN PDEVICE_EXTENSION DevExt
);

VOID
PDAQ4HardwareReset(
	IN PDEVICE_EXTENSION    DevExt
);

NTSTATUS PDAQ4CreateDMAReadCommonBuffer(IN PDEVICE_EXTENSION DevExt);

NTSTATUS PDAQ4CreateDMAWriteCommonBuffer(IN PDEVICE_EXTENSION DevExt);

NTSTATUS PDAQ4InterruptCreate(IN PDEVICE_EXTENSION DevExt
	,
	_In_ PCM_PARTIAL_RESOURCE_DESCRIPTOR    ResourcesRaw,
	_In_ PCM_PARTIAL_RESOURCE_DESCRIPTOR    ResourceTranslated
);

NTSTATUS PDAQ4InitWriteDMADescriptorTable(IN PDEVICE_EXTENSION DevExt);

NTSTATUS PDAQ4InitReadDMADescriptorTable(IN PDEVICE_EXTENSION DevExt);

NTSTATUS PDAQ4InitFIFO(IN PDEVICE_EXTENSION DevExt);

NTSTATUS PDAQ4InitDiagFIFO(IN PDEVICE_EXTENSION DevExt);

NTSTATUS PDAQ4InitWriteDMA(IN PDEVICE_EXTENSION DevExt, ULONG n_rfframesize, ULONG n_rtframesize);

NTSTATUS PDAQ4InitReadDMA(IN PDEVICE_EXTENSION DevExt, ULONG n_rfrawframesize);

// IOControl Routine

VOID PDAQ4_IOCTL_InitializeDMA(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);

VOID PDAQ4_IOCTL_QueryFIFO(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);

VOID PDAQ4_IOCTL_WriteConfigREGBlock(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);

VOID PDAQ4_IOCTL_WriteConfigREG(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);

VOID PDAQ4_IOCTL_ReadConfigREGs(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);

VOID PDAQ4_IOCTL_ReadConfigLength(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);

VOID PDAQ4_IOCTL_WriteConfigREGBlock(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);

VOID PDAQ4_IOCTL_QueryResource(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);

VOID PDAQ4_IOCTL_Init(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);

VOID PDAQ4_IOCTL_StartACQ(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);

VOID PDAQ4_IOCTL_StopACQ(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);

VOID PDAQ4_IOCTL_QueryDebugInfo(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);
VOID PDAQ4_IOCTL_GetDBGInfo(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);
VOID PDAQ4_IOCTL_ReadRegisterValue(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);
VOID PDAQ4_IOCTL_WriteRegisterValue(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);

// Connect the event to interrupt
VOID PDAQ4_IOCTL_ConnectChannel(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);
VOID PDAQ4_IOCTL_DisConnectChannel(WDFQUEUE Queue, WDFREQUEST Request, WDF_REQUEST_PARAMETERS* param);
//

VOID PDAQ4ReadRequestComplete(
	IN WDFDMATRANSACTION  DmaTransaction,
	IN NTSTATUS           Status
);

NTSTATUS
PDAQ4EvtInterruptDisable(
	IN WDFINTERRUPT Interrupt,
	IN WDFDEVICE    Device
);

NTSTATUS
PDAQ4EvtInterruptEnable(
	IN WDFINTERRUPT Interrupt,
	IN WDFDEVICE    Device
);

VOID
PDAQ4EvtInterruptDpc(
	WDFINTERRUPT Interrupt,
	WDFOBJECT    Device
);

BOOLEAN
PDAQ4ProgramReadDma(
	IN  WDFDMATRANSACTION       Transaction,
	IN  WDFDEVICE               Device,
	IN  WDFCONTEXT              Context,
	IN  WDF_DMA_DIRECTION       Direction,
	IN  PSCATTER_GATHER_LIST    SgList
);

BOOLEAN
PDAQ4EvtInterruptIsr(
	IN WDFINTERRUPT Interrupt,
	IN ULONG        MessageID
);

NTSTATUS
PDAQ4InitRead(
	IN PDEVICE_EXTENSION DevExt
);

//
VOID  PDAQ4SetFIFOParameters(IN PDEVICE_EXTENSION DevExt, ULONG RTFrameSize, ULONG RFFrameSize);

VOID  PDAQ4SetDiagFIFOParameters(IN PDEVICE_EXTENSION DevExt, ULONG RFRawFrameSize);
NTSTATUS PDAQ4ProgramFPGA(PDEVICE_EXTENSION, PCWSTR);

// FIFO Operation
BOOLEAN FIFOInit(DrvSharedFifo* fifo, PFIFO_CONFIG);
BOOLEAN Write(DrvSharedFifo* fifo);
BOOLEAN IsOverFlow(DrvSharedFifo* fifo);
BOOLEAN FifoUpdateOnWritten(DrvSharedFifo* pFifo);
BOOLEAN FifoUpdateOnRead(DrvSharedFifo* pFifo);
// FPGA BIN IMAGE
HANDLE OpenImageFile(PCWSTR pcwPath);
NTSTATUS CloseImageFile(HANDLE hFile);
ULONG GetImageFileLength(HANDLE hFile);
NTSTATUS ReadImageFile(HANDLE hFile, PVOID pv, ULONG len);

#pragma warning(disable:4127) // avoid conditional expression is constant error with W4

#endif  // _PDAQ4_PRIVATE_H_