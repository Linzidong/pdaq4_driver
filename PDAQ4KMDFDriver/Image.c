#include "Precomp.h"
HANDLE OpenImageFile(PCWSTR pcwPath)
{
	NTSTATUS status;
	IO_STATUS_BLOCK IoStatus;
	HANDLE hFile = NULL;
	OBJECT_ATTRIBUTES oa;
	UNICODE_STRING str;

	do
	{

		if (KeGetCurrentIrql() != PASSIVE_LEVEL)
		{
			status = STATUS_INVALID_DEVICE_STATE;
			break;
		}

		RtlInitUnicodeString(&str, pcwPath);

		InitializeObjectAttributes(&oa, &str, 0, NULL, NULL);
		status = ZwCreateFile(
			&hFile,
			FILE_GENERIC_READ | SYNCHRONIZE,
			&oa,
			&IoStatus,
			NULL,
			0,
			FILE_SHARE_READ,
			FILE_OPEN,
			FILE_NON_DIRECTORY_FILE | FILE_SYNCHRONOUS_IO_NONALERT,
			NULL,
			0
			);
	} while (FALSE);

	return (status == STATUS_SUCCESS) ? hFile : NULL;
}


NTSTATUS CloseImageFile(HANDLE hFile)
{
	NTSTATUS status = STATUS_SUCCESS;

	if (hFile != NULL)
	{
		status = ZwClose(hFile);
	}
	return status;


}

///////////////////////////////////////////////////////////////////////////////////
ULONG GetImageFileLength(HANDLE hFile)
{
	NTSTATUS status;
	IO_STATUS_BLOCK IoStatus;
	FILE_STANDARD_INFORMATION Info;

	if (hFile == NULL)
	{
		return 0;
	}

	status = ZwQueryInformationFile(
		hFile,
		&IoStatus,
		&Info,
		sizeof(FILE_STANDARD_INFORMATION),
		FileStandardInformation
		);

	if (status == STATUS_SUCCESS)
	{
		return (ULONG)Info.EndOfFile.LowPart;
	}

	return 0;
}


NTSTATUS ReadImageFile(HANDLE hFile, PVOID pv, ULONG len)
{
	IO_STATUS_BLOCK IoStatus;
	NTSTATUS status = ZwReadFile(
		hFile,
		NULL,
		NULL,
		NULL,
		&IoStatus,
		pv,
		len,
		NULL,
		NULL
		);
	//if ((ULONG)IoStatus.Information == len)
	//  return STATUS_SUCCESS;
	return status;
}

