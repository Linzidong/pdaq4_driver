#include <Windows.h>
#include "DriverAgent.h"
#include <SDKDDKVer.h>
#include <stdio.h>
#include <tchar.h>
#include <setupapi.h>
#include <stdlib.h>
#include "DriverAgent.h"
#include "../PDAQ4KMDFDriver/RegPDAQ4.h"
#define INITGUID
#include "../PDAQ4KMDFDriver/PDAQ4DriverPublic.h"
using namespace cli;
using namespace std;
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
using namespace System::Diagnostics;
using namespace System::Runtime::InteropServices;
using namespace System;
using namespace PDAQ4DriverAgent::PDAQ4Implementation;

#define OFFSET_VECTOR_PER_FRAME_REG 0x0020
#define OFFSET_SAMPLE_VEC_REG 0x0024
#define OFFSET_SAMPLE_VEC_REG 0x0028
#define OFFSET_WRITE_DMA_START 0x35
#define OFFSET_READ_DMA_START 0x36
#define OFFSET_INTERRUPT_ENABLE 0x6
#define INT_MASK_DMA_WRITE		   1
#define INT_MASK_DMA_READ          2
#define INT_MASK_DMA_HOST_LATENCY  4
#define READ_REG_NUM (64)
void OutputDebugPrintf(const char* strOutputString, ...)
{
	char strBuffer[4096] = { 0 };
	va_list vlArgs;
	va_start(vlArgs, strOutputString);
	_vsnprintf_s(strBuffer, sizeof(strBuffer) - 1, strOutputString, vlArgs);
	va_end(vlArgs);
	OutputDebugString(strBuffer);
}
PBYTE getDeviceProperty(HDEVINFO infoSet, PSP_DEVINFO_DATA infoData, DWORD property)
{
	PBYTE propBuffer = NULL;
	ULONG resourceSize = 0;
	BOOL  success;

	// Retrieve the device property, dynamically allocating a buffer.
	SetupDiGetDeviceRegistryProperty(infoSet,
		infoData,
		property,
		NULL,
		NULL,
		0,
		&resourceSize);

	// The call should fail with an insufficient buffer
	if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
		propBuffer = (PBYTE)malloc(resourceSize);
		if (propBuffer != NULL) {
			// Fetch the string into the allocated buffer
			success = SetupDiGetDeviceRegistryProperty(infoSet,
				infoData,
				property,
				NULL,
				(PBYTE)propBuffer,
				resourceSize,
				NULL);
			if (success == FALSE) {
				// Free the allocated buffer and set it to NULL
				free(propBuffer);
				propBuffer = NULL;
			}
		}
	}

	// Return the dynamically-allocated buffer, or NULL
	return(propBuffer);
}

DriverAgent::DriverAgent() {
	gDeviceHandle = INVALID_HANDLE_VALUE;
}
IPDAQDevice^ DriverAgent::FindPDAQ4()
{
	UINT32 instanceCount;
	SP_DEVICE_INTERFACE_DATA interfaceData;
	SP_DEVINFO_DATA infoData;
	HDEVINFO infoSet;
	PDAQDevice^ deviceinfo = gcnew PDAQDevice();
	// Retrieve the device information for all PDAQ4 devices.
	infoSet = SetupDiGetClassDevs(&GUID_PDAQ4_INTERFACE, NULL, NULL, (DIGCF_DEVICEINTERFACE | DIGCF_PRESENT));

	// Initialize the device interface and info data structures.
	interfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
	infoData.cbSize = sizeof(SP_DEVINFO_DATA);

	// Iterate through all the devices found in the system by the public driver GUID.
	// We just get the first device on bus, because normally there will not such case to have two cards on bus.
	instanceCount = 0;
	while (SetupDiEnumDeviceInterfaces(infoSet,
		NULL,
		&GUID_PDAQ4_INTERFACE,
		instanceCount,
		&interfaceData))
	{
		PSP_DEVICE_INTERFACE_DETAIL_DATA interfaceDetails;
		ULONG  resourceSize;
		BOOL   success;
		TCHAR* deviceName;
		TCHAR* hardwareId;

		// Get details about the device's interface.  Begin by dynamically determining the
		// size of the interface details structure to be returned.
		SetupDiGetDeviceInterfaceDetail(infoSet,
			&interfaceData,
			NULL,
			0,
			&resourceSize,
			NULL);

		// see remark http://msdn.microsoft.com/en-us/library/windows/hardware/ff551120%28v=vs.85%29.aspx
		// The error must be ERROR_INSUFFICIENT_BUFFER, Before the last step is for getting the size of detail Interface Description

		interfaceDetails = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(resourceSize);

		// Repeat the call, with an allocated details structure.  Set the cbSize member
		// to the fixed size of the structure.
		interfaceDetails->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
		success = SetupDiGetDeviceInterfaceDetail(infoSet,
			&interfaceData,
			interfaceDetails,
			resourceSize,
			NULL,
			&infoData);

		char devicepath[1000];
		strcpy_s(devicepath, interfaceDetails->DevicePath);
		gDevicePath = gcnew String(devicepath);
		free(interfaceDetails);

		// Get properties about the device,remember to delete the memeory allocated inside the getDeviceProperty
		deviceName = (TCHAR*)getDeviceProperty(infoSet, &infoData, SPDRP_DEVICEDESC);
		hardwareId = (TCHAR*)getDeviceProperty(infoSet, &infoData, SPDRP_HARDWAREID);

		if (deviceName == NULL || hardwareId == NULL)
			throw gcnew Exception("DeviceName and HardwareID = NULL");

		// Parse the hardware ID string, obtaining the vendor and device IDs for use in
		// creating a tag to search for an appropriate subclass.

		unsigned int  vendorId = 0;
		unsigned int  deviceId = 0;
		string idString(hardwareId);
		string descString(deviceName);
		size_t vendorPos = idString.find("VEN_");
		size_t devicePos = idString.find("DEV_");

		if ((vendorPos != string::npos) && (idString.length() >= (vendorPos + 8)) &&
			(devicePos != string::npos) && (idString.length() >= (devicePos + 8))) {
			istringstream vendorStream("0x" + idString.substr((vendorPos + 4), 4));
			vendorStream >> hex >> vendorId;

			istringstream deviceStream("0x" + idString.substr((devicePos + 4), 4));
			deviceStream >> hex >> deviceId;
		}

		// copy the PDAQ4 information to return it for user.
		deviceinfo->PID = deviceId;
		deviceinfo->VID = vendorId;
		deviceinfo->DeviceName = gcnew String(deviceName);
		deviceinfo->HardwareID = gcnew String(hardwareId);
		// Free the character string for the hardware ID and deviceName if not NULL
		free(hardwareId);
		free(deviceName);

		gDeviceFound = true;
		break;
	}
	// if no device found.
	if (gDeviceFound == false)
		throw gcnew NoPDAQ4CardException("No Device Found!");

	return deviceinfo;
}

void DriverAgent::OpenPDAQ4()
{
	// Create a file and get a handle to the device
	if (gDeviceHandle != INVALID_HANDLE_VALUE)
		throw gcnew OpenPDAQ4Exception("The device opened already.");
	if (gDeviceFound == false)
		throw gcnew OpenPDAQ4Exception("Found device First");

	char* str_devicepath = (char*)(void*)Marshal::StringToHGlobalAnsi(gDevicePath);

	gDeviceHandle = CreateFile(str_devicepath, (GENERIC_READ | GENERIC_WRITE), (FILE_SHARE_READ | FILE_SHARE_WRITE),
		NULL,
		OPEN_EXISTING,
		0,
		NULL);
	Marshal::FreeHGlobal((IntPtr)str_devicepath);

	if (gDeviceHandle == INVALID_HANDLE_VALUE)
	{
		DWORD errorcode = GetLastError();
		throw gcnew OpenPDAQ4Exception(String::Format("Create File Failed, GetLastError = {0}", errorcode));
	}

	PDAQ4Init();
	configspace = QueryPDAQ4Resource();
	//PDAQ4DMAInit();
}

void DriverAgent::ClosePDAQ4()
{
	if (gDeviceHandle == INVALID_HANDLE_VALUE)
		throw gcnew Exception("Close before Open");
	if (CloseHandle(gDeviceHandle) == false)
	{
		DWORD errorcode = GetLastError();
		throw gcnew Exception(String::Format("Close File Failed, GetLastError = {0}", errorcode));
	}
	gDeviceHandle = INVALID_HANDLE_VALUE;
	gDeviceFound = false;
}

HardwareResource^ DriverAgent::QueryPDAQ4Resource()
{
	HardwareResource^ cardresource = gcnew HardwareResource();
	DrvQueryResourcePacket queryresoucepackage;
	DWORD return_bytes = 0;
	if (DeviceIoControl(gDeviceHandle, IOCTL_QUERY_RESOURCE, NULL, 0, &queryresoucepackage, sizeof(DrvQueryResourcePacket), &return_bytes, NULL))
	{
		cardresource->PhysicalAddress = queryresoucepackage.resource[0].physAddr.QuadPart;
		cardresource->VirtualPointer = (IntPtr)queryresoucepackage.resource[0].pvUser;
		cardresource->Bytes = queryresoucepackage.resource[0].length;

		OutputDebugPrintf("RegisterFunc -> PhysicalAddress = %x\n", cardresource->PhysicalAddress);
		OutputDebugPrintf("RegisterFunc -> VirtualPointer = %x\n", cardresource->VirtualPointer);
		OutputDebugPrintf("RegisterFunc -> Length = %x\n", cardresource->Bytes);

		

		return cardresource;
	}

	DWORD errorcode = GetLastError();
	throw gcnew Exception(String::Format("QueryPDAQ4Resource Failed, GetLastError = {0}", errorcode));
}

cli::array<FifoInfo^>^ DriverAgent::QueryFIFO()
{
	PDrvQueryFifoPacket pqueryresoucepackage = (PDrvQueryFifoPacket)malloc(sizeof(DrvQueryFiFoPacket) * 3);
	DWORD return_bytes = 0;
	//DWORD fifoSize = sizeof(DrvQueryFiFoPacket) * 3;
	OutputDebugPrintf("DrvSharedResourceSize = %d\n", sizeof(DrvSharedResource));
	OutputDebugPrintf("DrvQueryFiFoPacketSize = %d\n", sizeof(DrvQueryFiFoPacket));
	OutputDebugPrintf("OutBufferSize = %d\n", sizeof(DrvQueryFiFoPacket) * 3);

	if (DeviceIoControl(gDeviceHandle,
		IOCTL_GET_DMABUFFER,
		NULL,
		0,
		pqueryresoucepackage,
		sizeof(DrvQueryFiFoPacket) * 3,
		&return_bytes, NULL))
	{
		PDrvQueryFifoPacket pqueryresoucepackage_0 = pqueryresoucepackage;
		cli::array<FifoInfo^>^ fifoinfo_array = gcnew cli::array<FifoInfo^>(3);
		for (int k = 0; k < 3; k++)
		{
			FifoInfo^ fifoinfo = gcnew FifoInfo();
			fifoinfo->FrameType = (UInt32)pqueryresoucepackage->frame_type;
			ULONG fifosize = pqueryresoucepackage->nFifoElement;
			for (int i = 0; i < fifosize; i++)
			{
				FifoElementPacket packet = pqueryresoucepackage->packets[i];
				FifoElement^ fifoelement = gcnew FifoElement();
				for (int j = 0; j < packet.nSharedResource; j++)
				{
					HardwareResource^ hwres = gcnew HardwareResource();
					hwres->PhysicalAddress = packet.ResourceList[j].physAddr.QuadPart;
					hwres->VirtualPointer = (IntPtr)packet.ResourceList[j].pvUser;
					hwres->Bytes = packet.ResourceList[j].length;
					fifoelement->dma_descriptor_list->Add(hwres);
				}
				fifoinfo->AddFifoElement(fifoelement);
			}
			fifoinfo_array[k] = fifoinfo;
			pqueryresoucepackage++;
		}

		free(pqueryresoucepackage_0);

		return fifoinfo_array;
	}

	free(pqueryresoucepackage);
	DWORD errorcode = GetLastError();
	throw gcnew Exception(String::Format("QueryFIFO Failed, GetLastError = {0}", errorcode));
}

void DriverAgent::PDAQ4Init()
{
	DWORD return_length = 0;
	if (DeviceIoControl(gDeviceHandle, IOCTL_INIT, NULL, 0, NULL, 0, &return_length, NULL))
	{
		Thread::Sleep(200);
		return;
	}

	DWORD errorcode = GetLastError();
	throw gcnew Exception(String::Format("PDAQ4Init Failed, GetLastError = {0}", errorcode));
}
DMAInfo^ DriverAgent::GetDMAInfo()
{
	DMAInfo^ info = gcnew DMAInfo();
	//REG32* configspace_start = (REG32*)(void*)configspace->VirtualPointer;
	/*UInt32 vpf = ReadRegisterValue(8);
	UInt32 rtspv = ReadRegisterValue(9);
	UInt32 rfspv = ReadRegisterValue(10);*/
	UInt32 vpf = 256;
	UInt32 rtspv = 256 / 4;
	UInt32 rfspv = 1024 / 2;
	info->UpdateDMAInfo(vpf, rtspv, rfspv);
	return info;
}

DMAInfo^ DriverAgent::PDAQ4DMAInit()
{
	DWORD return_length = 0;
	DrvInitRequest dma_init_request;
	DMAInfo^ dmainfo = GetDMAInfo();
	dma_init_request.n_rfframesize = dmainfo->rf_frame_size;
	dma_init_request.n_rtframesize = dmainfo->rt_frame_size;
	if (DeviceIoControl(gDeviceHandle, IOCTL_DMA_INIT, &dma_init_request, sizeof(DrvInitRequest), NULL, 0, &return_length, NULL))
	{
		return dmainfo;
	}

	DWORD errorcode = GetLastError();
	throw gcnew Exception(String::Format("PDAQ4DMAInit Failed, GetLastError = {0}", errorcode));
}

void DriverAgent::EnableDMAWriteInt(bool enabled)
{
	m_mutex->WaitOne();
	PREG32 preg = (PREG32)(void*)configspace->VirtualPointer;
	REG32 oldvalue = *(preg + OFFSET_INTERRUPT_ENABLE);
	REG32 newvalue = 0;
	if (enabled)
	{
		newvalue = oldvalue | INT_MASK_DMA_WRITE;
	}
	else
	{
		newvalue = oldvalue & (~INT_MASK_DMA_WRITE);
	}
	*(preg + OFFSET_INTERRUPT_ENABLE) = newvalue;

	m_mutex->ReleaseMutex();
}
void DriverAgent::EnableDMAReadInt(bool enabled)
{
	m_mutex->WaitOne();
	PREG32 preg = (PREG32)(void*)configspace->VirtualPointer;
	REG32 oldvalue = *(preg + OFFSET_INTERRUPT_ENABLE);
	REG32 newvalue = 0;
	if (enabled)
	{
		newvalue = oldvalue | INT_MASK_DMA_READ;
	}
	else
	{
		newvalue = oldvalue & (~INT_MASK_DMA_READ);
	}
	*(preg + OFFSET_INTERRUPT_ENABLE) = newvalue;

	m_mutex->ReleaseMutex();
}
void DriverAgent::EnableHostLatency(bool enabled)
{
	m_mutex->WaitOne();
	PREG32 preg = (PREG32)(void*)configspace->VirtualPointer;
	REG32 oldvalue = *(preg + OFFSET_INTERRUPT_ENABLE);
	REG32 newvalue = 0;
	if (enabled)
	{
		newvalue = oldvalue | INT_MASK_DMA_HOST_LATENCY;
	}
	else
	{
		newvalue = oldvalue & (~INT_MASK_DMA_HOST_LATENCY);
	}
	*(preg + OFFSET_INTERRUPT_ENABLE) = newvalue;

	m_mutex->ReleaseMutex();
}
UInt32 DriverAgent::ReadPDAQ4RegSpaceBytesLength()
{
	return configspace->Bytes;
}
cli::array<UInt32>^ DriverAgent::ReadPDAQ4RegSpace(UInt32 offset, UInt32 number_of_register)
{
	m_mutex->WaitOne();
	cli::array<UInt32>^ reg_array = gcnew cli::array<UInt32>(number_of_register);
	pin_ptr<UInt32> p = &reg_array[0];
	OutputDebugPrintf("offset = %d\n", offset * sizeof(UInt32));
	OutputDebugPrintf("length = %d\n", number_of_register * sizeof(UInt32));
	OutputDebugPrintf("pointer = %x\n", configspace->VirtualPointer + (offset * sizeof(UInt32)));
	OutputDebugPrintf("base = %x\n", configspace->VirtualPointer );
	//UInt32 finalOffset = offset * sizeof(UInt32);
	memcpy(p, (void*)(configspace->VirtualPointer + (offset * sizeof(UInt32))), number_of_register * sizeof(UInt32));
	m_mutex->ReleaseMutex();
	return reg_array;
}



bool DriverAgent::WriteRegisterValue(UInt32 registerType, UInt32 offset, UInt32 value) {

	UInt32 params[] = { registerType ,offset,value };

	unsigned long length = 0;
	DWORD return_bytes = 0;

	if (DeviceIoControl(gDeviceHandle, IOCTL_WRITE_REGISTER_VALUE, &params, 12, NULL, 0, &length, NULL)) {

		return TRUE;

	}
	else
	{
		return FALSE;
	}

}

ArrayList^ DriverAgent::ReadRegisterByIC(UInt32 registerType, UInt32 offset, UInt32 number) {

	UInt32 params[] = { registerType ,offset,number };
	//UInt32 result = 0;
	//UInt32 output[READ_REG_NUM];
	
	

	PRegisterPacket pBuffer = (PRegisterPacket)malloc(number * sizeof(RegisterPacket));
	PRegisterPacket pBuffer2 = pBuffer;
	//IntPtr ptr_register_array = Marshal::AllocHGlobal(sizeof(RegisterPacket) * number);
	//cli::array<PRegisterPacket>^ register_array = gcnew  cli::array<PRegisterPacket>(number);
	//cli::array<UInt32>^ register_array_uint = gcnew  cli::array<UInt32>(READ_REG_NUM);
	ArrayList^ registerResultlist = gcnew ArrayList;
	
	unsigned long length = 0;
	DWORD return_bytes = 0;
	if (DeviceIoControl(gDeviceHandle, IOCTL_READ_REGISTER_VALUE, &params, 12, pBuffer2, number * sizeof(RegisterPacket), &length,NULL)) {

		//registerResultlist = gcnew ArrayList();
		//PRegisterPacket pReg = (PRegisterPacket)ptr_register_array;
		/*Marshal::ptryo
		Marshal::Copy()*/
		for (ULONG i = 0; i < number; i++)
		{
			RegisterMd^ rs = gcnew RegisterMd();
			rs->address = (UInt64)pBuffer2->Address;
			rs->value = pBuffer2->Value;
			registerResultlist->Add(rs);

			pBuffer2++;

		}

		//for each (UInt32 value in output)
		//{
		//	/*RegisterPacket reg = *pReg;

		//	RegisterMd^ rs = gcnew RegisterMd();
		//	rs->address = (UInt64)reg.Address;
		//	rs->value = reg.Value;*/
		//	registerResultlist->Add(value);
		//	//registerResultlist->Add(rs);
		//}

		free(pBuffer);

		return registerResultlist;


	}
	else
	{
		return registerResultlist;
	}

	

}

UInt32 DriverAgent::ReadRegisterValue(UInt32 reg_offset)
{
	m_mutex->WaitOne();
	PREG32 ptr = ((PREG32)(void*)configspace->VirtualPointer) + reg_offset;
	m_mutex->ReleaseMutex();
	return *ptr;
}
void DriverAgent::WritePDAQ4Reg(UInt32 reg_address_byte4_offset, UInt32 value)
{
	m_mutex->WaitOne();
	PREG32 preg = (PREG32)(void*)configspace->VirtualPointer;
	*(preg + reg_address_byte4_offset) = value;
	m_mutex->ReleaseMutex();
}
void DriverAgent::WritePDAQ4RegBlock(UInt32 StartBARAddressOffset, cli::array<UInt32>^ reg_array)
{
	m_mutex->WaitOne();
	PREG32 preg = (PREG32)(void*)configspace->VirtualPointer;
	preg += StartBARAddressOffset / 4;

	for (int i = 0; i < reg_array->Length; i++)
	{
		*(preg + i) = reg_array[i];
	}
	m_mutex->ReleaseMutex();
}

PDAQ4DebugInfo^ DriverAgent::PDAQ4QueryDebugInfo()
{
	DrvDebugInfo querypackage;
	DWORD return_bytes = 0;
	PDAQ4DebugInfo^ debuginfo = gcnew PDAQ4DebugInfo();
	if (DeviceIoControl(gDeviceHandle, IOCTL_DIAG_QUERY_DEBUG_INFO, NULL, 0, &querypackage, sizeof(DrvDebugInfo), &return_bytes, NULL))
	{
		memcpy(&debuginfo, &querypackage, sizeof(DrvDebugInfo));
		return debuginfo;
	}
	DWORD errorcode = GetLastError();
	throw gcnew Exception(String::Format("PDAQ4DMAInit Failed, GetLastError = {0}", errorcode));
}
void DriverAgent::PDAQ4ConnectFIFOChannel(FiFoConnection^ connect)
{
	unsigned long actual_length_long = 0;
	DrvConnectFifoInPacket pRequest;
	pRequest.frame_type = (FRAME_TYPE)connect->GetChannelType();
	pRequest.hEvent = (HANDLE)connect->fifo->GetNativeWaitHandle();
	DrvConnectFifoOutPacket outpacket;
	OutputDebugPrintf("DrvConnectFifoOutPacket = %d\n", sizeof(DrvConnectFifoOutPacket));

	BOOL result = DeviceIoControl(gDeviceHandle, IOCTL_CONNECT_FIFO, &pRequest, sizeof(DrvConnectFifoInPacket), &outpacket, sizeof(DrvConnectFifoOutPacket), &actual_length_long, NULL);
	if (result)
	{
		connect->connect(IntPtr(outpacket.pFifo));
		return;
	}
	DWORD errorcode = GetLastError();
	throw gcnew Exception(String::Format("PDAQ4ConnectFIFOChannel Failed, GetLastError = {0}", errorcode));
}
void DriverAgent::PDAQ4DisConnectFIFOChannel(FiFoConnection^ connect)
{
	unsigned long actual_length_long = 0;
	UInt32 channelindex = (FRAME_TYPE)connect->GetChannelType();
	BOOL result = DeviceIoControl(gDeviceHandle, IOCTL_DISCONNECT_FIFO, &channelindex, sizeof(UINT32), NULL, 0, &actual_length_long, NULL);

	if (result)
	{
		return;
	}
	DWORD errorcode = GetLastError();
	throw gcnew Exception(String::Format("PDAQ4DisConnectFIFOChannel Failed, GetLastError = {0}", errorcode));
}

void DriverAgent::PDAQ4Reset()
{
	m_mutex->WaitOne();
	/*REG32* configspace_start = (REG32*)(void*)configspace->VirtualPointer;
	REG32* rest = configspace_start + 5;
	*rest = 1;*/
	m_mutex->ReleaseMutex();
	//WaitResetOk();
}

void DriverAgent::ProgramFPGA(String^ bit_file)
{
}

void DriverAgent::PrintDbgInfo() {
	m_mutex->WaitOne();
	DWORD return_length = 0;
	BOOL result = DeviceIoControl(gDeviceHandle, IOCTL_GET_DBG_INFO, NULL, 0, NULL, 0, &return_length, NULL);
	m_mutex->ReleaseMutex();

	if (result)
	{
		return;
	}
	DWORD errorcode = GetLastError();
	throw gcnew Exception(String::Format("PDAQ4STARTACQ Failed, GetLastError = {0}", errorcode));
}

void DriverAgent::MotorStart()
{
	m_mutex->WaitOne();
	/*
	PREG32 dma = (PREG32)((PUCHAR)configspace->VirtualPointer.ToPointer() + 0xD4);
	PREG32  dma_reg = (PREG32)dma;
	UINT start_dma = 0x400;
	*dma_reg = start_dma;
	PREG32  mot = (PREG32)((PUCHAR)configspace->VirtualPointer.ToPointer() + 0x4);
	*mot = 1;
	*/

	DWORD return_length = 0;
	BOOL result = DeviceIoControl(gDeviceHandle, IOCTL_START_ACQ, NULL, 0, NULL, 0, &return_length, NULL);
	m_mutex->ReleaseMutex();
	if (result)
	{
		return;
	}
	DWORD errorcode = GetLastError();
	throw gcnew Exception(String::Format("PDAQ4STARTACQ Failed, GetLastError = {0}", errorcode));
}

void DriverAgent::StartRawRFDMA()
{
	m_mutex->WaitOne();
	REG32* configspace_start = (REG32*)(void*)configspace->VirtualPointer;
	REG32* read_dma_ctrl = configspace_start + OFFSET_READ_DMA_START;
	*read_dma_ctrl = 0x400;
	m_mutex->ReleaseMutex();
}
void DriverAgent::StopRawRFDMA()
{
	m_mutex->WaitOne();
	REG32* configspace_start = (REG32*)(void*)configspace->VirtualPointer;
	REG32* read_dma_ctrl = configspace_start + OFFSET_READ_DMA_START;
	*read_dma_ctrl = 0x000;
	m_mutex->ReleaseMutex();
}

void DriverAgent::MotorStop()
{
	/*
	PREG32  mot = (PREG32)((PUCHAR)configspace->VirtualPointer.ToPointer() + 0x4);
	PREG32  mot2 = (PREG32)mot;
	*mot2 = 0;
	*/
	m_mutex->WaitOne();
	DWORD return_length = 0;
	BOOL result = DeviceIoControl(gDeviceHandle, IOCTL_STOP_ACQ, NULL, 0, NULL, 0, &return_length, NULL);

	// Close the driver and reopen it, there will no some invalid acess during switch from different view mode.
	// This is a walkaround
	ClosePDAQ4();
	FindPDAQ4();
	OpenPDAQ4();

	m_mutex->ReleaseMutex();
	if (result)
	{
		return;
	}
	DWORD errorcode = GetLastError();
	throw gcnew Exception(String::Format("PDAQ4STOPACQ Failed, GetLastError = {0}", errorcode));
}

void DriverAgent::StartDMA()
{
	m_mutex->WaitOne();
	REG32* configspace_start = (REG32*)(void*)configspace->VirtualPointer;
	REG32* read_dma_ctrl = configspace_start + OFFSET_WRITE_DMA_START;
	*read_dma_ctrl = 0x400;
	m_mutex->ReleaseMutex();
}
void DriverAgent::StopDMA()
{
	m_mutex->WaitOne();
	//REG32* configspace_start = (REG32*)(void*)configspace->VirtualPointer;
	//REG32* read_dma_ctrl = configspace_start + OFFSET_WRITE_DMA_START;
	//*read_dma_ctrl = 0x000;
	m_mutex->ReleaseMutex();
}

void DriverAgent::WaitResetOk()
{
	REG32* configspace_start = (REG32*)(void*)configspace->VirtualPointer;
	REG32* rest = configspace_start + 5;
	System::Diagnostics::Trace::WriteLine(String::Format("Rest value = {0:X}", *rest));
	// wait about 3 seconds for the PDAQ4 to reset
	int max_retries = 300;
	while (((*rest) & 0xFFFFFFFF) != 0xE0000000)
	{
		System::Diagnostics::Trace::WriteLine(String::Format("Rest value = {0:X}", *rest));
		Thread::Sleep(10);
		max_retries--;
		if (max_retries == 0)
		{
			throw gcnew PDAQ4SoftResetException(String::Format("Softreset fails, the reset register value is {0:X}", *rest));
		}
	}
}