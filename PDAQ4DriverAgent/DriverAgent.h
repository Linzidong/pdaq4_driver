#pragma once

using namespace System::Configuration;
using namespace System::Collections;
using namespace System;
using namespace PDAQ4DriverInterface;
#include "Fifo.h"
#include "../PDAQ4KMDFDriver/PDAQ4DriverPublic.h"
namespace PDAQ4DriverAgent
{
	namespace PDAQ4Implementation
	{
		public ref class NoPDAQ4CardException :public Exception
		{
		public:
			NoPDAQ4CardException(String^ exceptstr) :Exception(exceptstr) {}
		};
		public ref class OpenPDAQ4Exception :public Exception
		{
		public:
			OpenPDAQ4Exception(String^ exceptstr) :Exception(exceptstr) {}
		};
		public ref class PDAQ4SoftResetException :public Exception
		{
		public:
			PDAQ4SoftResetException(String^ exceptstr) :Exception(exceptstr) {}
		};
		public ref class HardwareResource
		{
		public:
			HardwareResource()
			{
				this->PhysicalAddress = 0;
				this->VirtualPointer = IntPtr(nullptr);
				this->Bytes = 0;
			}

			//------------------------------------------------//
			property Int64 PhysicalAddress
			{
				Int64 get() { return this->_physAddr; }
				void set(Int64 value) { this->_physAddr = value; }
			}
			property IntPtr VirtualPointer
			{
				IntPtr get() { return this->_userPtr;; }
				void set(IntPtr value) { this->_userPtr = value; }
			}
			property UInt32 Bytes
			{
				UInt32 get() { return this->_length; }
				void set(UInt32 value) { this->_length = value; }
			}

		private:
			Int64     _physAddr;
			IntPtr    _userPtr;   // Application poniter
			UInt32    _length;
		};

		public ref class PDAQ4DebugInfo
		{
		public:
			UInt32	interrupt_counter_1;
			UInt32	interrupt_counter_2;
			UInt32	FIFO_Index;
			UInt32	DMA_Word_Counter;
			UInt32	DMA_RTFrames_InBuffer;
			UInt32	DMA_RFFrames_InBuffer;
		};
		public ref class DMAInfo
		{
		private:
			UInt32 _vector_per_frame;
			UInt32 _rt_sample_per_vector;
			UInt32 _rf_sample_per_vector;
		public:
			void UpdateDMAInfo(UInt32 vpf_reg, UInt32 rtspv_reg, UInt32 rfspv_reg)
			{
				if (vpf_reg != 256 && vpf_reg != 512)
				{
					throw gcnew Exception(String::Format("The value of Vector Per Frame should be 256 or 512, but the actual value is {0} ", vpf_reg));
				}
				_vector_per_frame = vpf_reg;
				if (rtspv_reg != 256 / 4 && rtspv_reg != 512 / 4)
				{
					throw gcnew Exception(String::Format("The value of RT Sample Per Vector should be 256 / 4 or 512 / 4, but the actual value is {0} ", rtspv_reg));
				}
				_rt_sample_per_vector = rtspv_reg * 4;

				if (rfspv_reg != 1024 / 2 && rfspv_reg != 2048 / 2)
				{
					throw gcnew Exception(String::Format("The value of RF Sample Per Vector should be 1024/2  or 2048/2, but the actual value is {0} ", rfspv_reg));
				}
				_rf_sample_per_vector = rfspv_reg * 2;
			}
			property UInt32 vector_per_frame
			{
				UInt32 get()
				{
					return _vector_per_frame;
				}
			}

			property UInt32 rt_sample_per_vector
			{
				UInt32 get()
				{
					return _rt_sample_per_vector;
				}
			}
			property UInt32 rf_sample_per_vector
			{
				UInt32 get()
				{
					return _rf_sample_per_vector;
				}
			}

			property UInt32 rf_frame_size
			{
				UInt32 get() {
					UInt32 framesize = FRAME_SIZE(vector_per_frame, rf_sample_per_vector, 2);
					return framesize;
				}
			}
			property UInt32 raw_rf_frame_size
			{
				UInt32 get() {
					UInt32 framesize = RAW_FRAME_SIZE(vector_per_frame, rf_sample_per_vector, 2);
					return framesize;
				}
			}
			property UInt32 rt_frame_size
			{
				UInt32 get() {
					//UInt32 framesize = FRAME_SIZE(vector_per_frame, rt_sample_per_vector, RT_SAMPLE_BYTES);
					UInt32 framesize = FRAME_SIZE(vector_per_frame, rt_sample_per_vector, 1);
					return framesize;
				}
			}
		};

		public ref class PDAQDevice :IPDAQDevice
		{
		private:
			String^ _DeviceName;
			String^ _HardwareID;
			UInt32 _PID;
			UInt32 _VID;

		public:
			virtual property String^ DeviceName
			{
				String^ get() { return _DeviceName; }
				void set(String^ DeviceName) { _DeviceName = DeviceName; }
			}
			virtual property String^ HardwareID
			{
				String^ get() { return _HardwareID; }
				void set(String^ hardwareid) { _HardwareID = hardwareid; }
			}
			virtual property UInt32 PID
			{
				UInt32 get() { return _PID; }
				void set(UInt32 pid) { _PID = pid; }
			}
			virtual property UInt32 VID
			{
				UInt32 get() { return _VID; }
				void set(UInt32 vid) { _VID = vid; }
			}
		};

		public  ref class FiFoConnection abstract
		{
		private:
			Thread^ m_thread;
			FifoInfo^ m_fifo_info;
		protected:
			ChannelHeader^ channelHeader;
		public:
			event DeviceGenericEvent<Frame^>^ FrameReadyEvent;
			virtual void run() = 0;
			static void ThreadProc(Object^ p)
			{
				FiFoConnection^ connection = (FiFoConnection^)p;
				connection->run();
			}

		public:
			CFifo^ fifo;
			FiFoConnection(ChannelHeader^ channelhdr, FifoInfo^ fifo_info)
			{
				channelHeader = channelhdr;
				m_fifo_info = fifo_info;
				fifo = gcnew CFifo();
				fifo->Init();
				m_thread = nullptr;
			}
			ChannelType GetChannelType()
			{
				return channelHeader->ChannelType;
			}
			void connect(IntPtr sharedfifo)
			{
				this->fifo->Connect(sharedfifo);
				this->fifo->SetFifoInfo(m_fifo_info);
			}
			void start()
			{
				if (m_thread == nullptr)
				{
					m_thread = gcnew Thread(gcnew ParameterizedThreadStart(FiFoConnection::ThreadProc));
					//m_thread->Name = String::Format("Thread {0}, ID = {1} ",this->ToString(),m_thread->ManagedThreadId);
					System::Diagnostics::Trace::WriteLine(String::Format("Thread Started {0}", m_thread->Name));
				}
				m_thread->Start(this);
			}
			void stop()
			{
				m_thread->Abort();
				while (m_thread->IsAlive)
				{
					Thread::Sleep(10);
				}
			}
			void fireFrameEvent(Frame^ frame)
			{
				
				FrameReadyEvent(frame);
			}
		};

		public ref class Frame_Connection :FiFoConnection
		{
		public:
			Frame_Connection(ChannelHeader^ chhdr, FifoInfo^ fifo_info) :FiFoConnection(chhdr, fifo_info)
			{
			}
		public:
			void  run() override
			{
				AutoResetEvent^ ev = fifo->ReadyEvent;
				// The size of frame include the padding 128 boundary alignment
				Frame^ frame = gcnew Frame(channelHeader);
				long counter = 0;
				long debug = 0;
				try
				{
					while (1)
					{
						bool bReady = ev->WaitOne(200);
						debug = counter;
						if (bReady)
						{
							long index = -1;
							while ((index = fifo->Read()) != -1) //m_pElement))
							{
								fifo->IncReadIndex();
								pin_ptr<Byte> p = &(frame->FrameBlock[0]);
								fifo->Pack(IntPtr(p), index);
								p = nullptr;
								fireFrameEvent(frame);
								//System::Diagnostics::Trace::WriteLine(String::Format("Frame Type = {0}", channelHeader->ChannelType));
							} // while this is data inf fifo
							counter++;
						}
					}
				} // try
				catch (ThreadAbortException^)
				{
					System::Diagnostics::Trace::WriteLine("Frame Receving thread exit");
				}
				catch (Exception^ err)
				{
					System::Diagnostics::Trace::WriteLine(String::Format("{0}", err));
				}
			}
		};

		public ref class Diag_Frame_Connection :FiFoConnection
		{
		public:
			Diag_Frame_Connection(ChannelHeader^ chhdr, FifoInfo^ fifo_info) :FiFoConnection(chhdr, fifo_info)
			{
			}
		public:
			void  run() override
			{
				AutoResetEvent^ ev = fifo->ReadyEvent;
				// The size of frame include the padding 128 boundary alignment
				Frame^ frame = gcnew Frame(channelHeader);
				long counter = 0;
				long debug = 0;
				try
				{
					while (1)
					{
						bool bReady = ev->WaitOne(200);
						debug = counter;
						if (bReady)
						{
							long index = -1;

							while ((index = fifo->Write()) != -1) //m_pElement))
							{
								// Get the frame to write
								fireFrameEvent(frame);
								pin_ptr<Byte> p = &(frame->FrameBlock[0]);
								fifo->UnPack(IntPtr(p), frame->FrameBlock->Length, index);
								fifo->IncWriteIndex();
								//System::Diagnostics::Trace::WriteLine(String::Format("Frame Type = {0}", channelHeader->ChannelType));
								p = nullptr;
							} // while this is data inf fifo

							//System::Diagnostics::Trace::WriteLine("Read Frame Received....");
							counter++;
						}
					}
				} // try
				catch (ThreadAbortException^)
				{
					System::Diagnostics::Trace::WriteLine("Frame Receving thread exit");
				}
				catch (Exception^ err)
				{
					System::Diagnostics::Trace::WriteLine(String::Format("{0}", err));
				}
			}
		};

		public ref class RegisterMd {



		public :
			
			UInt64 address;
			UInt32 value;
			String^ name;
			

		};

		public ref class DriverAgent
		{
		private:
			String^ gDevicePath;
			bool gDeviceFound;
			HANDLE gDeviceHandle;
			HardwareResource^ configspace;
			static initonly Mutex^ m_mutex = gcnew Mutex;
		public:

			DriverAgent();

			IPDAQDevice^ FindPDAQ4();
			void OpenPDAQ4();
			void ClosePDAQ4();
			HardwareResource^ QueryPDAQ4Resource();
			
			cli::array<FifoInfo^>^ QueryFIFO();
			void PDAQ4Init();
			void PDAQ4Reset();
			DMAInfo^ PDAQ4DMAInit();
			void EnableDMAWriteInt(bool enabled);
			void EnableDMAReadInt(bool enabled);
			void EnableHostLatency(bool enabled);
			void PrintDbgInfo();
			UInt32 ReadPDAQ4RegSpaceBytesLength();
			cli::array<UInt32>^ ReadPDAQ4RegSpace(UInt32 offset, UInt32 number_of_register);
			ArrayList^ ReadRegisterByIC(UInt32 registerType, UInt32 offset, UInt32 number);

			bool WriteRegisterValue(UInt32 registerType, UInt32 offset, UInt32 value);
			UInt32 ReadRegisterValue(UInt32 reg_offset);
			//PDAQ4_STATUS ReadPDAQ4RegPtr(unsigned int  *regptr, unsigned int *actual_regspace_length_in_bytes);
			void WritePDAQ4Reg(UInt32 reg_address_byte4_offset, UInt32 value);
			void WritePDAQ4RegBlock(UInt32 StartBARAddressOffset, cli::array<UInt32>^ reg_array);
			PDAQ4DebugInfo^ PDAQ4QueryDebugInfo();

			void PDAQ4ConnectFIFOChannel(FiFoConnection^);
			void PDAQ4DisConnectFIFOChannel(FiFoConnection^);

			void ProgramFPGA(String^ bit_file);
			void StartDMA();
			void StopDMA();
			void MotorStart();
			void MotorStop();
			void StartRawRFDMA();
			void StopRawRFDMA();
			DMAInfo^ GetDMAInfo();
		private:
			void WaitResetOk();
		};
	}
}