#pragma once
#ifndef __PDAQ4IOCTRL
#define __PDAQ4IOCTRL
#include "plx.hpp"
#include "../../PDAQ4KMDFDriver/RegPDAQ4.h"
#include "../../PDAQ4KMDFDriver/PDAQ4DriverPublic.h"


#define PDAQ4_STATUS_SUCCESS   0
#define PDAQ4_STATUS_NO_PDAQ4_PRESENT 1
#define PDAQ4_STATUS_CANNOT_OPEN_AGAIN 2
#define PDAQ4_STATUS_OPEN_BEFORE_FOUND 3
#define PDAQ4_STATUS_READ_FRAME_TIMEOUT 5
#define PDAQ4_STATUS_CANNOT_OPEN_DEVICE 6
#define PDAQ4_STATUS_CLOSE_BEOFRE_OPEN 7
#define PDAQ4_STATUS_CANNOT_CLOSE 8
#define PDAQ4_STATUS_DEVICE_NOT_OPEN_BEFORE_USE 9
#define PDAQ4_DEVICE_IO_CTRL_ERROR 10
#define PDAQ4_EXCEPTION_IN_FIND_PDAQ4 11
#define PDAQ4_STATUS_READ_ERROR 12
#define PDAQ4_STATUS_QUERY_RESOURCE_FAILED 13
#define PDAQ4_STATUS_UNKNOW 99
#define VECTOR_PER_FRAME		256
#define RT_SAMPLE_PER_VECTOR	256
#define RF_SAMPLE_PER_VECTOR    1024
typedef unsigned int  PDAQ5_STATUS;

typedef struct __PDAQ4CARDResource
{
    LARGE_INTEGER physical_regbase;
    PUCHAR virtual_regbase;
    unsigned int reglength;
}PDAQ4CARDResource;
class PDAQ4IOCTRL
{
private:
    HANDLE pDaq_device;
    HDEVINFO pDaq_devInfo;
    PSP_DEVICE_INTERFACE_DETAIL_DATA pDaq_DeviceInterfaceDetail;
public:
    PDAQ4IOCTRL(HANDLE pdevice, PSP_DEVICE_INTERFACE_DETAIL_DATA deviceInfo);
    ~PDAQ4IOCTRL();
    PDAQ5_STATUS PDAQ4InitIOEvt();
    void setPDAQHandle(HANDLE pdevice);
    void setPDAQDeviceInterfaceDetail(PSP_DEVICE_INTERFACE_DETAIL_DATA deviceInfo);
    PDAQ5_STATUS QueryPDAQ4Resource(PDAQ4CARDResource* cardResource);
};
#endif
