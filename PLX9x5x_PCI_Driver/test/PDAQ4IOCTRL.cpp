#include "PDAQ4IOCTRL.h"
using namespace std;

PDAQ4IOCTRL::PDAQ4IOCTRL(HANDLE pdevice, PSP_DEVICE_INTERFACE_DETAIL_DATA deviceInfo)
{
	pDaq_device = pdevice;
	pDaq_DeviceInterfaceDetail = deviceInfo;
}

PDAQ4IOCTRL::~PDAQ4IOCTRL()
{
}

PDAQ5_STATUS PDAQ4IOCTRL::PDAQ4InitIOEvt()
{
	DWORD return_length = 0;
	if (DeviceIoControl(pDaq_device, IOCTL_INIT, NULL, 0, NULL, 0, &return_length, NULL))
	{

		return PDAQ4_STATUS_SUCCESS;
	}
	return PDAQ4_DEVICE_IO_CTRL_ERROR;
}

PDAQ5_STATUS PDAQ4IOCTRL::QueryPDAQ4Resource(PDAQ4CARDResource* cardResource)
{
	UNREFERENCED_PARAMETER(cardResource);
	DrvQueryResourcePacket queryresoucepackage;
	DWORD return_bytes = 0;
	if (DeviceIoControl(pDaq_device, IOCTL_QUERY_RESOURCE, NULL, 0, &queryresoucepackage, sizeof(DrvQueryResourcePacket), &return_bytes, NULL))
	{
		cardResource->physical_regbase = queryresoucepackage.resource[0].physAddr;
		cardResource->virtual_regbase = queryresoucepackage.resource[0].pvUser;
		cardResource->reglength = queryresoucepackage.resource[0].length;
		//gResourcePacket = queryresoucepackage;
		printf("/----------------- Resource Info ----------------/ \n");
		printf("physical register base address	:	0x%.8X%.8X\n" , cardResource->physical_regbase.HighPart, cardResource->physical_regbase.LowPart);
		printf("virtual register base address	:	%s \n", cardResource->virtual_regbase);
		printf("register length					:	%u \n", cardResource->reglength);
		return PDAQ4_STATUS_SUCCESS;
	}
	return PDAQ4_DEVICE_IO_CTRL_ERROR;
}

void PDAQ4IOCTRL::setPDAQHandle(HANDLE pdevice)
{
	pDaq_device = pdevice;
}

void PDAQ4IOCTRL::setPDAQDeviceInterfaceDetail(PSP_DEVICE_INTERFACE_DETAIL_DATA deviceInfo)
{
	pDaq_DeviceInterfaceDetail = deviceInfo;
}
