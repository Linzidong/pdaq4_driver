﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDAQ4DriverInterface
{
       public interface IPDAQDevice
       {
           String DeviceName { get; set; }
           String HardwareID { get; set; }
           UInt32 PID { get; set; }
           UInt32 VID { get; set; }
       }
       public delegate void DeviceGenericEvent<T>(T arg);
     

}
