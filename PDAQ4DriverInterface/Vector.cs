// Vector.cs

namespace PDAQ4DriverInterface
{
   using System;

   public class Vector
   {
      // ----- member data and constants ----- //
      private readonly ArraySegment<byte> _data;

      internal const int OffsetVectorIndex = 0;
      internal const int OffsetAngle = 2;
      internal const int OffsetProcessingStatus = 4;
      internal const int OffsetTransmitTime = 8;
      internal const int OffsetSamples = 16;

      /// <summary>
      /// The length of the vector header memory image.
      /// </summary>
      internal const int VectorHeaderLength = OffsetSamples;

      #region constructors

      /// <summary>
      /// Initializes a new instance of the Vector class.
      /// </summary>
      /// <param name="data">An ArraySegmentxmlxml object.</param>
      internal Vector(ArraySegment<byte> data)
      {
         _data = data;
      }
      #endregion

       /// <summary>
       /// Gets an ArraySegment that represents the data portion of the vector.
       /// </summary>
       public ArraySegment<byte> Data
       {
           get
           {
               return new ArraySegment<byte>(_data.Array, _data.Offset + VectorHeaderLength, _data.Count - VectorHeaderLength);
           }
       }


      /// <summary>
      /// Gets or sets the Angle field.
      /// </summary>
      public ushort Angle
      {
         get
         {
            return BitConverter.ToUInt16(_data.Array, _data.Offset + OffsetAngle);
         }
         set
         {
             unsafe
             {
                 fixed (byte* b = &_data.Array[_data.Offset + OffsetAngle])
                 {
                     *((ushort*)b) = value;
                 }
             }
         }
      }

      /// <summary>
      /// Gets or sets the VectorIndex field.
      /// </summary>
      public ushort VectorIndex
      {
         get
         {
            return BitConverter.ToUInt16(_data.Array, _data.Offset + OffsetVectorIndex);
         }
         set
         {
             unsafe
             {
                 fixed (byte* b = &_data.Array[_data.Offset + OffsetVectorIndex])
                 {
                     *((ushort*)b) = value;
                 }
             }
         }
      }

      /// <summary>
      /// Gets or sets the TransmitTime field.
      /// </summary>
      public ulong TransmitTime
      {
         get
         {
            return BitConverter.ToUInt64(_data.Array, _data.Offset + OffsetTransmitTime);
         }
         set
         {
             unsafe
             {
                 fixed (byte* b = &_data.Array[_data.Offset + OffsetTransmitTime])
                 {
                     *((ulong*)b) = value;
                 }
             }
         }
      }

      /// <summary>
      /// Gets or sets the ProcessingStatus field.
      /// </summary>
      public uint ProcessingStatus
      {
         get
         {
            return BitConverter.ToUInt32(_data.Array, _data.Offset + OffsetProcessingStatus);
         }
         set
         {
             unsafe
             {
                 fixed (byte* b = &_data.Array[_data.Offset + OffsetProcessingStatus])
                 {
                     *((uint*)b) = value;
                 }
             }
         }
      }

      /// <summary>
      /// Indexer to access the index'th byte in the Vector object's sample space.
      /// </summary>
      /// <param name="index">Index into the Vector's sample (byte) array.  
      /// This index is NOT an index to a specific sample.</param>
      /// <returns>Single byte at the index provided.</returns>
      public byte this[int index]
      {
         get
         {
            return _data.Array[_data.Offset + VectorHeaderLength + index];
         }
         set
         {
            _data.Array[_data.Offset + VectorHeaderLength + index] = value;
         }
      }

	// Set vector array directly from a byte array
	public void SetVector(byte [] data)
	{
		Buffer.BlockCopy(data, 0, _data.Array, _data.Offset, data.Length);
	}

      /// <summary>
      /// GetSample() gets an indexed byte in the sample space.
      /// </summary>
      /// <param name="index">Index into the Vector's sample (byte) array.  
      /// This index is NOT an index to a specific sample.</param>
      /// <returns>the indexed byte.</returns>
      public byte GetSample(int index)
      {
         return this[index];
      }

      /// <summary>
      /// SetSample() - Sets the value of a byte in the sample space at a specific index.
      /// </summary>
      /// <param name="index">Index into the Vector's sample (byte) array.  
      /// This index is NOT an index to a specific sample.</param>
      /// <param name="sampleValue">The byte value to store.</param>
      public void SetSample(int index, byte sampleValue)
      {
         this[index] = sampleValue;
      }

      /// <summary>
      /// Prints the contents of a Vector object.
      /// </summary>
      public void PrintContents()
      {
         Console.WriteLine("\t\tVectorIndex: \t\t{0}", this.VectorIndex.ToString("x"));
         Console.WriteLine("\t\tAngle: \t\t\t{0}", this.Angle.ToString("x"));
         Console.WriteLine("\t\tProcessingStatus: \t{0}", this.ProcessingStatus.ToString("x"));
         Console.WriteLine("\t\tTransmitTime: \t\t{0}", this.TransmitTime.ToString("x"));
         // Console.WriteLine("\t\tSample Data (only the first 16 samples):");
         Console.Write("\t\t\t");

		 //for (int i = 0; i < 16; i++)
		 //{
            // if (i < 15)
            //   Console.Write("{0}, ", Samples[i].ToString("x"));
            // else
            //   Console.Write("{0}...\n", Samples[i].ToString("x"));
		 //}
      }
    }
}
