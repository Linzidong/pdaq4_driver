using System.IO;

namespace PDAQ4DriverInterface
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// ChannelHeader class
    /// </summary>
    [Serializable]
    public class ChannelHeader : IChannelHeader, ISerializable, ICloneable
    {
        #region data_members

        private byte channelIndex;
        private ChannelType channelType;
        private ushort frameHdrSize;
        private byte frameRate;
        private ulong channelLatency;
        private ushort vectorHdrSize;
        private ushort vectorsPerFrame;
        private ushort samplesPerVector;
        private byte bitsPerSample;
        private ushort sampleMask;
        private ChannelTrigger triggerType;
        private uint currentHwFrameNo;
        private ulong currentHwFrameTime;
        private ushort noFramesAcq;
        private ulong channelSync;
        private ushort firmwareVersion;
        private ushort softwareVersion;

        #endregion

        /// <summary>
        /// Initializes a new instance of the ChannelHeader class.
        /// </summary>
        public ChannelHeader()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ChannelHeader class.
        /// </summary>
        /// <param name="info">SerializationInfo object.</param>
        /// <param name="ctxt">StreamingContext object.</param>
        public ChannelHeader(SerializationInfo info, StreamingContext ctxt)
        {
            //this.channelIndex = info.GetByte("ChannelIndex");	// Don't set channel index directly the controller needs a specific ID which is returned when a channel is created
            this.channelType = (ChannelType)info.GetByte("ChannelType");
            this.frameHdrSize = info.GetUInt16("FrameHdrSize");
            this.frameRate = info.GetByte("FrameRate");
            this.channelLatency = info.GetUInt64("ChannelLatency");
            this.vectorHdrSize = info.GetUInt16("VectorHdrSize");
            this.vectorsPerFrame = info.GetUInt16("VectorsPerFrame");
            this.samplesPerVector = info.GetUInt16("SamplesPerVector");
            this.bitsPerSample = info.GetByte("BitsPerSample");
            this.sampleMask = info.GetUInt16("SampleMask");
            this.triggerType = (ChannelTrigger)info.GetByte("TriggerType");
            this.currentHwFrameNo = info.GetUInt32("CurrentHWFrameNo");
            this.currentHwFrameTime = info.GetUInt64("CurrentHWFrameTime");
            this.noFramesAcq = info.GetUInt16("NoFramesAcq");
            this.channelSync = info.GetUInt64("ChannelSync");
            this.firmwareVersion = info.GetUInt16("FwVersion");
            this.softwareVersion = info.GetUInt16("SwVersion");
        }

        /// <summary>
        /// Gets or sets the ChannelIndex field.
        /// </summary>
        public byte ChannelIndex
        {
            get
            {
                return channelIndex;
            }

            set
            {
                channelIndex = value;
            }
        }

        /// <summary>
        /// Gets or sets the the CHType field.
        /// </summary>
        public ChannelType ChannelType
        {
            get
            {
                return channelType;
            }

            set
            {
                channelType = value;
            }
        }

        /// <summary>
        /// Gets or sets the the FrameHdrSize field.
        /// </summary>
        public ushort FrameHdrSize
        {
            get
            {
                return frameHdrSize;
            }

            set
            {
                frameHdrSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the the FrameRate field.
        /// </summary>
        public byte FrameRate
        {
            get
            {
                return frameRate;
            }

            set
            {
                frameRate = value;
            }
        }

        /// <summary>
        /// Gets or sets the the ChannelLatency field.
        /// </summary>
        public ulong ChannelLatency
        {
            get
            {
                return channelLatency;
            }

            set
            {
                channelLatency = value;
            }
        }

        /// <summary>
        /// Gets or sets the the VectorHdrSize field.
        /// </summary>
        public ushort VectorHdrSize
        {
            get
            {
                return vectorHdrSize;
            }

            set
            {
                vectorHdrSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the the VectorsPerFrame field.
        /// </summary>
        public ushort VectorsPerFrame
        {
            get
            {
                return vectorsPerFrame;
            }

            set
            {
                vectorsPerFrame = value;
            }
        }

        /// <summary>
        /// Gets or sets the the SamplesPerVector field.
        /// </summary>
        public ushort SamplesPerVector
        {
            get
            {
                return samplesPerVector;
            }

            set
            {
                samplesPerVector = value;
            }
        }

        /// <summary>
        /// Gets or sets the the BitsPerSample field.
        /// </summary>
        public byte BitsPerSample
        {
            get
            {
                return bitsPerSample;
            }

            set
            {
                bitsPerSample = value;
            }
        }

        /// <summary>
        /// Gets or sets the Sample Mask field.
        /// </summary>
        public ushort SampleMask
        {
            get
            {
                return sampleMask;
            }

            set
            {
                sampleMask = value;
            }
        }

        /// <summary>
        /// Gets or sets the Trigger Type field.
        /// </summary>
        public ChannelTrigger TriggerType
        {
            get
            {
                return triggerType;
            }

            set
            {
                triggerType = value;
            }
        }

        /// <summary>
        /// Gets or sets the the CurrentHWFrameNo field.
        /// </summary>
        public uint CurrentHwFrameNo
        {
            get
            {
                return this.currentHwFrameNo;
            }

            set
            {
                this.currentHwFrameNo = value;
            }
        }

        /// <summary>
        /// Gets or sets the the CurrentHWFrameTime field.
        /// </summary>
        public ulong CurrentHwFrameTime
        {
            get
            {
                return this.currentHwFrameTime;
            }

            set
            {
                this.currentHwFrameTime = value;
            }
        }

        /// <summary>
        /// Gets or sets the the NoFramesAcq  field (no of frames acquired).
        /// </summary>
        public ushort NoFramesAcq
        {
            get
            {
                return noFramesAcq;
            }

            set
            {
                noFramesAcq = value;
            }
        }

        /// <summary>
        /// Gets or sets the the ChannelSync field.
        /// </summary>
        public ulong ChannelSync
        {
            get
            {
                return channelSync;
            }

            set
            {
                channelSync = value;
            }
        }

        /// <summary>
        /// Gets or sets the FwVersion field.
        /// </summary>
        public ushort FwVersion
        {
            get
            {
                return firmwareVersion;
            }

            set
            {
                firmwareVersion = value;
            }
        }

        /// <summary>
        /// Gets or sets the SwVersion field.
        /// </summary>
        public ushort SwVersion
        {
            get
            {
                return softwareVersion;
            }

            set
            {
                softwareVersion = value;
            }
        }

        /// <summary>
        /// Gets ChannelHeaderLength.
        /// </summary>
        public int ChannelHeaderLength
        {
            get
            {
                return 41;
            }
        }

        /// <summary>
        /// Clones a ChannelHeader object.
        /// </summary>
        /// <returns>A copied ChannelHeader object.</returns>
        public object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// CalculateBytesPerSample() calculates the number of bytes needed to allocate a single sample.
        /// The calculation is based on the BitsPerSample field.
        /// </summary>
        /// <returns>The number of bytes in a sample.</returns>
        public int CalculateBytesPerSample()
        {
            return (BitsPerSample / 8) + ((BitsPerSample % 8) == 0 ? 0 : 1);
        }

        /// <summary>
        /// CalculateVectorSize() calculates the number of bytes needed to allocate a singel vector (including header)
        /// The calculation is based on Vector.VectorHeaderLength, SamplesPerVector and the CalculateBytesPerSample().
        /// </summary>
        /// <returns>The size of the vector in bytes.</returns>
        public int CalculateVectorSize()
        {
            return vectorHdrSize + (SamplesPerVector * CalculateBytesPerSample());
        }

        /// <summary>
        /// CalculateFrameSize() calculates the number of bytes needed to allocate a frame (including the frame header).
        /// The calculation is based on FrameHdrSize, VectorsPerFrame and the CalculateVectorSize() method.
        /// </summary>
        /// <returns>The size of the frame in bytes.</returns>
        public int CalculateFrameSize()
        {
            return FrameHdrSize + (VectorsPerFrame * CalculateVectorSize());
        }

        /// <summary>
        /// Deserializes a ChannelHeader object.
        /// </summary>
        public void Serialize(Stream fileStream)
        {
            byte[] channelHeaderData = new byte[41];
            int offSet = 0;
            Buffer.BlockCopy(BitConverter.GetBytes(channelIndex), 0, channelHeaderData, offSet, sizeof(byte));
            offSet += sizeof(byte);
            Buffer.BlockCopy(BitConverter.GetBytes((byte)channelType), 0, channelHeaderData, offSet, sizeof(byte));
            offSet += sizeof(byte);
            Buffer.BlockCopy(BitConverter.GetBytes(vectorsPerFrame), 0, channelHeaderData, offSet, sizeof(ushort));
            offSet += sizeof(ushort);
            Buffer.BlockCopy(BitConverter.GetBytes(samplesPerVector), 0, channelHeaderData, offSet, sizeof(ushort));
            offSet += sizeof(ushort);
            Buffer.BlockCopy(BitConverter.GetBytes(bitsPerSample), 0, channelHeaderData, offSet, sizeof(ushort));
            offSet += sizeof(ushort);
            Buffer.BlockCopy(BitConverter.GetBytes(this.currentHwFrameTime), 0, channelHeaderData, offSet, sizeof(ulong));
            offSet += sizeof(ulong);
            Buffer.BlockCopy(BitConverter.GetBytes(this.currentHwFrameNo), 0, channelHeaderData, offSet, sizeof(ushort));
            offSet += sizeof(ushort);
            Buffer.BlockCopy(BitConverter.GetBytes(noFramesAcq), 0, channelHeaderData, offSet, sizeof(ushort));
            offSet += sizeof(ushort);
            Buffer.BlockCopy(BitConverter.GetBytes(frameHdrSize), 0, channelHeaderData, offSet, sizeof(ushort));
            offSet += sizeof(ushort);
            Buffer.BlockCopy(BitConverter.GetBytes(frameRate), 0, channelHeaderData, offSet, sizeof(byte));
            offSet += sizeof(byte);
            Buffer.BlockCopy(BitConverter.GetBytes(vectorHdrSize), 0, channelHeaderData, offSet, sizeof(ushort));
            offSet += sizeof(ushort);
            Buffer.BlockCopy(BitConverter.GetBytes(channelLatency), 0, channelHeaderData, offSet, sizeof(ulong));
            offSet += sizeof(ulong);
            Buffer.BlockCopy(BitConverter.GetBytes(channelSync), 0, channelHeaderData, offSet, sizeof(ulong));
            ////offSet += sizeof(ulong);
            fileStream.Write(channelHeaderData, 0, channelHeaderData.Length);
        }

        public void DeSerialize(Stream fileStream)
        {
            byte[] channelHeaderData = new byte[41];
            int offSet = 0;
            fileStream.Read(channelHeaderData, 0, channelHeaderData.Length);
            channelIndex = channelHeaderData[offSet];
            ++offSet;
            channelType = (ChannelType)channelHeaderData[offSet];
            ++offSet;
            vectorsPerFrame = BitConverter.ToUInt16(channelHeaderData, offSet);
            offSet += sizeof(ushort);
            samplesPerVector = BitConverter.ToUInt16(channelHeaderData, offSet);
            offSet += sizeof(ushort);
            bitsPerSample = channelHeaderData[offSet];
            offSet += sizeof(byte);
            this.currentHwFrameTime = BitConverter.ToUInt64(channelHeaderData, offSet);
            offSet += sizeof(ulong);
            this.currentHwFrameNo = BitConverter.ToUInt16(channelHeaderData, offSet);
            offSet += sizeof(ushort);
            noFramesAcq = BitConverter.ToUInt16(channelHeaderData, offSet);
            offSet += sizeof(ushort);
            frameRate = channelHeaderData[offSet];
            offSet += sizeof(byte);
            vectorHdrSize = BitConverter.ToUInt16(channelHeaderData, offSet);
            offSet += sizeof(ushort);
            channelLatency = BitConverter.ToUInt64(channelHeaderData, offSet);
            offSet += sizeof(ulong);
            channelSync = BitConverter.ToUInt64(channelHeaderData, offSet);
            ////offSet += sizeof(ulong);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            //info.AddValue("ChannelIndex", (ushort)this.channelIndex);
            info.AddValue("ChannelType", (byte)this.channelType);
            info.AddValue("VectorsPerFrame", this.vectorsPerFrame);
            info.AddValue("SamplesPerVector", this.samplesPerVector);
            info.AddValue("BitsPerSample", this.bitsPerSample);
            info.AddValue("CurrentHWFrameTime", this.currentHwFrameTime);
            info.AddValue("CurrentHWFrameNo", this.currentHwFrameNo);
            info.AddValue("NoFramesAcq", this.noFramesAcq);
            info.AddValue("FrameHdrSize", this.frameHdrSize);
            info.AddValue("FrameRate", this.frameRate);
            info.AddValue("VectorHdrSize", this.vectorHdrSize);
            info.AddValue("ChannelLatency", this.channelLatency);
            info.AddValue("ChannelSync", this.channelSync);
            info.AddValue("SampleMask", this.sampleMask);
            info.AddValue("TriggerType", (byte)this.triggerType);
            info.AddValue("FwVersion", this.firmwareVersion);
            info.AddValue("SwVersion", this.softwareVersion);
        }

        public override bool Equals(object obj)
        {
            ChannelHeader other = obj as ChannelHeader;
            if (other == null) return false;

            return (channelIndex == other.channelIndex) && (channelType == other.channelType)
                   && (frameHdrSize == other.frameHdrSize) && (frameRate == other.frameRate)
                   && (channelLatency == other.channelLatency) && (vectorHdrSize == other.vectorHdrSize)
                   && (vectorsPerFrame == other.vectorsPerFrame) && (samplesPerVector == other.samplesPerVector)
                   && (bitsPerSample == other.bitsPerSample) && (sampleMask == other.sampleMask)
                   && (triggerType == other.triggerType) && (currentHwFrameNo == other.currentHwFrameNo)
                   && (currentHwFrameTime == other.currentHwFrameTime) && (noFramesAcq == other.noFramesAcq)
                   && (channelSync == other.channelSync) && (firmwareVersion == other.firmwareVersion)
                   && (softwareVersion == other.softwareVersion);
        }

        public override int GetHashCode()
        {
            return frameHdrSize.GetHashCode() ^ vectorHdrSize.GetHashCode() ^ vectorsPerFrame.GetHashCode()
                   ^ samplesPerVector.GetHashCode() ^ bitsPerSample.GetHashCode();
        }
    }
}
