// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Frame.cs" company="">
//   
// </copyright>
// <summary>
//   The frame.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace PDAQ4DriverInterface
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    // Frame class definition

    /// <summary>
    /// The frame.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public class Frame
    {
        #region constants

        // Field Offsets into memory "block"

        /// <summary>
        /// The offset frame sync pattern.
        /// </summary>
        internal const int OffsetFrameSyncPattern = 0;

        /// <summary>
        /// The offset vectors per frame.
        /// </summary>
        internal const int OffsetVectorsPerFrame = 4;

        /// <summary>
        /// The offset channel type.
        /// </summary>
        internal const int OffsetChannelType = 6;

        /// <summary>
        /// The offset channel id.
        /// </summary>
        internal const int OffsetChannelID = 7;

        /// <summary>
        /// The offset samples per vector.
        /// </summary>
        internal const int OffsetSamplesPerVector = 8;

        /// <summary>
        /// The offset hw frame no.
        /// </summary>
        internal const int OffsetHWFrameNo = 12;

        /// <summary>
        /// The offset frame time.
        /// </summary>
        internal const int OffsetFrameTime = 16;

        /// <summary>
        /// The offset prev hw frame time.
        /// </summary>
        internal const int OffsetPrevHWFrameTime = 24;

        /// <summary>
        /// The offset prev hw frame no.
        /// </summary>
        internal const int OffsetPrevHWFrameNo = 32;

        /// <summary>
        /// The offset pullback distance.
        /// </summary>
        internal const int OffsetPullbackDistance = 36;

        /// <summary>
        /// The offset tx waveform type.
        /// </summary>
        internal const int OffsetTxWaveformType = 38;

        /// <summary>
        /// The offset sample spacing.
        /// </summary>
        internal const int OffsetSampleSpacing = 40;

        /// <summary>
        /// The offset frame size.
        /// </summary>
        internal const int OffsetFrameSize = 48;

        /// <summary>
        /// The offset bits per sample.
        /// </summary>
        internal const int OffsetBitsPerSample = 52;

        /// <summary>
        /// The offset tx operating point.
        /// </summary>
        internal const int OffsetTxOperatingPoint = 53;

        /// <summary>
        /// The offset trigger type.
        /// </summary>
        internal const int OffsetTriggerType = 54;

        /// <summary>
        /// The offset display method.
        /// </summary>
        internal const int OffsetDisplayMethod = 55;

        /// <summary>
        /// The offset tgc controls.
        /// </summary>
        internal const int OffsetTGCControls = 56;

        /// <summary>
        /// The offset Pre TGC value.
        /// </summary>
        internal const int OffsetPreTGCValue = 73;

        /// <summary>
        /// The offset TGC Delay.
        /// </summary>
        internal const int OffsetTGCDelay = 74;

        /// <summary>
        /// The offset Catheter Mask Info.
        /// </summary>
        internal const int OffsetCatheterMaskInfo = 76;

        /// <summary>
        /// The offset reserved.
        /// </summary>
        internal const int OffsetReserved = 80;

        /// <summary>
        /// The offset next field after tgc controls.
        /// </summary>
        internal const int OffsetNextFieldAfterTgcControls = OffsetPreTGCValue; // used to calculate TGC[] size below

        /// <summary>
        /// Offset into the header of the catheter offset information
        /// </summary>
        internal const int OffsetCatheterOffset = 76;

        /// <summary>
        /// The offset data.
        /// </summary>
        internal const int OffsetData = 96;

        // all the frame will be aligned at this boundary
        internal const int framesizeboundary = 128;

        #region Public constants
        /// <summary>
        /// The channel type offset.
        /// </summary>
        public const int ChannelTypeOffset = OffsetChannelType; // used by the marshaller

        /// <summary>
        /// The tgc offset.
        /// </summary>
        public const int TgcOffset = OffsetTGCControls;

        /// <summary>
        /// The tgc data size.
        /// </summary>
        public const int TgcDataSize = OffsetNextFieldAfterTgcControls - OffsetTGCControls;

        #endregion

        #endregion
        
        #region Data

        /// <summary>
        ///   Initialize size completed.
        /// </summary>
        private bool _initialized;

        /// <summary>
        /// The frame header block size.
        /// </summary>
        private int frameHeaderBlockSize = OffsetData;

        /// <summary>
        /// The vector samples block size.
        /// </summary>
        private int vectorSamplesBlockSize = 256 * ((Vector.VectorHeaderLength + 256) * 1);

        /// <summary>
        /// The _vector samples segment.
        /// </summary>
        private ArraySegment<byte> vectorSamplesSegment;

        /// <summary>
        /// The _frame block.
        /// </summary>
        private byte[] frameBlock;

        /// <summary>
        /// The vectors.
        /// </summary>
        private Vector[] vectors;

        /// <summary>
        /// The _marshaling buffers.
        /// List of references to the various component segments of the Frame
        /// </summary>
        private ArraySegment<byte>[] marshalingBuffers = new ArraySegment<byte>[2];

        private ChannelHeader __channelHeader;

        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Frame"/> class.
        /// </summary>
        public Frame()
        {
            FrameHeaderBlockSize = OffsetData;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Frame"/> class.
        /// </summary>
        /// <para>
        /// This constructor ensures that the allocated data blocks are large enough to hold the
        /// data for one frame of the channel type associated with the channel header.
        /// </para>
        /// <param name="channelHeader">
        /// The channel header.
        /// </param>
        public Frame(ChannelHeader channelHeader)
        {
            __channelHeader = new ChannelHeader();
            __channelHeader = channelHeader;
            FrameHeaderBlockSize = __channelHeader.FrameHdrSize;
            InitializeFromChannelHeader(channelHeader);
        }

        #endregion

        public ChannelHeader ChannelHdr
        {

            get { return __channelHeader; }

        }

        #region Marshalling management

        /// <summary>
        /// Gets or sets FrameHeaderBlockSize.
        /// </summary>
        public int FrameHeaderBlockSize
        {
            get
            {
                return this.frameHeaderBlockSize;
            }

            set
            {
                this.frameHeaderBlockSize = value;
            }
        }

        /// <summary>
        /// Gets or sets VectorSamplesBlockSize.
        /// </summary>
        public int VectorSamplesBlockSize
        {
            get
            {
                return this.vectorSamplesBlockSize;
            }

            set
            {
                this.vectorSamplesBlockSize = value;
            }
        }

        /// <summary>
        /// Gets or sets FrameBlock.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown if the data is too large
        /// </exception>
        public byte[] FrameBlock
        {
            get
            {
                if (this.frameBlock == null)
                {
                    this.frameBlock = new byte[this.FrameHeaderBlockSize + this.VectorSamplesBlockSize];
                }

                return this.frameBlock;
            }

            set
            {
                if ((this.frameBlock == null) || (this.marshalingBuffers == null))
                {
                    this.InitializeFromDataMember(value);
                }
                else
                {
                    if (value.Length > this.frameBlock.Length)
                    {
                        throw new ArgumentOutOfRangeException("From Frame.FrameBlock.set()");
                    }

                    this.frameBlock = value;
                }
            }
        }

        /// <summary>
        /// Gets MarshalingBuffers.
        /// </summary>
        /// <returns>List of Marshalling Buffers</returns>
        public IList<ArraySegment<byte>> MarshalingBuffers
        {
            get
            {
                return this.marshalingBuffers;
            }
        }

        /// <summary>
        /// The reallocate frame block.
        /// </summary>
        /// <returns>
        /// Array Segment produced after reallocation
        /// </returns>
        public ArraySegment<byte> ReallocateFrameBlock()
        {   
           
            int framesize = this.frameHeaderBlockSize + this.vectorSamplesBlockSize;
            int framesize_boundary_aligned = ((framesize+framesizeboundary-1)&~(framesizeboundary-1));
            this.frameBlock = new byte[framesize_boundary_aligned];
            this.marshalingBuffers[1] = new ArraySegment<byte>(this.frameBlock);
            this.vectorSamplesSegment = new ArraySegment<byte>(
                this.frameBlock, this.frameHeaderBlockSize, this.VectorSamplesBlockSize);
            return this.marshalingBuffers[1];
        }

        /// <summary>
        /// The reallocate vectors array.
        /// </summary>
        /// <param name="expectedVectorsPerFrame">
        /// The expected vectors per frame.
        /// </param>
        private void ReallocateVectorsArray(int expectedVectorsPerFrame)
        {
            // initialize
            this.vectors = new Vector[expectedVectorsPerFrame];
        }

        /// <summary>
        /// The initialize vectors array.
        /// </summary>
        /// <param name="expectedVectorsPerFrame">
        /// The expected vectors per frame.
        /// </param>
        /// <param name="vectorSize">
        /// The vector size.
        /// </param>
        public void InitializeVectorsArray(int expectedVectorsPerFrame, int vectorSize)
        {
            this.ReallocateVectorsArray(expectedVectorsPerFrame);

            // For each vector actually in the frame, create a Vector object and ArraySegment.
            for (int i = 0; i < expectedVectorsPerFrame; i++)
            {
                this.vectors[i] =
                    new Vector(
                        new ArraySegment<byte>(this.frameBlock, this.FrameHeaderBlockSize + i * vectorSize, vectorSize));
            }
        }

        #endregion

        #region Serialization Management

        /// <summary>
        /// TGet entire frame content as a byte array.
        /// This method is intended to be used only during serialization
        /// </summary>
        /// <param name="blockData">
        /// The block data.
        /// </param>
        public void GetFrameDataBlock(byte[] blockData)
        {
            if (blockData.Length >= (this.FrameHeaderBlockSize + this.VectorSamplesBlockSize))
            {
                // Copy frame header
                Buffer.BlockCopy(this.frameBlock, 0, blockData, 0, this.FrameHeaderBlockSize + this.VectorSamplesBlockSize);
            }
        }

        /// <summary>
        /// Set all of the bytes in a frame from a byte array.
        /// This method is intended to be used only during serialization
        /// </summary>
        /// <param name="blockData">
        /// The block data.
        /// </param>
        public void SetFrameDataBlock(byte[] blockData)
        {
            // Copy only if blockData size is at least the size of the frame
            if (blockData.Length >= (this.FrameHeaderBlockSize + this.VectorSamplesBlockSize))
            {
                // Copy frame header
                Buffer.BlockCopy(
                    blockData, 0, this.frameBlock, 0, this.FrameHeaderBlockSize + this.VectorSamplesBlockSize);
            }
        }

        #endregion

        #region Field accessors/mutators

        /// <summary>
        /// Gets or sets FrameSyncPattern.
        /// </summary>
        public uint FrameSyncPattern
        {
            get
            {
                return BitConverter.ToUInt32(this.frameBlock, OffsetFrameSyncPattern);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetFrameSyncPattern, bytes.Length);
            }
        }

        /// <summary>
        /// Gets or sets VectorsPerFrame.
        /// </summary>
        public ushort VectorsPerFrame
        {
            get
            {
                return BitConverter.ToUInt16(this.frameBlock, OffsetVectorsPerFrame);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetVectorsPerFrame, bytes.Length);
            }
        }

        /// <summary>
        /// Gets or sets ChannelType.
        /// </summary>
        public ChannelType ChannelType
        {
            get
            {
                return (ChannelType)this.frameBlock[OffsetChannelType];
            }

            set
            {
                this.frameBlock[OffsetChannelType] = (byte)value;
            }
        }

        /// <summary>
        /// Gets or sets ChannelId.
        /// </summary>
        public byte ChannelId
        {
            get
            {
                return this.frameBlock[OffsetChannelID];
            }

            set
            {
                this.frameBlock[OffsetChannelID] = value;
            }
        }

        /// <summary>
        /// Gets or sets SamplesPerVector.
        /// </summary>
        public uint SamplesPerVector
        {
            get
            {
                return BitConverter.ToUInt32(this.frameBlock, OffsetSamplesPerVector);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetSamplesPerVector, bytes.Length);
            }
        }

        /// <summary>
        /// Gets or sets the catheter offset.
        /// This is expressed as the ratio of the catheter diameter to the depth value.
        /// </summary>
        public float CatheterOffset
        {
            get
            {
                return BitConverter.ToSingle(this.frameBlock, OffsetCatheterOffset);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetCatheterOffset, bytes.Length);
            }
        }

        /// <summary>
        /// Gets or sets HwFrameNo.
        /// </summary>
        public uint HwFrameNo
        {
            get
            {
                return BitConverter.ToUInt32(this.frameBlock, OffsetHWFrameNo);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetHWFrameNo, bytes.Length);
            }
        }

        /// <summary>
        /// Gets or sets HwFrameTime.
        /// </summary>
        public ulong HwFrameTime
        {
            get
            {
                return BitConverter.ToUInt64(this.frameBlock, OffsetFrameTime);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetFrameTime, bytes.Length);
            }
        }

        /// <summary>
        /// Gets or sets PrevHwFrameTime.
        /// </summary>
        public ulong PrevHwFrameTime
        {
            get
            {
                return BitConverter.ToUInt64(this.frameBlock, OffsetPrevHWFrameTime);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetPrevHWFrameTime, bytes.Length);
            }
        }

        /// <summary>
        /// Gets or sets PrevHwFrameNo.
        /// </summary>
        public uint PrevHwFrameNo
        {
            get
            {
                return BitConverter.ToUInt32(this.frameBlock, OffsetPrevHWFrameNo);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetPrevHWFrameNo, bytes.Length);
            }
        }

        /// <summary>
        /// Gets or sets PullbackDistance.
        /// </summary>
        public short PullbackDistance
        {
            get
            {
                return BitConverter.ToInt16(this.frameBlock, OffsetPullbackDistance);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetPullbackDistance, bytes.Length);
            }
        }

        /// <summary>
        /// Gets or sets TxWaveformType.
        /// </summary>
        public ushort TxWaveformType
        {
            get
            {
                return BitConverter.ToUInt16(this.frameBlock, OffsetTxWaveformType);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetTxWaveformType, bytes.Length);
            }
        }

        /// <summary>
        /// Gets or sets SampleSpacing.
        /// </summary>
        public ulong SampleSpacing
        {
            get
            {
                return BitConverter.ToUInt64(this.frameBlock, OffsetSampleSpacing);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetSampleSpacing, bytes.Length);
            }
        }

        /// <summary>
        /// Gets or sets FrameSize.
        /// </summary>
        public uint FrameSize
        {
            get
            {
                return BitConverter.ToUInt32(this.frameBlock, OffsetFrameSize);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetFrameSize, bytes.Length);
            }
        }

        /// <summary>
        /// Gets or sets BitsPerSample.
        /// </summary>
        public byte BitsPerSample
        {
            get
            {
                return this.frameBlock[OffsetBitsPerSample];
            }

            set
            {
                this.frameBlock[OffsetBitsPerSample] = value;
            }
        }

        /// <summary>
        /// Gets or sets TxOperatingPoint.
        /// </summary>
        public byte TxOperatingPoint
        {
            get
            {
                return this.frameBlock[OffsetTxOperatingPoint];
            }

            set
            {
                this.frameBlock[OffsetTxOperatingPoint] = value;
            }
        }

        /// <summary>
        /// Gets or sets TriggerType.
        /// </summary>
        public byte TriggerType
        {
            get
            {
                return this.frameBlock[OffsetTriggerType];
            }

            set
            {
                this.frameBlock[OffsetTriggerType] = value;
            }
        }

        /// <summary>
        /// Gets or sets DisplayMethod.
        /// </summary>
        public byte DisplayMethod
        {
            get
            {
                return this.frameBlock[OffsetDisplayMethod];
            }

            set
            {
                this.frameBlock[OffsetDisplayMethod] = value;

                // byte[] bytes = BitConverter.GetBytes(value);
                // Buffer.BlockCopy(bytes, offset, frameBlock, offset, bytes.Length);
            }
        }

        /// <summary>
        /// The get tgc controls.
        /// </summary>
        /// <returns>
        /// </returns>
        public byte[] GetTgcControls()
        {
            var tgcControls = new byte[TgcDataSize];
            Buffer.BlockCopy(this.frameBlock, OffsetTGCControls, tgcControls, 0, tgcControls.Length);
            return tgcControls;
        }

        /// <summary>
        /// The set tgc controls.
        /// </summary>
        /// <param name="tgcIn">
        /// The tgc in.
        /// </param>
        public void SetTgcControls(byte[] tgcIn)
        {
            Buffer.BlockCopy(tgcIn, 0, this.frameBlock, OffsetTGCControls, TgcDataSize);
        }

        /// <summary>
        /// Gets or sets CatheterMaskInfo.
        /// </summary>
        public float CatheterMaskInfo
        {
            get
            {
                return BitConverter.ToSingle(this.frameBlock, OffsetCatheterMaskInfo);
            }

            set
            {
                byte[] bytes = BitConverter.GetBytes(value);
                Buffer.BlockCopy(bytes, 0, this.frameBlock, OffsetCatheterMaskInfo, bytes.Length);
            }
        }

        //// FwVersion
        // public ushort FwVersion
        // {
        // get
        // {
        // return BitConverter.ToUInt16(frameBlock, OffsetFwVersion);
        // }
        // set
        // {
        // byte[] bytes = BitConverter.GetBytes(value);
        // Buffer.BlockCopy(bytes, 0, frameBlock, OffsetFwVersion, bytes.Length);
        // }
        // }

        //// SwVersion
        // public ushort SwVersion
        // {
        // get
        // {
        // return BitConverter.ToUInt16(frameBlock, OffsetSwVersion);
        // }
        // set
        // {
        // byte[] bytes = BitConverter.GetBytes(value);
        // Buffer.BlockCopy(bytes, 0, frameBlock, OffsetSwVersion, bytes.Length);
        // }
        // }

        //// SampleMask - removed 
        // public uint SampleMask
        // {
        // get
        // {
        // return BitConverter.ToUInt32(frameBlock, OffsetSampleMask);
        // }
        // set
        // {
        // byte[] bytes = BitConverter.GetBytes(value);
        // Buffer.BlockCopy(bytes, 0, frameBlock, OffsetSampleMask, bytes.Length);
        // }
        // }
        #endregion

        /// <summary>
        /// Indexer for Vectors
        /// Data is modeled as an array of VectorsPerFrame Vector objects
        /// Index operator returning a Vector
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        public Vector this[int index]
        {
            get
            {
                return this.vectors[index];
            }
        }

        /// <summary>
        /// The print contents.
        /// </summary>
        /// <param name="bPrintVectorData">
        /// The b print vector data.
        /// </param>
        public void PrintContents(bool bPrintVectorData)
        {
            int i;

            Console.WriteLine("\tSyncPattern: \t\t\t{0}", this.FrameSyncPattern.ToString("x"));
            Console.WriteLine("\tVectorsPerFrame: \t\t{0}", this.VectorsPerFrame.ToString("x"));
            Console.WriteLine("\tChannelType: \t\t\t{0}", ((byte)this.ChannelType).ToString("x"));
            Console.WriteLine("\tChannelIndex: \t\t\t{0}", this.ChannelId.ToString("x"));
            Console.WriteLine("\tSamplesPerVector: \t\t{0}", this.SamplesPerVector.ToString("x"));
            Console.WriteLine("\tHwFrameNumber: \t\t\t{0}", this.HwFrameNo.ToString("x"));
            Console.WriteLine("\tTimeStamp: \t\t\t{0}", this.HwFrameTime.ToString("x")); // Not sure how to format time.
            Console.WriteLine("\tPrevTimeStamp: \t\t\t{0}", this.PrevHwFrameTime.ToString("x"));
                
                // Not sure how to format time.
            Console.WriteLine("\tPrevHwFrameNumber: \t\t{0}", this.PrevHwFrameNo.ToString("x"));
            Console.WriteLine("\tPullBackDistance: \t\t{0}", this.PullbackDistance.ToString("x"));
            Console.WriteLine("\tTxWaveform: \t\t\t{0}", this.TxWaveformType.ToString("x"));
            Console.WriteLine("\tSampleSpacing: \t\t\t{0}", this.SampleSpacing.ToString("x"));
            Console.WriteLine("\tFrameSize: \t\t\t{0}", this.FrameSize.ToString());
            Console.WriteLine("\tBitsPerSample: \t\t\t{0}", this.BitsPerSample.ToString("x"));
            Console.WriteLine("\tTxOpPoint: \t\t\t{0}", this.TxOperatingPoint.ToString("x"));
            Console.WriteLine("\tTriggerType: \t\t\t{0}", this.TriggerType.ToString("x"));
            Console.WriteLine("\tDisplayMethod: \t\t\t{0}", this.DisplayMethod.ToString("x"));
            Console.Write("\tTgc: \t\t\t\t");
            byte[] tgcControls = this.GetTgcControls();
            for (i = 0; i < 17; i++)
            {
                Console.Write("{0}", tgcControls[i].ToString("x"));
                if (i < 16)
                {
                    Console.Write(", ");
                }
                else
                {
                    Console.Write("\n");
                }
            }

            if (bPrintVectorData)
            {
                for (i = 0; i < this.VectorsPerFrame; i++)
                {
                    Console.WriteLine("\n\tVector number {0}", i);
                    this[i].PrintContents();
                }
            }
        }

        /// <summary>
        /// The add vector.
        /// </summary>
        /// <param name="vector">
        /// The vector.
        /// </param>
        /// <returns>
        /// The add vector.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public int AddVector(Vector vector)
        {
            throw new NotImplementedException();

            // ChannelHeader channelHeader = SessionManager.Instance.GetCurrentSession().GetChannelInfo(ChannelType);
            // // As long as the number of vectors in this frame is less than the number allocated.
            // if (channelHeader.VectorsPerFrame <= VectorsPerFrame)
            // throw new DataMisalignedException();

            // int vectorSize = channelHeader.CalculateVectorSize();
            // int next = vectors.Length;
            // vectors[next] = new Vector(new ArraySegment<byte>(VectorSamplesBlock, next * vectorSize, vectorSize));
            // this[next].Angle = vector.Angle;
            // this[next].ProcessingStatus = vector.ProcessingStatus;
            // this[next].TransmitTime= vector.TransmitTime;
            // this[next].VectorIndex = vector.VectorIndex;

            // for (int i = 0; i < SamplesPerVector * channelHeader.CalculateBytesPerSample(); i++)
            // this[next][i] = vector[i];
            // return 1;
        }

        /// <summary>
        /// Gets NoOfVectors.
        /// </summary>
        public int NoOfVectors
        {
            get
            {
                return this.vectors.Length;
            }
        }

        /// <summary>
        /// The get vector.
        /// </summary>
        /// <param name="index">
        /// The index.
        /// </param>
        /// <returns>
        /// </returns>
        public Vector GetVector(int index)
        {
            return this[index];
        }

        /// <summary>
        /// Initializes the frame using the information in a ChannelHeader.
        /// </summary>
        /// <param name="channelHeader">The ChannelHeader containing the initialization information.</param>
        protected void InitializeFromChannelHeader(ChannelHeader channelHeader)
        {
            // Calculate the size of one vector based on Channel Header data.
            int vectorSize = channelHeader.CalculateVectorSize();
            this.VectorSamplesBlockSize = channelHeader.VectorsPerFrame * vectorSize;

            // Reallocate the space for the Vectors and Samples data based on channel header calculations
            this.ReallocateFrameBlock();

            this.InitializeVectorsArray(channelHeader.VectorsPerFrame, vectorSize);
            this.FrameSize = (uint)(this.FrameHeaderBlockSize + this.VectorSamplesBlockSize);
        }

        #region Acquisition Remoting Implementation

        /// <summary>
        /// The presentation app needs this for using WCF between Imaging Service and 
        /// Data Repository.  WCF does not call the constructor and we have to ensure 
        /// the objects are not null when the FrameBlock is referenced.
        /// </summary>
        /// <param name="newData">
        /// The new data.
        /// </param>
        private void InitializeFromDataMember(byte[] newData)
        {
            this.frameBlock = newData;
            this.frameHeaderBlockSize = OffsetData;
            this.vectorSamplesBlockSize = newData.Length - OffsetData;
            this.marshalingBuffers = new ArraySegment<byte>[2];
            this.marshalingBuffers[1] = new ArraySegment<byte>(this.frameBlock);
        }

        #endregion

        /// <summary>
        /// Creates a token used by the PoolFactory to initialize VectorFrames created by the pool.
        /// </summary>
        /// <param name="channelHeader">
        /// The ChannelHeader used to generate the creation args. 
        /// </param>
        /// <returns>
        /// A token used by the PoolFactor to initialize VectorFrames. 
        /// </returns>
        public static object GetPoolCreationArgs(ChannelHeader channelHeader)
        {
            return channelHeader;
        }

        /// <summary>
        /// The initialize size.
        /// </summary>
        /// <param name="creationParams">
        /// The creation Params.
        /// </param>
        public virtual void Initialize(object creationParams)
        {
            if (!_initialized)
            {
                ChannelHeader channelHeader = (ChannelHeader)creationParams;
                InitializeFromChannelHeader(channelHeader);

                // Only init size once
                _initialized = true;
            }
        }

        /// <summary>
        /// Resets a a pooled object to it's default state.
        /// </summary>
        public virtual void Reset()
        {
        }
    }
}