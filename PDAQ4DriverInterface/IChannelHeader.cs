namespace PDAQ4DriverInterface
{
    using System.IO;

    // Enumeration definition for the ChannelType
   public enum ChannelType
   {
      RF_CHANNEL,
      RT_CHANNEL,
      RF_RAW_CHANNEL,
      NUMBER_OF_CHANNELS
   }

   public enum ChannelTrigger : byte
   {
     EncoderTrigger,      // motor
     FirmwareTrigger,     // FPGA generated
     PrimaryFrameTrigger, // frame sync of envelope signal
     TimerTrigger,        // Periodical Timer
     DemandTrigger,       // Software request
     EcgTrigger,          // not supported (note that ECG gating of RF is NOT acquisition trigger)
   }


   // Interface to specify the services that are provided by ChannelHeader object.
   public interface IChannelHeader
   {
      // Methods to get & set ChannelIndex
      byte ChannelIndex
      {
         get;
         set;
      }

      // Methods to get & set CHType
      ChannelType ChannelType
      {
         get;
         set;
      }

      // Methods to get & set FrameHdrSize
      ushort FrameHdrSize
      {
         get;
         set;
      }

      // Methods to get & set FrameRate
      byte FrameRate
      {
         get;
         set;
      }

      // Methods to get & set ChannelLatency
      ulong ChannelLatency
      {
         get;
      }
 
      // Methods to get & set VectorHdrSize
      ushort VectorHdrSize
      {
         get;
         set;
      }

      // Methods to get & set VectorsPerFrame
      ushort VectorsPerFrame
      {
         get;
         set;
      }

      // Methods to get & set SamplesPerVector
      ushort SamplesPerVector
      {
         get;
         set;
      }

      // Methods to get & set BitsPerSample
      byte BitsPerSample
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the SampleMask field.
      /// </summary>
      ushort SampleMask
      {
         get; 
         set;
      }

      /// <summary>
      /// Gets or sets the TriggerType field.
      /// </summary>
      ChannelTrigger TriggerType
      {
         get; 
         set;
      }

      // Methods to get & set CurrentHWFrameNo
      uint CurrentHwFrameNo
      {
         get;
         set;
      }

      // Methods to get & set CurrentHWFrameTime
      ulong CurrentHwFrameTime
      {
        get;
        set;
      }

      // Methods to get & set NoFramesAcq
      ushort NoFramesAcq
      {
        get;
        set;
      }

      // Methods to get & set ChannelSync
      ulong ChannelSync
      {
         get;
      }

      /// <summary>
      /// Gets or sets the Firmware version field.
      /// </summary>
      ushort FwVersion
      {
         get;
         set;
      }

      /// <summary>
      /// Gets or sets the Software version field.
      /// </summary>
      ushort SwVersion
      {
         get;
         set;
      }

       /// <summary>
       /// Gets the channel header length
       /// </summary>
       int ChannelHeaderLength { get; }

      /// <summary>
      /// Clones a ChannelHeader object.
      /// </summary>
      /// <returns>A copied ChannelHeader object.</returns>
      object Clone();

       /// <summary>
       /// CalculateBytesPerSample() calculates the number of bytes needed to allocate a single sample.
       /// The calculation is based on the BitsPerSample field.
       /// </summary>
       /// <returns>The number of bytes in a sample.</returns>
       int CalculateBytesPerSample();

       /// <summary>
       /// CalculateVectorSize() calculates the number of bytes needed to allocate a singel vector (including header)
       /// The calculation is based on Vector.VectorHeaderLength, SamplesPerVector and the CalculateBytesPerSample().
       /// </summary>
       /// <returns>The size of the vector in bytes.</returns>
       int CalculateVectorSize();

       /// <summary>
       /// CalculateFrameSize() calculates the number of bytes needed to allocate a frame (including the frame header).
       /// The calculation is based on FrameHdrSize, VectorsPerFrame and the CalculateVectorSize() method.
       /// </summary>
       /// <returns>The size of the frame in bytes.</returns>
       int CalculateFrameSize();

       void Serialize(Stream fileStream);

       void DeSerialize(Stream fileStream);
   }
}
